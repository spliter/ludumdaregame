/*
 * AnimationPath.cpp
 *
 *  Created on: 28-06-2012
 *      Author: Spliter
 */
#include "CurvePath.h"
#include <stdio.h>

CurvePath::CurvePath()
{
	_startMillis = 0;
	_endMillis = 1000;
	_controlPoints.push_back(lbVec3f(0,0,0));
	_controlPoints.push_back(lbVec3f(1,0,0));
	_tangents.push_back(lbVec3f(0, 0, 0));
	_tangents.push_back(lbVec3f(0, 0, 0));
	_curves.push_back(new HermiteCurve());
	_length = 0;
	_calculateFirstTangent = true;
	needsUpdate();
}

CurvePath::CurvePath(long startMillis, long endMillis, lbVec3f startPoint, lbVec3f endPoint)
{
	_startMillis = startMillis;
	_endMillis = endMillis;
	_controlPoints.push_back(startPoint);
	_controlPoints.push_back(endPoint);
	_tangents.push_back(lbVec3f(0, 0, 0));
	_tangents.push_back(lbVec3f(0, 0, 0));
	_curves.push_back(new HermiteCurve());
	_length = 0;
	_calculateFirstTangent = true;
	needsUpdate();
}

CurvePath::~CurvePath()
{
//	printf("Destroying animPath %d\n",_controlPoints.size());
	for(int i=0;i<_curves.size();i++)
	{
		delete _curves[i];
	}
}

void CurvePath::setFirstTangent(lbVec3f firstTangent)
{
	_firstTangent = firstTangent;
	needsUpdate();
}

void CurvePath::setCalculateFirstTangent(bool calculate)
{
	if(calculate!=_calculateFirstTangent)
	{
		_calculateFirstTangent = calculate;
		needsUpdate();
	}
}

void CurvePath::setStartTime(long startMillis)
{
	_startMillis = startMillis;
}

void CurvePath::setEndTime(long endMillis)
{
	_endMillis = endMillis;
}

lbVec3f CurvePath::getPosAtTime(long millis)
{
	long length = _endMillis-_startMillis;
	double localTime = millis-_startMillis;
	double ratio = double(localTime)/double(length);
	return getPosAt(ratio);
}

lbVec3f CurvePath::getPosAt(float ratio)
{
	if(ratio<=0.0f)
	{
		return _controlPoints[0];
	}
	if(ratio>=1.0f)
	{
		return _controlPoints[_controlPoints.size()-1];
	}
	updateCurves();
	float neededLength = _length*ratio;
	float curLength = 0;
	for(int i = 0; i<_curves.size(); i++)
	{
		float prevLength = curLength;
		float curveLen = _curves[i]->getLength();
		curLength += curveLen;
		if(curLength>=neededLength)
		{
			if(curveLen<0.0000001)
			{
				return _controlPoints[i];
			}
			else
			{
				float curveRatio = (neededLength-prevLength)/curveLen;
				float uniformtRatio = _curves[i]->getUniformTime(curveRatio);
				return _curves[i]->getSplinePoint(uniformtRatio);
			}
		}
	}
	return _controlPoints[_controlPoints.size()-1];
}

lbVec3f CurvePath::getSpeedAt(float ratio)
{
	updateCurves();
	if(ratio<=0.0f)
	{
		return _curves[0]->getSplineSpeed(0);
	}
	if(ratio>=1.0f)
	{
		return _curves[_curves.size()-1]->getSplineSpeed(1);
	}

	float neededLength = _length*ratio;
	float curLength = 0;
	for(int i = 0; i<_curves.size(); i++)
	{
		float prevLength = curLength;
		float curveLen = _curves[i]->getLength();
		curLength += curveLen;
		if(curLength>=neededLength)
		{
			if(curveLen<0.0000001)
			{
				return _controlPoints[i];
			}
			else
			{
				float curveRatio = (neededLength-prevLength)/curveLen;
				float uniformtRatio = _curves[i]->getUniformTime(curveRatio);
				return _curves[i]->getSplineSpeed(uniformtRatio);
			}
		}
	}
	return _curves[_curves.size()-1]->getSplineSpeed(1);
}


void CurvePath::getPointAt(float ratio, lbVec3f& pos, lbVec3f& speed)
{
	updateCurves();
	if(ratio<=0.0f)
	{
		pos = _controlPoints[0];
		speed = _tangents[0];
		return;
	}
	if(ratio>=1.0f)
	{
		pos = _controlPoints[_controlPoints.size()-1];
		speed = _tangents[_controlPoints.size()-1];
		return;
	}
	float neededLength = _length*ratio;
	float curLength = 0;
	for(int i = 0; i<_curves.size(); i++)
	{
		float prevLength = curLength;
		float curveLen = _curves[i]->getLength();
		curLength += curveLen;
		if(curLength>=neededLength)
		{
			if(curveLen<0.0000001)
			{

				pos = _controlPoints[i];
				speed = _tangents[i];
				return;
			}
			else
			{
				float curveRatio = (neededLength-prevLength)/curveLen;
				float uniformtRatio = _curves[i]->getUniformTime(curveRatio);
				pos = _curves[i]->getSplinePoint(uniformtRatio);
				speed = _curves[i]->getSplineSpeed(uniformtRatio);
				return;
			}
		}
	}
	pos = _controlPoints[_controlPoints.size()-1];
	speed = _tangents[_controlPoints.size()-1];
}



int CurvePath::getPointNum()
{
	return _controlPoints.size();
}

void CurvePath::addPoint(lbVec3f pos)
{
	_controlPoints.push_back(pos);
	_curves.push_back(new HermiteCurve());
	_tangents.push_back(lbVec3f(0, 0, 0));
	needsUpdate();
}

void CurvePath::setPoint(lbVec3f pos, int point)
{
	_controlPoints[point] = pos;
	needsUpdate();
}

lbVec3f CurvePath::getPoint(int point)
{
	return _controlPoints[point];
}

lbVec3f CurvePath::getTangent(int point)
{
	return _tangents[point];
}

void CurvePath::needsUpdate()
{
	_needsUpdate = true;
}

void CurvePath::updateCurves()
{
	if(!_needsUpdate)
		return;

	_needsUpdate = false;

	//Temporary tangents, later we should allow tangents to be configurable (ie: no tangent, split tangent, auto tangent)
	_tangents[0] = lbVec3f(0, 0, 0);
	_tangents[_tangents.size()-1] = lbVec3f(0, 0, 0);

	for(int i = 1; i<_tangents.size()-1; i++)
	{
		lbVec3f prevPoint = _controlPoints[i-1];
		lbVec3f nextPoint = _controlPoints[i+1];
		lbVec3f curPoint = _controlPoints[i];
		lbVec3f curtan = (nextPoint-prevPoint)*0.5f;
		_tangents[i] = curtan;
	}

	if(_tangents.size()>1)
	{
		if(_calculateFirstTangent)
		{
			_tangents[0] = (_controlPoints[1]-_controlPoints[0])*0.5f;
		}
		else
		{
			_tangents[0] = _firstTangent;
		}

		_tangents[_tangents.size()-1] = (_controlPoints[_tangents.size()-1]-_controlPoints[_tangents.size()-2])*0.5f;
	}

	_length = 0;
	for(int i = 0; i<_curves.size(); i++)
	{
		_curves[i]->setPointA(_controlPoints[i]);
		_curves[i]->setPointB(_controlPoints[i+1]);

		_curves[i]->setTangentA(_tangents[i]);
		_curves[i]->setTangentB(_tangents[i+1]);

		_length += _curves[i]->getLength();
	}

}

