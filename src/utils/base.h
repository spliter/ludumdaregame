/*
 * base.h
 *
 *  Created on: 08-02-2012
 *      Author: Spliter
 */

#pragma once


bool invertLBMatrix(const float *m, float *invOut);

#define LOGVAR(v) cout<<#v<<(v)<<endl;

//#define LOGGING_ENABLED
//#define FORCE_LOGGING_FLUSH

#ifdef LOGGING_ENABLED
	#define auxLog(info) Logger::getBuffer()<<__FUNCTION__<<"()  in  "<<__FILE__<<" @ "<<__LINE__<<": "<<info<<endl;Logger::flush()
	#ifdef FORCE_LOGGING_FLUSH
		#define LogState(info) auxLog(info);Logger::flush();
	#else
		#define LogState(info) auxLog(info);
	#endif
#else
	#define LogState(info)
#endif

