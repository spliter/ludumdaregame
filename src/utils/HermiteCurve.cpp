/*
 * HermiteSpline.cpp
 *
 *  Created on: 28-06-2012
 *      Author: Spliter
 */

#include "HermiteCurve.h"
#include <stdio.h>


HermiteCurve::HermiteCurve()
{
	set(lbVec3f(0,0,0),lbVec3f(0,0,0),lbVec3f(0,0,0),lbVec3f(0,0,0));
}

HermiteCurve::HermiteCurve(lbVec3f pointA, lbVec3f tanA, lbVec3f pointB, lbVec3f tanB)
{
	set(pointA, tanA, pointB, tanB);
}

void HermiteCurve::set(lbVec3f pointA, lbVec3f tanA, lbVec3f pointB, lbVec3f tanB)
{
	_pointA = pointA;
	_pointB = pointB;
	_tanA = tanA;
	_tanB = tanB;
	_precalcUniformTimes[0]=0.0f;
	_precalcUniformTimes[SPLINE_TIME_SECTION_NUMBER-1]=1.0f;
	_precalcLengths[0]=0.0f;
	needsUpdate();
}

void HermiteCurve::setPointA(lbVec3f pA)
{
	_pointA = pA;
	needsUpdate();
}

void HermiteCurve::setPointB(lbVec3f pB)
{
	_pointB = pB;
	needsUpdate();
}

void HermiteCurve::setTangentA(lbVec3f tA)
{
	_tanA = tA;
	needsUpdate();
}

void HermiteCurve::setTangentB(lbVec3f tB)
{
	_tanB = tB;
	needsUpdate();
}

lbVec3f HermiteCurve::getPointA() const
{
	return _pointA;
}

lbVec3f HermiteCurve::getPointB() const
{
	return _pointB;
}

lbVec3f HermiteCurve::getTangentA() const{
	return _tanA;
}

lbVec3f HermiteCurve::getTangentB() const
{
	return _tanB;
}

void HermiteCurve::needsUpdate()
{
	_timeNeedsUpdate = true;
	_lengthNeedsUpdate = true;
}

lbVec3f HermiteCurve::getSplinePoint(float t) const
{
	float tp2 = t*t;
	float tp3 = tp2*t;

	lbVec3f pt = (2*tp3-3*tp2+1)*_pointA+(tp3-2*tp2+t)*_tanA+(-2*tp3+3*tp2)*_pointB+(tp3-tp2)*_tanB;
	return pt;
}

lbVec3f HermiteCurve::getSplineSpeed(float t) const
{
	float tp2 = t*t;
	lbVec3f vt = (6*tp2-6*t)*_pointA+(3*tp2-4*t+1)*_tanA+(-6*tp2+6*t)*_pointB+(3*tp2-2*t)*_tanB;
	return vt;
}

float HermiteCurve::getLength()
{
	updatePrecalcLength();
	return _precalcLengths[SPLINE_LENGTH_SECTION_NUMBER-1];
}

float HermiteCurve::getLengthAt(float t)
{
	if (t<=0.0f)
	{
		return 0.0f;
	}

	updatePrecalcLength();

	if (t>=1.0f)
	{
		return _precalcLengths[SPLINE_LENGTH_SECTION_NUMBER-1];
	}

	float tAux = (SPLINE_LENGTH_SECTION_NUMBER-1)*t;
	float fract = tAux-(int) tAux;
	int index = (int)tAux;

	if(index>=SPLINE_LENGTH_SECTION_NUMBER)
		return _precalcLengths[SPLINE_LENGTH_SECTION_NUMBER-1];

	float length1 = _precalcLengths[index];
	float length2 = _precalcLengths[index+1];

	float length = length1+(length2-length1)*fract;

//	printf("Calculated length1 = %f -- length2 = %f -- final = %f\n",length1,length2,length);



	return length;

}

float HermiteCurve::getUniformTime(float t)
{
	if (t<=0.0f)
	{
		return 0.0f;
	}

	if (t>=1.0f)
	{
		return 1.0f;
	}

	updatePrecalcTime();
	float tAux = (SPLINE_TIME_SECTION_NUMBER-1)*t;
	float fract = tAux-(int) tAux;
	int index = (int)tAux;

	if(index>=SPLINE_TIME_SECTION_NUMBER)
		return 1.0f;

	float time1 = _precalcUniformTimes[index];
	float time2 = _precalcUniformTimes[index+1];

	float time = time1+(time2-time1)*fract;

//	printf("Uniform Time turns %f into %f (time1 = %f, time2 = %f, tAux = %f, fract = %f, index = %d)\n",t,time,time1,time2,tAux,fract,index);
	return time;
}

void HermiteCurve::updatePrecalcTime()
{
	if (!_timeNeedsUpdate)
	{
		return;
	}

	float length = getLength();
	float spacing = length/(SPLINE_TIME_SECTION_NUMBER-1);

//	printf("\nPrecalculating Timetime length = %f\n\n",length);
	for(int i=0; i<SPLINE_TIME_SECTION_NUMBER;i++)
	{
		float t = 0.5f;
		float lowT = 0.0f;
		float highT = 1.0f;
		float destL = i*spacing;
		float len = 0;
		for(int j=0;j<SPLINE_TIME_SECTION_ITERATION_NUMBER;j++)
		{
			len = getLengthAt(t);
			if(len<destL)
			{
				lowT = t;
			}
			else if(len>destL)
			{
				highT = t;
			}
			t = (highT+lowT)*0.5f;
		}
//		printf("Calculated time = %f (len = %f, destL = %f, deviation = %f%%)\n",t,len, destL,((len-destL)/spacing)*100.0f);
		_precalcUniformTimes[i] = t;
	}

	_precalcUniformTimes[0]=0.0f;
	_precalcUniformTimes[SPLINE_TIME_SECTION_NUMBER-1]=1.0f;

	_timeNeedsUpdate = false;
}

void HermiteCurve::updatePrecalcLength()
{
	if (!_lengthNeedsUpdate)
	{
		return;
	}
	float length = 0;
	double fract = 1.0/(SPLINE_LENGTH_SECTION_NUMBER*SPLINE_LENGTH_SECTION_ITERATION_NUMBER);
	lbVec3f prevPoint = _pointA;
	lbVec3f curPoint = _pointA;
	lbVec3f difVec;
	for(int i=0; i<SPLINE_LENGTH_SECTION_NUMBER-1;i++)
	{
		for(int j=1;j<=SPLINE_LENGTH_SECTION_ITERATION_NUMBER;j++)
		{
			float t = (i*SPLINE_LENGTH_SECTION_ITERATION_NUMBER+j)*fract;
			curPoint = getSplinePoint(t);

			difVec = curPoint - prevPoint;

			length+=difVec.length();
			prevPoint = curPoint;
		}
		_precalcLengths[i+1] = length;
	}
	_lengthNeedsUpdate = false;
}


