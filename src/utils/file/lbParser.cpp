/*
 * lbParser.cpp
 *
 *  Created on: 06-02-2012
 *      Author: Spliter
 */

#include "lbParser.h"
#include <cstdio>
#include <string>
#include <iostream>
using std::cout;

#define FLAG_IN_LINE_COMMENT 1
#define FLAG_IN_BLOCK_COMMENT 2
#define FLAG_IN_STRING 4



//character checking helper functions, ordered this way according to probability of appearance
inline bool isEmptyChar(char c)
{
	return c==' ' || c=='\t' || c=='\n' || c=='\r';
}

inline bool isAlphanumeric(char c)
{
	return (c>='a' && c<='z') || (c>='A' && c<='Z') || (c>='0' && c<='9');
}

inline bool isAlphanumericPlus(char c)
{
	return (c>='a' && c<='z') || (c>='A' && c<='Z') || c=='_' || (c>='0' && c<='9');
}

inline bool isNumerical(char c)
{
	return (c>='0' && c<='9');
}

lbParser::lbParser()
{
	parseFlags=0;
	fileOpen=false;
	lastValue[0]=0;
	file=NULL;
	parseSucceeded=true;
	prevParsePos=0;
}

lbParser::~lbParser()
{
	if(fileOpen)
	{
		fclose(file);
	}
}

bool lbParser::openFile(std::string filename)
{
	closeFile();//openFile might be called sequentially without closing the previous file, we're just double checking for those cases

	file=fopen(filename.c_str(),"rb");

	if(!file)
	{
		fileOpen=false;
		return false;
	}
	fileOpen=true;
	return true;
}

void lbParser::closeFile()//closed file and resets variables
{
	if(file)
	{
		fclose(file);

		fileOpen=false;
		lastValue[0]=0;
		parseFlags=0;
		file=NULL;
		prevParsePos=0;
	}
}

void lbParser::printRemainingFile()
{
	printf("PRINTING REMAINING FILE----------------------------------------------------------\n");
	char c;
	fpos_t pos;
	fgetpos(file,&pos);
	do
	{
		c=fgetc(file);
		putchar(c);
	}while(c!=EOF);
	fsetpos(file,&pos);
	printf("\n---------------------------------------------------------------------------------\n");
	fflush(stdout);
}

void lbParser::undoParse()
{
	fsetpos(file,&prevParsePos);
}

bool lbParser::isFileOpen()
{
	return fileOpen;
}

bool lbParser::eofReached()
{
	return !fileOpen || feof(file);
}

//note: for performance reasons we won't be checking if file is open before parsing
std::string lbParser::parseName()
{

	fgetpos(file,&prevParsePos);

	skipToNextUsable();
	char c=fgetc(file);
	lastValue[0]=0;
	int i=0;
	if(!isAlphanumericPlus(c))
	{
		ungetc(c,file);

		parseSucceeded=false;
		return std::string("");
	}


	while(c!=EOF && isAlphanumericPlus(c))
	{
		lastValue[i]=c;
		c=fgetc(file);
		i++;
	}
	lastValue[i]=0;

	ungetc(c,file);//we unget the character since the last character read did not belong to name and might be needed in the next parse
	parseSucceeded=true;
	return std::string(lastValue);
}

bool lbParser::parseToken(std::string token)
{
	fgetpos(file,&prevParsePos);
	skipToNextUsable();
	bool success=true;


	lastValue[0]=0;
	int rc=0;
	int length=token.length();
	while(rc<length)
	{
		char c=fgetc(file);
		lastValue[rc]=c;
		if(c!=token.at(rc))
		{
			ungetc(c,file);
			break;
		}
		rc++;
	}
	lastValue[rc]=0;
	if(rc!=length)
	{
		for(int i=rc-1;i>=0;i--)
		{
			ungetc(lastValue[i],file);
		}
		lastValue[0]=0;
		parseSucceeded=false;
		return false;
	}

	parseSucceeded=true;
	//by now we should have read all the characters in the token and nothing more
	return true;
}

bool lbParser::parseSingleToken(char tokenChar)
{
	fgetpos(file,&prevParsePos);

	skipToNextUsable();

	char c=fgetc(file);
	if(c==tokenChar)
	{
		parseSucceeded=true;
		return true;
	}

	ungetc(c,file);
	parseSucceeded=false;
	return false;
}


int lbParser::parseInt()
{
	fgetpos(file,&prevParsePos);
	skipToNextUsable();
	int value=0;
	int i=fscanf(file,"%d",&value);

	if(i==EOF || i==0)
		parseSucceeded=false;
	else
		parseSucceeded=true;

	return value;
}

float lbParser::parseFloat()
{
	fgetpos(file,&prevParsePos);
	skipToNextUsable();
	float value=0;
	int i=fscanf(file,"%f",&value);

	if(i==EOF || i==0)
		parseSucceeded=false;
	else
		parseSucceeded=true;

	return value;
}
#define addChar(character) lastValue[rc]=character;rc++;

std::string lbParser::parseString()
{
	fgetpos(file,&prevParsePos);
	skipToNextUsable();
	char c=fgetc(file);
	if(c=='\"')
	{
		lastValue[0]=0;
		int rc=0;
		while(true)
		{
			c=fgetc(file);
			if(c=='\\')
			{
				char c2=fgetc(file);
				switch(c2)
				{
					case 'n':addChar('\n');break;
					case 't':addChar('\t');break;
					case '\\':addChar('\\');break;
					case '\"':addChar('\"');break;
					case 'r':addChar('\r');break;
					//if none of those cases then we just ignore it
				}
			}
			else if(c=='\"')
			{
				break;
			}
			else if(c==EOF)
			{
				break;
			}
			else
			{
				addChar(c);
			}
		}

		lastValue[rc]=0;
		parseSucceeded=true;
		return std::string(lastValue);
	}
	else
	{
		parseSucceeded=false;
		return std::string("");
	}
}

int lbParser::skipValue(std::string openTag, std::string closeTag, bool areStringValues)
{
	if(openTag.empty() || closeTag.empty() || openTag==closeTag)
		return -1;
	fgetpos(file,&prevParsePos);

	char c=0;
	lastValue[0]=0;
	bool foundFirst=false;
	skipToNextUsable();
	int openTags=0;
	int closedTags=0;
	int openLetterCount=0;
	int closedLetterCount=0;


	while(c!=EOF)
	{

		int skipped=skipToNextUsable();
		if(skipped==0)
		{
			openLetterCount=0;
			closedLetterCount=0;
		}
		c=fgetc(file);
		if(c=='\"')
		{
			openLetterCount=0;
			closedLetterCount=0;
			parseFlags|=FLAG_IN_STRING;
			skipString();
			if(areStringValues)
			{
				openTags++;
				closedTags++;
				if(openTags==closedTags)
				{
					break;
				}
			}
			continue;
		}

		if(c==openTag[openLetterCount])
		{
			openLetterCount++;
			if(openLetterCount==openTag.length())
			{
				openTags++;
				openLetterCount=0;
				foundFirst=true;
			}
		}
		else
		{
			openLetterCount=0;
		}

		if(c==closeTag[closedLetterCount])
		{
			closedLetterCount++;
			if(closedLetterCount==closeTag.length())
			{
				closedTags++;
				closedLetterCount=0;
			}
		}
		else
		{
			closedLetterCount=0;
		}

		if(closedTags==openTags && closedTags!=0)
		{
			break;
		}
	}

	if(c==EOF)
	{
		if(closedTags!=openTags)
			return -1;
	}
	return closedTags;
}

bool lbParser::wasParseSuccessful()
{
	return parseSucceeded;
}

void lbParser::skipLineComment()
{
	//basically skip until the end the next line

	if(!(parseFlags&FLAG_IN_LINE_COMMENT))
		return;
	char c=EOF;
	do
	{
		c=fgetc(file);
	}while(c!='\n' && c!=EOF);
	parseFlags&=!FLAG_IN_LINE_COMMENT;
	return;
}

void lbParser::skipBlockComment()
{
	//basically skip to the next "*/" no matter where it is
	if(!(parseFlags&FLAG_IN_BLOCK_COMMENT))
		return;

	char c=EOF;

	do
	{
		c=fgetc(file);
		if(c=='*')
		{
			char c2=fgetc(file);
			if(c2=='/')
				break;
			//if this is not the closing '/' then we just unget it in case of sequential '*'
			ungetc(c2,file);
		}
	}while(c!=EOF);
	parseFlags&=!FLAG_IN_BLOCK_COMMENT;
	return;
}

void lbParser::skipString()
{
	//basically skip until the end the next line

	if(!(parseFlags&FLAG_IN_STRING))
		return;
	char c;
	char c2=0;
	while(true)
	{
		c=fgetc(file);
		if(c=='\"')
		{
			if(c2!='\"')
			{
				//we've reached an un-escaped quote meaning the end of the string
				break;
			}
		}
		else if(c==EOF)
		{
			break;
		}
		c2=c;
	}
	parseFlags&=!FLAG_IN_STRING;
	return;
}

int lbParser::skipToNextUsable()
{
	int i=0;
	char c=EOF;

	do
	{
		c=fgetc(file);

//		cout<<"char: \""<<c<<"\"\n";
//		cout.flush();
		if(!isEmptyChar(c))
		{
			if(c=='/')
			{
				char c2=fgetc(file);
				if(c2=='/')
				{
					parseFlags|=FLAG_IN_LINE_COMMENT;
					skipLineComment();
				}
				else if(c2=='*')
				{
					parseFlags|=FLAG_IN_BLOCK_COMMENT;
					skipBlockComment();
				}
				else
				{
					ungetc(c,file);
					break;
				}
			}
			else
			{
				ungetc(c,file);
				break;
			}
		}
		i++;
	}
	while(c!=EOF);



	return i;
}
