/*
 * lbParser.h
 *
 *  Created on: 06-02-2012
 *      Author: Spliter
 */

#pragma once

#include <string>
#include <cstdio>

#define MAX_PARSE_LENGTH 1024
class lbParser
{
public:
	lbParser();
	~lbParser();


	bool openFile(std::string filename);
	void closeFile();
	void printRemainingFile();
	void undoParse();//undoes one parse operation

	bool isFileOpen();
	bool eofReached();

	//parse functions: will parse a value and skip the caret to the character right after it

	//name is an alphanumeric std::string containing only 'A-Z','a-z','0-9' and _
	std::string parseName();//parses a name, returns empty std::string if no name was found

	bool parseToken(std::string token);//if token found it'll skip to the char right after it, otherwise will not do anything
	bool parseSingleToken(char tokenChar);


	int parseInt();//will parse an int
	float parseFloat();//will parse a float
	std::string parseString();//will parse a std::string

	/* skip until number of openTags found is equal to number of closedTags (tags must be outside of strings and comments)
	 */
	int skipValue(std::string openTag, std::string closeTag,bool areStringValues=true);

	bool wasParseSuccessful();//returns if last parse was successful

	fpos_t getPrevPos(){return prevParsePos;}
	FILE* getFile(){return file;}
private:
	void skipLineComment();
	void skipBlockComment();
	void skipString();
	int skipToNextUsable();//skips to next non-empty character outside of comments

	int parseFlags;
	bool fileOpen;
	char lastValue[MAX_PARSE_LENGTH];
	FILE* file;
	bool parseSucceeded;
	fpos_t prevParsePos;
};
