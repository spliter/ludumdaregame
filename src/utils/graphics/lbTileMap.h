/*
 * lbTileMap.h
 *
 *  Created on: 06-11-2012
 *      Author: Spliter
 */

#pragma once
#include <math/util/lbRect.h>
#include "lbDrawable.h"
#include <GLee.h>
class lbTexture;
class TiXmlElement;

class lbTileMap: public lbDrawable
{
public:
	struct Tile
	{
		unsigned int renderInfo;
		unsigned int color;
		Tile(){renderInfo=0;color=0xffffffff;}
	};

	static lbTileMap* createFromXmlElement(TiXmlElement* element);
	inline static int getTileTextureCoord(int x, int y){return (((x)<<8)|(y));}
	inline static int getTileTextureX(int textureCoord){return (((textureCoord)>>8)&0xff);}
	inline static int getTileTextureY(int textureCoord){return ((textureCoord)&0xff);}

	lbTileMap(int width, int height, int tileWidth, int tileHeight);
	virtual ~lbTileMap();

	void setTexture(lbTexture* texture, int tileColumns, int tileRows);
	lbTexture* getTexture(){return _texture;}

	virtual void draw(lbRectI camera);

	Tile* getTiles(){return _tileMap;}

	void updateTiles();

	int getWidth(){return _width;}
	int getHeight(){return _height;}

private:
	void setupVertexBuffer(int startX, int startY, int visibleColumns, int visibleRows);

	struct VertexData
	{
		GLfloat x,y,z;
		GLfloat tx,ty;
		GLuint color;
	};

	Tile* _tileMap;
	lbTexture* _texture;
	VertexData* _vertexData;
	GLuint _vbo;

	int	_width;
	int _height;

	int _tileWidth;
	int _tileHeight;

	int _textureTileColumns;
	int _textureTileRows;

	int _lastVisibleColumns;
	int _lastVisibleRows;

	//Note: later you can add an optimization for when player is standing still and the startX and startY don't change (no need to recreate the buffer there.
//	int _prevStartX;
//	int _prevStartY;

	int _numVertices;
};
