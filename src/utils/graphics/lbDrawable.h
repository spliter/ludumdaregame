/*
 * lbDrawable.h
 *
 *  Created on: 17-10-2012
 *      Author: Spliter
 */

#pragma once
#include <math/util/lbRect.h>

class lbDrawableLayer;

class lbDrawable
{
public:
	lbDrawable();
	virtual ~lbDrawable();

	void setRenderArea(lbRectI renderArea){_renderArea=renderArea;}
	void setRenderArea(float x1, float y1, float x2, float y2){_renderArea.set(x1,y1,x2,y2);}
	lbRectI getRenderArea() const {return _renderArea;}
	virtual void draw(lbRectI camera)=0;


	void setVisibility(bool isVisible){_visible=isVisible;}
	bool isVisible() const {return _visible;}

	void setZOrder(int zOrder){_zOrder=zOrder;}//higher z-order means it'll be drawn on top
	bool getZOrder() const {return _zOrder;}

	lbDrawableLayer* getLayer() const {return _layer;}
private:
	friend class lbDrawableLayer;
	friend bool drawableDepthComparison(lbDrawable* a, lbDrawable* b);
	int _zOrder;
	bool _visible;
	lbRectI _renderArea;
	lbDrawableLayer* _layer;
};

inline bool drawableDepthComparison(lbDrawable* a, lbDrawable* b)
{
	return a->_zOrder < b->_zOrder;
}
