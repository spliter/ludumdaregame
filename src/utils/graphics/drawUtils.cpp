/*
 * drawUtils.cpp
 *
 *  Created on: 16-09-2012
 *      Author: Spliter
 */
#include <GLee.h>
#include <math.h>

void drawCircle(float x, float y, float radius, int segments, bool drawFilled)
{
	if(drawFilled)
	{
		glBegin(GL_TRIANGLE_FAN);
	}
	else
	{
		glBegin(GL_LINE_STRIP);
	}

	float invAngle = 1.0f/segments*2.0f*M_PI;
	for(int i=0;i<=segments;i++)
	{
		float angle = (float)i*invAngle;
		float px = x+cos(angle)*radius;
		float py = y+sin(angle)*radius;
		glVertex2f(px,py);
	}
	glEnd();
}

void drawRect(float x1, float y1, float x2,float y2, bool drawFilled)
{
	if(drawFilled)
	{
		glBegin(GL_QUADS);
	}
	else
	{
		glBegin(GL_LINE_LOOP);
	}

	glTexCoord2f(0,0);glVertex2f(x1,y1);
	glTexCoord2f(1,0);glVertex2f(x2,y1);
	glTexCoord2f(1,1);glVertex2f(x2,y2);
	glTexCoord2f(0,1);glVertex2f(x1,y2);
	glEnd();
}

void drawLine(float x1, float y1, float x2,float y2)
{
	glBegin(GL_LINES);
		glVertex2f(x1,y1);
		glVertex2f(x2,y2);
	glEnd();
}

void drawAxis(float size)
{
	glBegin(GL_LINES);
	glColor3f(1,0,0); glVertex3f(0,0,0); glVertex3f(size,0,0);
	glColor3f(0,1,0); glVertex3f(0,0,0); glVertex3f(0,size,0);
	glColor3f(0,0,1); glVertex3f(0,0,0); glVertex3f(0,0,size);
	glEnd();
}
