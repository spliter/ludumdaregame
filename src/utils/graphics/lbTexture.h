/*
 * lbTexture.h
 *
 *  Created on: 09-10-2012
 *      Author: Spliter
 */

#pragma once
#include <math/util/lbRect.h>
#include <GLee.h>

class lbTexture
{
public:
	static lbTexture* loadTexture(const char* fname);

	lbTexture(lbTexture* parent, lbRectI region);
	lbTexture(int width, int height, unsigned char* data);
	lbTexture(unsigned int image);
	lbTexture(int width, int height, GLuint internalFormat, GLuint format, GLuint dataType, const char* data=NULL);

	virtual ~lbTexture();

	int getWidth(){return _width;}
	int getHeight(){return _height;}

	int getGLTexture(){return _glTexture;}
	int getILImage(){return _ilImage;}
	void updateGLTexture();
	bool isSubTexture(){return _isSubTexture;}//is this just a part of a larger texture? ie: tilemap

	lbRectF remapCoords(lbRectF rect);//remaps and normalizes coords
	void remapCoords(float inX, float inY, float &outNX, float &outNY);//remaps and normalizes coords
	void remapNormalCoords(float inNX, float inNY, float &outNX, float &outNY);//remaps normalized coords (does nothing if it's not a subtexture

	lbTexture* getParentTexture(){return _parentTex;}

	void bind();
	static void unbind();
private:
	bool _isSubTexture;
	GLuint _glTexture;
	int _ilImage;
	int _width,_height;
	int _xoffset,_yoffset;//offset from top left corner (if it's a subtexture)
	float _xscale,_yscale;//scale 1/size/parent_size (if sub texture)
	lbTexture* _parentTex;

	static GLuint _boundGLTexture;
};
