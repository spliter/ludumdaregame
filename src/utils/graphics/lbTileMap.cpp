/*
 * lbTileMap.cpp
 *
 *  Created on: 06-11-2012
 *      Author: Spliter
 */

#include "lbTileMap.h"
#include "../../utils/graphics/lbTexture.h"
#include <algorithm>
#include <stddef.h>


lbTileMap* lbTileMap::createFromXmlElement(TiXmlElement* element)
{
	return NULL;
}


lbTileMap::lbTileMap(int width, int height, int tileWidth, int tileHeight):
	_tileMap(NULL),
	_texture(NULL),
	_vertexData(NULL),
	_vbo(0),
	_width(width),
	_height(height),
	_tileWidth(tileWidth),
	_tileHeight(tileHeight),
	_textureTileColumns(0),
	_textureTileRows(0),
	_lastVisibleColumns(0),
	_lastVisibleRows(0),
	_numVertices(0)
{
	_tileMap = new Tile[width*height];
}

lbTileMap::~lbTileMap()
{
	if(_vbo!=0)
	{
		glDeleteBuffers(1,&_vbo);
		delete[] _vertexData;
		_vbo=0;
	}
	delete[] _tileMap;
}

void lbTileMap::setTexture(lbTexture* texture, int tileColumns, int tileRows)
{
	_texture = texture;
	_textureTileColumns=tileColumns;
	_textureTileRows=tileRows;
}


void lbTileMap::draw(lbRectI camera)
{
	if(_texture)
	{
		int startX = camera.x1/_tileWidth;
		int startY = camera.y1/_tileHeight;

		int columns = ((camera.x2-camera.x1)/_tileWidth)+2;
		int rows = ((camera.y2-camera.y1)/_tileHeight)+2;

		setupVertexBuffer(startX,startY,columns,rows);

		glBindBuffer(GL_ARRAY_BUFFER,_vbo);
		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3,GL_FLOAT,sizeof(VertexData),(void*)(offsetof(VertexData,x)));
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2,GL_FLOAT,sizeof(VertexData),(void*)(offsetof(VertexData,tx)));
		glEnableClientState(GL_COLOR_ARRAY);
		glColorPointer(4,GL_UNSIGNED_BYTE,sizeof(VertexData),(void*)(offsetof(VertexData,color)));


		glPushMatrix();
	//		glTranslatef(xOffset,yOffset,0);
			glScalef(_tileWidth,_tileHeight,1);
			_texture->bind();
			glDrawArrays(GL_QUADS,0,_numVertices);
			_texture->unbind();
		glPopMatrix();


		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}

void lbTileMap::setupVertexBuffer(int startX, int startY, int visibleColumns, int visibleRows)
{
	if(_vbo!=0 && (_lastVisibleColumns!=visibleColumns || _lastVisibleRows!=visibleRows))
	{
		glDeleteBuffers(1,&_vbo);
		delete[] _vertexData;
		_vbo=0;
	}

	if(_vbo==0)
	{
		_lastVisibleColumns = visibleColumns;
		_lastVisibleRows = visibleRows;
		_numVertices = visibleRows*visibleColumns*4;

		glGenBuffers(1,&_vbo);

		glBindBuffer(GL_ARRAY_BUFFER,_vbo);
		glBufferData(GL_ARRAY_BUFFER,_numVertices*sizeof(VertexData),NULL,GL_STREAM_DRAW);

		_vertexData = new VertexData[_numVertices];
	}
	int i=0;
	int tid=0;
	float txScale=1.0f/_textureTileColumns;
	float tyScale=1.0f/_textureTileRows;

	float txOffset = 0;
	float tyOffset = 0;
	_texture->remapNormalCoords(txOffset,tyOffset,txOffset,tyOffset);
	_texture->remapNormalCoords(txScale,tyScale,txScale,tyScale);

	memset(_vertexData,0,_numVertices*sizeof(VertexData));

	int maxX = std::min<int>(startX+visibleColumns,_width);
	int maxY = std::min<int>(startY+visibleRows,_height);
	startX=std::max<int>(startX,0);
	startY=std::max<int>(startY,0);

	for(int y=startY;y<maxY;y++)
	{
		for(int x=startX;x<maxX;x++)
		{
			tid=y*_width+x;
			int tile = _tileMap[tid].renderInfo;
			int tileX = getTileTextureX(tile);
			int tileY = getTileTextureY(tile);
			float tx = tileX*txScale+txOffset;
			float ty = tileY*tyScale+tyOffset;
			lbRectF tc(tx,ty,tx+txScale,ty+tyScale);
			unsigned int tileColor = _tileMap[tid].color;

			if(tileX!=255 || tileY!=255)
			{
				_vertexData[i].x=x;
				_vertexData[i].y=y;
				_vertexData[i].z=0;
				_vertexData[i].tx=tx;
				_vertexData[i].ty=ty;
				_vertexData[i].color=tileColor;


				i++;
				_vertexData[i].x=x;
				_vertexData[i].y=y+1;
				_vertexData[i].z=0;
				_vertexData[i].tx=tx;
				_vertexData[i].ty=ty+tyScale;
				_vertexData[i].color=tileColor;

				i++;
				_vertexData[i].x=x+1;
				_vertexData[i].y=y+1;
				_vertexData[i].z=0;
				_vertexData[i].tx=tx+txScale;
				_vertexData[i].ty=ty+tyScale;
				_vertexData[i].color=tileColor;

				i++;
				_vertexData[i].x=x+1;
				_vertexData[i].y=y;
				_vertexData[i].z=0;
				_vertexData[i].tx=tx+txScale;
				_vertexData[i].ty=ty;
				_vertexData[i].color=tileColor;

				i++;
			}
			else
			{
				i+=4;
			}
		}
	}
	glBindBuffer(GL_ARRAY_BUFFER,_vbo);
	glBufferData(GL_ARRAY_BUFFER,_numVertices*sizeof(VertexData),_vertexData,GL_STREAM_DRAW);
}
