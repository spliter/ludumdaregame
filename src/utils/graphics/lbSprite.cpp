/*
 * lbSprite.cpp
 *
 *  Created on: 10-10-2012
 *      Author: Spliter
 */
#include "drawUtils.h"
#include "../resources/lbTextureResourceManager.h"
#include <utils/graphics/lbTexture.h>
#include "lbSprite.h"
#include "../../Global.h"

#include <utils/debug/debug.h>
#include <utils/file/lbParser.h>
#include <GLee.h>
#include <string>


/*
 * Necessary tests:
 * 	-check if auto spacing calculation is working:
 * 		o-load an image with known frame size but unknown spacing
 * 		o-load an image with unknown frame size and spacing
 * 	-check if auto frame size calculation is working
 * 		o-load image with unknown frame size but known spacing
 * 		o-load image with unknown frame size and spacing
 */
using std::string;

lbSprite* lbSprite::loadSprite(const char* fname, lbTextureResourceManager* textureManager)
{
	int nColumns = 0;//column_number
	int nFrames = 0;//frame_number
	int centerX = CENTER_MIDDLE;//center_x (middle,left,right)
	int centerY = CENTER_MIDDLE;//center_y (middle,top,bottom)
	int startXOffset=0;//frame_start_x
	int startYOffset=0;//frame_start_y
	int hSpacing=-1;//spacing_horizontal
	int vSpacing=-1;//spacing_vertical
	int frameWidth=-1;//frame_width
	int frameHeight=-1;//frame_height
	string textureFname;//texture_file

	string textureDir = retrieveDirectory(string(fname));
	lbParser parser;
	if(parser.openFile(fname))
	{
		printf("Opened file: %s\n",fname);
		while(!parser.eofReached())
		{
			string name = parser.parseName();
			parser.parseSingleToken('=');

			if(name=="column_number")
			{
				nColumns = parser.parseInt();
			}
			else if(name=="frame_number")
			{
				nFrames= parser.parseInt();
			}
			else if(name=="center_x")
			{
				centerX = parser.parseInt();
				if(!parser.wasParseSuccessful())
				{
					parser.undoParse();

					string strVal = parser.parseString();
					if(parser.wasParseSuccessful())
					{
						if(strVal=="middle")
						{
							centerX = CENTER_MIDDLE;
						}
						else if(strVal=="left")
						{
							centerX = CENTER_LEFT;
						}
						else if(strVal=="right")
						{
							centerX = CENTER_RIGHT;
						}
					}
					else
					{
						printf("ERROR: Failed to parse value for center_x\n");
					}
				}
			}
			else if(name=="center_y")
			{
				centerY = parser.parseInt();
				if(!parser.wasParseSuccessful())
				{
					parser.undoParse();
					string strVal = parser.parseString();
					if(parser.wasParseSuccessful())
					{
						if(strVal=="middle")
						{
							centerY = CENTER_MIDDLE;
						}
						else if(strVal=="top")
						{
							centerY = CENTER_TOP;
						}
						else if(strVal=="bottom")
						{
							centerY = CENTER_BOTTOM;
						}
					}
					else
					{
						printf("ERROR: Failed to parse value for center_y\n");
					}
				}
			}
			else if(name=="frame_start_x")
			{
				startXOffset = parser.parseInt();
			}
			else if(name=="frame_start_y")
			{
				startYOffset = parser.parseInt();
			}
			else if(name=="spacing_horizontal")
			{
				hSpacing = parser.parseInt();
			}
			else if(name=="spacing_vertical")
			{
				vSpacing = parser.parseInt();
			}
			else if(name=="frame_width")
			{
				frameWidth = parser.parseInt();
			}
			else if(name=="frame_height")
			{
				frameHeight = parser.parseInt();
			}
			else if(name=="texture_file")
			{
				textureFname = parser.parseString();
				string textureDir = retrieveDirectory(string(fname));
				textureFname = textureDir+textureFname;
			}
			else
			{
				if(!name.empty())
				{
					printf("Unknown parameter name \"%s\", parse ending.\n\n\n",name.c_str());
					parser.printRemainingFile();
				}
				break;
			}
		}
		parser.closeFile();
//		printf("Closed file: %s\n",fname);

		if(nFrames<=0)
		{
			printf("PARSE ERROR: NO NUMBER OF FRAMES SPECIFIED\n");
			return NULL;
		}

		lbTexture* spriteTexture = textureManager->getResource(textureFname.c_str());
		if(spriteTexture==NULL)
		{
			return NULL;
		}

		return new lbSprite(spriteTexture,nFrames,nColumns,centerX,centerY,startXOffset,startYOffset,hSpacing,vSpacing,frameWidth,frameHeight);
	}
	else
	{
		printf("Failed to open sprite file: \"%s\"\n",fname);
	}

	return NULL;
}

lbSprite::lbSprite(lbTexture* texture, int nFrames, int nColumns, int centerX, int centerY, int startXOffset, int startYOffset, int hSpacing, int vSpacing, int frameWidth, int frameHeight)
{
	lbAssert(texture!=NULL && nFrames>0);
	if(nColumns<=0)
	{
		nColumns = nFrames;
	}

	int nRows = (nFrames+nColumns-1)/nColumns;
	_boundTexture = texture;
	int fWidth=frameWidth;
	int fHeight=frameHeight;
	int fHOffset=0;
	int fVOffset=0;
	int startX=0;
	int startY=0;

	if(frameWidth<0)//size doesn't matter
	{
		if(hSpacing<0)
		{
			//size doesn't matter and spacing doesn't matter
			hSpacing=0;
		}
		fWidth=(texture->getWidth()-(nColumns+1)*hSpacing)/nColumns;
		fHOffset = fWidth+hSpacing;
		startX=hSpacing;
		if(fWidth<0)
		{
			fWidth=1;
			fHOffset = 1;
			startX=0;
		}
	}
	else
	{
		fWidth = frameWidth;
		if(hSpacing<0)//size matters but spacing doesn't
		{
			int surplusWidth = texture->getWidth()/frameWidth;
			hSpacing = surplusWidth/nColumns;
		}
		fHOffset = hSpacing+fWidth;
		startX=hSpacing;
	}



	if(frameHeight<0)//size doesn't matter
	{
		if(vSpacing<0)
		{
			//size doesn't matter and spacing doesn't matter
			vSpacing=0;
		}
		fHeight=(texture->getHeight()-(nRows+1)*vSpacing)/nRows;
		fVOffset = fHeight+vSpacing;
		startY=vSpacing;
		if(fHeight<0)
		{
			fHeight=1;
			fVOffset = 1;
			startY=0;
		}
	}
	else
	{
		fHeight = frameHeight;
		if(vSpacing<0)//size matters but spacing doesn't
		{
			int surplusHeight= texture->getHeight()/frameHeight;
			vSpacing = surplusHeight/nRows;
		}
		fVOffset = vSpacing+fHeight;
		startY=vSpacing;
	}

	switch(centerX)
	{
		case CENTER_MIDDLE:
			centerX = fWidth/2;
			break;
		case CENTER_LEFT:
			centerX = 0;
			break;

		case CENTER_RIGHT:
			centerX = fWidth;
			break;
		default:
			//keep the same
			break;
	};

	switch(centerY)
	{
		case CENTER_MIDDLE:
			centerY = fHeight/2;
			break;
		case CENTER_TOP:
			centerY = 0;
			break;

		case CENTER_BOTTOM:
			centerY = fHeight;
			break;
		default:
			//keep the same
			break;
	};

	_bbox.set(-centerX,-centerY,-centerX+fWidth,-centerY+fHeight);

	_frames.reserve(nFrames);

	startX+=startXOffset;
	startY+=startYOffset;

//	printf("nFrames = %d, nColumns=%d, nRows=%d, fVOffset=%d, startY=%d",nFrames,nColumns,nRows,fVOffset,startY);
	int left;
	int top = startY;
	int totalFrames = 0;
	for(int y=0;y<nRows && totalFrames<nFrames;y++)
	{
		left = startX;

		for(int x=0;x<nColumns && totalFrames<nFrames;x++)
		{
			_frames.push_back(lbRectI(left,top,left+fWidth,top+fHeight));
			left+=fHOffset;
			totalFrames++;
		}
		top+=fVOffset;
	}

}

lbSprite::~lbSprite()
{

}

lbTexture* lbSprite::getBoundTexture()
{
	return _boundTexture;
}

int lbSprite::getFrameNum()
{
	return _frames.size();
}

lbRectI lbSprite::getFrameRect(int frame)
{
	return _frames[frame];
}

void lbSprite::renderFrame(int frame, int x, int y, float xScale, float yScale)
{
	_boundTexture->bind();

	lbRectF bbox = toRectF(getSpriteBbox());
	bbox.scale(xScale,yScale);
	bbox.move(x,y);

	lbRectF frameBbox = toRectF(getFrameRect(frame));

	frameBbox = _boundTexture->remapCoords(frameBbox);
	glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(frameBbox.x1,frameBbox.y1);glVertex2f(bbox.x1,bbox.y1);
		glTexCoord2f(frameBbox.x1,frameBbox.y2);glVertex2f(bbox.x1,bbox.y2);
		glTexCoord2f(frameBbox.x2,frameBbox.y1);glVertex2f(bbox.x2,bbox.y1);
		glTexCoord2f(frameBbox.x2,frameBbox.y2);glVertex2f(bbox.x2,bbox.y2);
	glEnd();
//	_boundTexture->unbind();
}

void lbSprite::renderFrame(int frame, lbRectF rect)
{
	_boundTexture->bind();
	lbRectF frameBbox = toRectF(getFrameRect(frame));
	frameBbox = _boundTexture->remapCoords(frameBbox);
	glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(frameBbox.x1,frameBbox.y1);glVertex2f(rect.x1,rect.y1);
		glTexCoord2f(frameBbox.x1,frameBbox.y2);glVertex2f(rect.x1,rect.y2);
		glTexCoord2f(frameBbox.x2,frameBbox.y1);glVertex2f(rect.x2,rect.y1);
		glTexCoord2f(frameBbox.x2,frameBbox.y2);glVertex2f(rect.x2,rect.y2);
	glEnd();
}

