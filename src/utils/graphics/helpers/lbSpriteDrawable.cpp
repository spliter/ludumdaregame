/*
 * lbSpriteDrawable.cpp
 *
 *  Created on: 24-11-2012
 *      Author: Spliter
 */

#include "lbSpriteDrawable.h"
#include "GLee.h"
lbSpriteDrawable::lbSpriteDrawable():
	_x(0),
	_y(0),
	_r(1),_g(1),_b(1),_a(1),
	_curFrame(0),
	_sprite(NULL)
{

}

lbSpriteDrawable::~lbSpriteDrawable()
{
}

void lbSpriteDrawable::setSprite(lbSprite* sprite)
{
	_sprite=sprite;
	if(_sprite)
	{
		lbRectI bbox =_sprite->getSpriteBbox();
		bbox.move(_x,_y);
		setRenderArea(bbox);
	}
	else
	{
		setRenderArea(-1,-1,-1,-1);
	}
}

void lbSpriteDrawable::setPosition(float x, float y)
{
	_x=x;
	_y=y;
	if(_sprite)
	{
		lbRectI bbox =_sprite->getSpriteBbox();
		bbox.move(_x,_y);
	}
}

void lbSpriteDrawable::draw(lbRectI camera)
{
	if(_sprite)
	{
		glColor4f(_r,_g,_b,_a);
		_sprite->renderFrame(_curFrame,_x,_y,1.0f,1.0f);
	}
}
