/*
 * lbDrawable.cpp
 *
 *  Created on: 17-10-2012
 *      Author: Spliter
 */

#include "lbDrawable.h"
#include "lbDrawableLayer.h"

lbDrawable::lbDrawable():
	_layer(NULL)
{
	_zOrder=0;
	_visible=true;
}

lbDrawable::~lbDrawable()
{
	if(_layer)
	{
		_layer->removeDrawable(this);
	}
}
