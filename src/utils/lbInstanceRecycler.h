/*
 * lbInstanceRecycler.h
 *
 *  Created on: 20-10-2012
 *      Author: Spliter
 */

#pragma once
#include <vector>
#include <list>

#ifndef NULL
#define NULL   ((void *) 0)
#endif

template<typename T> class lbInstanceRecycler
{
public:
	lbInstanceRecycler();
	~lbInstanceRecycler();

	void addInstanceToReserve(T* instance);

	T* useInstance();//uses up an instance
	void freeInstance(T* instance);

private:
	std::vector<T*> _instances;
	std::list<T*> _freeInstances;
};

template<typename T> lbInstanceRecycler<T>::lbInstanceRecycler()
{
}

template<typename T> lbInstanceRecycler<T>::~lbInstanceRecycler()
{
	_freeInstances.clear();
	for(int i=0;i<_instances.size();i++)
	{
		delete _instances[i];
	}
	_instances.clear();
}

template<typename T> void lbInstanceRecycler<T>::addInstanceToReserve(T* instance)
{
	_instances.push_back(instance);
	_freeInstances.push_back(instance);
}

template<typename T> T* lbInstanceRecycler<T>::useInstance()
{
	if(_freeInstances.empty())
	{
		return NULL;
	}
	T* instance = _freeInstances.front();
	_freeInstances.pop_front();
	return instance;
}

template<typename T> void lbInstanceRecycler<T>::freeInstance(T* instance)
{
	_freeInstances.push_back(instance);
}
