/*
 * AnimationPath.h
 *
 *  Created on: 28-06-2012
 *      Author: Spliter
 */

#pragma once
#include <vector>
#include "HermiteCurve.h"
#include <math/lbMath.h>

//Animation path between two keyframes composed of multiple curves
class CurvePath
{
public:
	CurvePath();
	CurvePath(long startMillis, long endMillis, lbVec3f startPoint, lbVec3f endPoint);
	~CurvePath();


	void setFirstTangent(lbVec3f firstTangent);
	void setCalculateFirstTangent(bool calculate);

	void setStartTime(long startMillis);
	void setEndTime(long endMillis);

	lbVec3f getPosAtTime(long millis);
	lbVec3f getPosAt(float ratio);
	lbVec3f getSpeedAt(float ratio);
	void getPointAt(float ratio, lbVec3f& pos, lbVec3f& speed);

	int getPointNum();

	void addPoint(lbVec3f pos);
	void setPoint(lbVec3f pos, int point);
	lbVec3f getPoint(int point);
	lbVec3f getTangent(int point);

	void needsUpdate();

private:

	void updateCurves();
	std::vector<lbVec3f> _controlPoints;
	std::vector<lbVec3f> _tangents;
	std::vector<HermiteCurve*> _curves;
	long _startMillis;
	long _endMillis;
	bool _needsUpdate;
	float _length;
	bool _calculateFirstTangent;
	lbVec3f _firstTangent;
};
