/*
 * debug.h
 *
 *  Created on: 09-10-2012
 *      Author: Spliter
 */

#pragma once
#include <cstdio>
#include <cstdlib>

#define lbAssert(condition)\
		if(!(condition)){printf("condition \""#condition"\" failed on line %d in file %s\n",__LINE__,__FILE__);exit(1);}

#define lbAssertMsg(condition, message)\
		if(!condition){printf("condition \""#condition"\" failed on line %d in file %s with message: \"%s\"\n",__LINE__,__FILE__,message);exit(1);}

#define lbAssertMsgArgs(condition,format,args...)\
		if(!condition){printf("condition \""#condition"\" failed on line %d in file %s with message: "format,__LINE__,__FILE__,args);exit(1);}
