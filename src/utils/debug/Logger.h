//logger.cpp
#ifndef __LOGGER__
#define __LOGGER__
#include <fstream>
#include <string>
#include <sstream>
#include <time.h>

class Logger
{
	  private:
			std::stringstream buffer;
			Logger();
			Logger(Logger& logger){}
			std::string filename;
			bool app;
	  public:


			static Logger& getLogger();
			static std::stringstream& getBuffer();
			//void Init()
			static void setLogFile(std::string& fname,bool append);
			static void logTime();
			static void logText(std::string& text);
			static void logText(const char* text);
			static void logValue(double val);
			static void logValue(float val);
			static void logValue(char val);
			static void logValue(int val);
			static void logPtr(void* val);
			static void clear();
			static void flush();//flush the current buffer entry
};
#define FLUSHLOG(x) Logger::logText(#x);Logger::flush();
#endif
