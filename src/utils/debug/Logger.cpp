#include "Logger.h"
#include <fstream>
#include <iostream>
#include <stdlib.h>

inline Logger::Logger()
{
	filename="LogOutputFile.log";
	app=false;
}
inline Logger& Logger::getLogger()
{
	static Logger logger;
	return logger;
}

std::stringstream& Logger::getBuffer()
{
	return Logger::getLogger().buffer;
}

void Logger::setLogFile(std::string& fname,bool append)
{
	Logger::getLogger().filename=fname;
	Logger::getLogger().app=append;
}

void Logger::logTime()
{
	char date[9],time[9];
	_strdate(date);
	_strtime(time);
	Logger::getLogger().buffer<<date<<" - "<<time;
}
void Logger::logText(const char* text)
{
    Logger::logTime();
	Logger::getLogger().buffer<<" - "<<text<<"\n";
}
void Logger::logText(std::string& text)
{
    Logger::logTime();
	Logger::getLogger().buffer<<" - "<<text<<"\n";
}
void Logger::logValue(int val)
{
	Logger::logTime();
	Logger::getLogger().buffer<<" - "<<val<<"\n";
}
void Logger::logValue(double val)
{
    Logger::logTime();
	Logger::getLogger().buffer<<" - "<<val<<"\n";
}
void Logger::logValue(float val)
{
    Logger::logTime();
	Logger::getLogger().buffer<<" - "<<val<<"\n";
}
void Logger::logValue(char val)
{
    Logger::logTime();
	Logger::getLogger().buffer<<" - "<<val<<"\n";
}
void Logger::logPtr(void* val)
{
	//unsigned int valptr=(unsigned int) val;
	Logger::logTime();
	Logger::getLogger().buffer<<" - "<<std::hex<<val<<std::dec<<"\n";
}
void Logger::clear()
{
	Logger::getLogger().buffer.clear();
}
void Logger::flush()
{
	std::ios_base::openmode writeType=Logger::getLogger().app?std::ios_base::out|std::ios_base::app:std::ios_base::out;
	std::ofstream logFile(getLogger().filename.c_str(),writeType);
	logFile<<Logger::getLogger().buffer.str();
	Logger::getLogger().buffer.ignore();
	logFile.close();
}
