/*
 * lbTextureManager.h
 *
 *  Created on: 25-05-2013
 *      Author: Spliter
 */

#pragma once
#include <map>
#include <string>
#include "lbResourceManager.h"

class lbTexture;

class lbTextureResourceManager: public lbResourceManager<lbTexture>
{
public:
	lbTextureResourceManager();
	virtual ~lbTextureResourceManager();

private:
	void createDefaultTexture();
	virtual lbTexture* getDefaultResource(){return _defaultTexture;}
	virtual lbTexture* loadResourceFromFile(std::string fname);
	lbTexture* _defaultTexture;
};
