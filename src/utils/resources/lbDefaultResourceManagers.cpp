/*
 * lbDefaultResourceManagers.cpp
 *
 *  Created on: 25-05-2013
 *      Author: Spliter
 */

#include "lbDefaultResourceManagers.h"
#include "lbTextureResourceManager.h"
#include "lbSpriteResourceManager.h"

lbDefaultResourceManagers& lbDefaultResourceManagers::instance()
{
	static lbDefaultResourceManagers instance;
	return instance;
}

lbDefaultResourceManagers::lbDefaultResourceManagers()
{
}

lbDefaultResourceManagers::~lbDefaultResourceManagers()
{
	_textureMgr->clearResources();
	_spriteMgr->clearResources();

	delete _textureMgr;
	delete _spriteMgr;
}

void lbDefaultResourceManagers::setup()
{
	_textureMgr = new lbTextureResourceManager();
	_spriteMgr = new lbSpriteResourceManager(_textureMgr);
}
