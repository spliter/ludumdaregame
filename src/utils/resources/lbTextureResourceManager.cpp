/*
 * lbTextureManager.cpp
 *
 *  Created on: 25-05-2013
 *      Author: Spliter
 */

#include "lbTextureResourceManager.h"
#include <utils/graphics/lbTexture.h>

lbTextureResourceManager::lbTextureResourceManager()
{
	createDefaultTexture();
}

lbTextureResourceManager::~lbTextureResourceManager()
{
	delete _defaultTexture;
}

lbTexture* lbTextureResourceManager::loadResourceFromFile(std::string fname)
{
	return lbTexture::loadTexture(fname.c_str());
}

void lbTextureResourceManager::createDefaultTexture()
{
	int texWidth = 8;
	int texHeight = 8;
	int texBpp = 4;
	unsigned char data[texWidth * texHeight * texBpp];
	for(int x = 0; x < texWidth; x++)
		for(int y = 0; y < texHeight; y++)
		{
			int i = (y * texWidth + x);

			int* datai = (int*) data;
			if((y + x) % 2)
			{
				datai[i] = 0xffbf5680;
			}
			else
			{
				datai[i] = 0xff000000;
			}
		}
	_defaultTexture = new lbTexture(texWidth, texHeight, data);
}
