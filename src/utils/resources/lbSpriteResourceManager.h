/*
 * lbSpriteResourceManager.h
 *
 *  Created on: 25-05-2013
 *      Author: Spliter
 */

#pragma once
#include <map>
#include <string>
#include "lbResourceManager.h"

class lbSprite;
class lbTexture;
class lbTextureResourceManager;

class lbSpriteResourceManager: public lbResourceManager<lbSprite>
{
public:
	static lbSpriteResourceManager& getInstance();
	lbSpriteResourceManager(lbTextureResourceManager* textureManager);
	virtual ~lbSpriteResourceManager();

private:
	void createDefaultSprite();
	virtual lbSprite* getDefaultResource(){return _defaultSprite;}
	virtual lbSprite* loadResourceFromFile(std::string fname);

	lbTexture* _defaultTexture;
	lbSprite* _defaultSprite;
	lbTextureResourceManager* _textureManager;
};
