/*
 * lbDefaultResourceManagers.h
 *
 *  Created on: 25-05-2013
 *      Author: Spliter
 */

#pragma once

class lbTextureResourceManager;
class lbSpriteResourceManager;

class lbDefaultResourceManagers
{
public:
	static lbDefaultResourceManagers& instance();
	lbDefaultResourceManagers();
	~lbDefaultResourceManagers();

	void setup();

	lbTextureResourceManager* getTextureManager(){return _textureMgr;}
	lbSpriteResourceManager* getSpriteManager(){return _spriteMgr;}
private:
	lbTextureResourceManager* _textureMgr;
	lbSpriteResourceManager* _spriteMgr;
};
