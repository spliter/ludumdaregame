/*
 * lbResourceManager.h
 *
 *  Created on: 09-10-2012
 *      Author: Spliter
 */

#pragma once
#include <map>
#include <string>

class lbSprite;
class lbAnimationData;
class lbAnimationClipSet;


template <class T>
class lbResourceManager
{
public:
	lbResourceManager();
	virtual ~lbResourceManager();//Note: resource must be cleared manually.
	virtual bool hasResource(std::string fname);
	virtual T* getResource(std::string fname);
	virtual void clearResources();

protected:
	virtual T* getDefaultResource()=0;
	virtual T* loadResourceFromFile(std::string fname)=0;

	std::map<std::string,T*> _resources;
};

template <class T>
lbResourceManager<T>::lbResourceManager()
{

}

template <class T>
lbResourceManager<T>::~lbResourceManager()
{
}

template <class T>
bool lbResourceManager<T>::hasResource(std::string fname)
{
	typename std::map<std::string,T*>::iterator iter = _resources.find(fname);
	return iter != _resources.end();
}

template <class T>
T* lbResourceManager<T>::getResource(std::string fname)
{
	typename std::map<std::string,T*>::iterator iter = _resources.find(fname);
	if(iter == _resources.end())
	{
		T* res = loadResourceFromFile(fname);
		if(!res)
		{
			res = getDefaultResource();
		}
		_resources.insert(std::pair<std::string, T*>(fname, res));
		return res;
	}
	return iter->second;
}

template <class T>
void lbResourceManager<T>::clearResources()
{
	while(!_resources.empty())
	{
		typename std::map<std::string,T*>::iterator iter = _resources.begin();
		if(iter->second != getDefaultResource())
		{
			delete iter->second;
		}
		_resources.erase(iter);
	}
}

//class lbResourceManager
//{
//public:
//	static lbResourceManager& getInstance();
//	virtual ~lbResourceManager();
//
//	void init();
//
//
//	bool hasSprite(std::string fname);//checks if a sprite is already loaded
//	lbSprite* getSprite(std::string fname);//will generate an empty sprite
//	void clearSprites();
//
//	bool hasAnimationData(std::string fname);//checks if a sprite is already loaded
//	lbAnimationData* getAnimationData(std::string fname);//will generate an empty sprite
//	void clearAnimationsDatas();
//
//	bool hasAnimationClipSet(std::string fname);//checks if a sprite is already loaded
//	lbAnimationClipSet* getAnimationClipSet(std::string fname);//will generate an empty sprite
//	void clearAnimationsClipSets();
//
//
//private:
//
//	lbResourceManager();
//
//	void createDefaultSprite();
//
//	typedef std::map<std::string,lbSprite*>::iterator SpriteIter;
//	std::map<std::string,lbSprite*> _sprites;
//	lbSprite* _defaultSprite;
//
//	typedef std::map<std::string,lbAnimationData*>::iterator AnimDataIter;
//	std::map<std::string,lbAnimationData*> _animationDatas;
//
//	typedef std::map<std::string,lbAnimationClipSet*>::iterator AnimClipSetIter;
//	std::map<std::string,lbAnimationClipSet*> _animationClipSets;
//	lbSprite* _defaultAnimationClipSet;
//
//};
