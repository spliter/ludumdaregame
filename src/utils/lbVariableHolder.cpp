/*
 * GlobalVariableHolder.cpp
 *
 *  Created on: 29-10-2012
 *      Author: Spliter
 */

#include "lbVariableHolder.h"

lbVariableHolder::lbVariableHolder()
{

}

lbVariableHolder::~lbVariableHolder()
{
	clearVariables();
}

void lbVariableHolder::clearVariables()
{
	_boolVariables.clear();
	_intVariables.clear();
	_floatVariables.clear();
	_vec3Variables.clear();
	_stringVariables.clear();
}

bool lbVariableHolder::hasVariable(std::string variableName, VariableType type)
{
	switch(type)
	{
		case lbVariableHolder::Boolean:
		{
			BoolIter iter = _boolVariables.find(variableName);
			return iter != _boolVariables.end();
		}
		break;
		case lbVariableHolder::Integer:
		{
			IntIter iter = _intVariables.find(variableName);
			return iter != _intVariables.end();
		}
		break;
		case lbVariableHolder::Float:
		{
			FloatIter iter = _floatVariables.find(variableName);
			return iter != _floatVariables.end();
		}
		break;
		case lbVariableHolder::Vector3:
		{
			Vec3Iter iter = _vec3Variables.find(variableName);
			return iter != _vec3Variables.end();
		}
		break;
		case lbVariableHolder::String:
		{
			StringIter iter = _stringVariables.find(variableName);
			return iter != _stringVariables.end();
		}
		break;
	}
	return false;
}

bool lbVariableHolder::setBoolean(std::string variableName, bool value)
{
	BoolIter iter = _boolVariables.find(variableName);
	if(iter==_boolVariables.end())
	{
		VariableInfo<bool> newVar;
		newVar.name=variableName;
		newVar.value=value;
		_boolVariables.insert(std::pair<std::string,VariableInfo<bool> >(variableName,newVar));
	}
	else
	{
		iter->second.value=value;
	}

	return true;
}

bool lbVariableHolder::getBoolean(std::string variableName, bool defaultValue)
{
	BoolIter iter = _boolVariables.find(variableName);
	if(iter!=_boolVariables.end())
	{
		return iter->second.value;
	}
	return defaultValue;
}

bool lbVariableHolder::setInteger(std::string variableName, int value)
{
	IntIter iter = _intVariables.find(variableName);
	if(iter==_intVariables.end())
	{
		VariableInfo<int> newVar;
		newVar.name=variableName;
		newVar.value=value;
		_intVariables.insert(std::pair<std::string,VariableInfo<int> >(variableName,newVar));
	}
	else
	{
		iter->second.value=value;
	}

	return true;
}

int lbVariableHolder::getInteger(std::string variableName, int defaultValue)
{
	IntIter iter = _intVariables.find(variableName);
	if(iter!=_intVariables.end())
	{
		return iter->second.value;
	}
	return defaultValue;
}

bool lbVariableHolder::setFloat(std::string variableName, float value)
{
	FloatIter iter = _floatVariables.find(variableName);
	if(iter==_floatVariables.end())
	{
		VariableInfo<float> newVar;
		newVar.name=variableName;
		newVar.value=value;
		_floatVariables.insert(std::pair<std::string,VariableInfo<float> >(variableName,newVar));
	}
	else
	{
		iter->second.value=value;
	}

	return true;
}

float lbVariableHolder::getFloat(std::string variableName, float defaultValue)
{
	FloatIter iter = _floatVariables.find(variableName);
	if(iter!=_floatVariables.end())
	{
		return iter->second.value;
	}
	return defaultValue;
}

bool lbVariableHolder::setVector3(std::string variableName, lbVec3f value)
{
	Vec3Iter iter = _vec3Variables.find(variableName);
	if(iter==_vec3Variables.end())
	{
		VariableInfo<lbVec3f> newVar;
		newVar.name=variableName;
		newVar.value=value;
		_vec3Variables.insert(std::pair<std::string,VariableInfo<lbVec3f> >(variableName,newVar));
	}
	else
	{
		iter->second.value=value;
	}

	return true;
}

lbVec3f lbVariableHolder::getVector3(std::string variableName, lbVec3f defaultValue)
{
	Vec3Iter iter = _vec3Variables.find(variableName);
	if(iter!=_vec3Variables.end())
	{
		return iter->second.value;
	}
	return defaultValue;
}

bool lbVariableHolder::setString(std::string variableName, std::string value)
{
	StringIter iter = _stringVariables.find(variableName);
	if(iter==_stringVariables.end())
	{
		VariableInfo<std::string> newVar;
		newVar.name=variableName;
		newVar.value=value;
		_stringVariables.insert(std::pair<std::string,VariableInfo<std::string> >(variableName,newVar));
	}
	else
	{
		iter->second.value=value;
	}

	return true;
}

std::string lbVariableHolder::getString(std::string variableName, std::string defaultValue)
{
	StringIter iter = _stringVariables.find(variableName);
	if(iter!=_stringVariables.end())
	{
		return iter->second.value;
	}
	return defaultValue;
}
