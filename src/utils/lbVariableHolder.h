/*
 * GlobalVariableHolder.h
 *
 *  Created on: 29-10-2012
 *      Author: Spliter
 */

#pragma once
#include <string>
#include <map>
#include <math/lbVec3f.h>

class lbVariableHolder
{
public:
	enum VariableType
	{
		Boolean,Integer,Float,Vector3,String
	};

	template<typename T>struct VariableInfo
	{
		std::string name;
		T value;
	};

	lbVariableHolder();
	virtual ~lbVariableHolder();

	void clearVariables();

	bool hasVariable(std::string variableName,VariableType type);

	bool setBoolean(std::string variableName, bool value);//returns true on success, false on failure
	bool getBoolean(std::string variableName, bool defaultValue);

	bool setInteger(std::string variableName, int value);//returns true on success, false on failure
	int getInteger(std::string variableName, int defaultValue);

	bool setFloat(std::string variableName, float value);//returns true on success, false on failure
	float getFloat(std::string variableName, float defaultValue);

	bool setVector3(std::string variableName, lbVec3f value);//returns true on success, false on failure
	lbVec3f getVector3(std::string variableName, lbVec3f defaultValue);

	bool setString(std::string variableName, std::string value);//returns true on success, false on failure
	std::string getString(std::string variableName, std::string defaultValue);

private:
	typedef std::map<std::string, VariableInfo<bool> >::iterator 		BoolIter;
	typedef std::map<std::string, VariableInfo<int> >::iterator 		IntIter;
	typedef std::map<std::string, VariableInfo<float> >::iterator 		FloatIter;
	typedef std::map<std::string, VariableInfo<lbVec3f> >::iterator 	Vec3Iter;
	typedef std::map<std::string, VariableInfo<std::string> >::iterator StringIter;

	std::map<std::string, VariableInfo<bool> > _boolVariables;
	std::map<std::string, VariableInfo<int> > _intVariables;
	std::map<std::string, VariableInfo<float> > _floatVariables;
	std::map<std::string, VariableInfo<lbVec3f> > _vec3Variables;
	std::map<std::string, VariableInfo<std::string> > _stringVariables;
};
