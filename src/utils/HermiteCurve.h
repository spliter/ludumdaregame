/*
 * HermiteSpline.h
 *
 *  Created on: 28-06-2012
 *      Author: Spliter
 */

#pragma once
#include <math/lbMath.h>

#define SPLINE_TIME_SECTION_NUMBER 80
#define SPLINE_LENGTH_SECTION_NUMBER 100

#define SPLINE_TIME_SECTION_ITERATION_NUMBER 20
#define SPLINE_LENGTH_SECTION_ITERATION_NUMBER 20

class HermiteCurve
{
public:
	HermiteCurve();
	HermiteCurve(lbVec3f pointA, lbVec3f tanA, lbVec3f pointB, lbVec3f tanB);

	void set(lbVec3f pointA, lbVec3f tanA, lbVec3f pointB, lbVec3f tanB);

	void setPointA(lbVec3f pA);
	void setPointB(lbVec3f pB);
	void setTangentA(lbVec3f tA);
	void setTangentB(lbVec3f tB);

	lbVec3f getPointA() const;
	lbVec3f getPointB() const;
	lbVec3f getTangentA() const;
	lbVec3f getTangentB() const;

	void needsUpdate();

	lbVec3f getSplinePoint(float t) const;
	lbVec3f getSplineSpeed(float t) const;

	float getLength();
	float getLengthAt(float t);

	float getUniformTime(float t);
private:
	void updatePrecalcTime();//if needed will update the uniform times
	void updatePrecalcLength();//if needed will update the lengths
	bool _timeNeedsUpdate;
	bool _lengthNeedsUpdate;

	float _precalcUniformTimes[SPLINE_TIME_SECTION_NUMBER];//precalculated times to get uniform spacing
	float _precalcLengths[SPLINE_LENGTH_SECTION_NUMBER];//precalculated lengths (linear t -> non-linear length)

	lbVec3f _pointA;
	lbVec3f _pointB;
	lbVec3f _tanA;
	lbVec3f _tanB;

//	float arr[SPLINE_TIME_SECTION_NUMBER];
//	float arr2[SPLINE_TIME_SECTION_NUMBER];
//	float arr3[SPLINE_TIME_SECTION_NUMBER];
};
