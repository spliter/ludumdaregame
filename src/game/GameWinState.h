/*
 * MenuState.h
 *
 *  Created on: 23-08-2013
 *      Author: Spliter
 */

#pragma once
#include <GLee.h>
#include <map>
#include <string>
#include "Constants.h"

#include "../states/State.h"
#include "../ui/UiButton.h"

#include <utils/graphics/basic/glCamera.h>
#include "GameContext.h"


class GameCore;
class GameState;
class lbTexture;

class GameWinState: public State, UiListener
{
public:
	GameWinState(GameCore* owner);
	virtual ~GameWinState();

	virtual void onStart();
	virtual void onResume(State* state);
	virtual void onPause(State* state);
	virtual void onFrameUpdate();
	virtual void onFrameRender();
	virtual void onEnd();

	virtual void onMouseMotionEvent(lbMouseMotionEvent *event);
	virtual void onMouseButtonEvent(lbMouseButtonEvent *event);
	virtual void onMouseWheelEvent(lbMouseWheelEvent *event);
	virtual void onKeyboardEvent(lbKeyboardEvent *event);

private:
	glCamera _uiCamera;

	GameCore* _owner;

	bool _inputLocked;

	lbTexture* _winScreen;

	float _animTime;
};

