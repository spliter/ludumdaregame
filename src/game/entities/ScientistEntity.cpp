/*
 * ScientistEntity.cpp
 *
 *  Created on: 24-08-2013
 *      Author: Spliter
 */

#include "ScientistEntity.h"
#include "PlayerEntity.h"
#include "../GameState.h"
#include <core/GameCore.h>
#include <engine/input/lbInputSystem.h>
#include <math/util/lbRect.h>
#include <utils/graphics/drawUtils.h>
#include <utils/graphics/lbDrawableLayer.h>
#include <utils/resources/lbDefaultResourceManagers.h>
#include <utils/resources/lbSpriteResourceManager.h>
#include <global.h>

ScientistEntity::ScientistEntity(GameContext* context):
	GameEntity(context)
{
	_isActive = false;
	_activateTime = 0;
}

ScientistEntity::~ScientistEntity()
{
}

void ScientistEntity::onCreated()
{
	setSize(lbVec2f(32,32));
	getGameContext()->gameState->getCharacterLayer()->addDrawable(this);
	lbInputSystem::getInstance().addGameEventListener(this);
	lbSprite* screenSprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/scientist_speech.txt");
	spriteDrawable.setSprite(screenSprite);
	spriteDrawable.setColour(1,1,1);

	_displaySprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/scientist.txt");
}

void ScientistEntity::onSpawned()
{
}

void ScientistEntity::onKilled()
{
	getGameContext()->gameState->getCharacterLayer()->removeDrawable(this);
	getGameContext()->gameState->getUILayer()->removeDrawable(&spriteDrawable);
}

void ScientistEntity::onDestroy()
{
}

void ScientistEntity::update(float dt)
{
	lbSprite* sprite = spriteDrawable.getSprite();
	float sw = getScreenWidth();
	float sh = getScreenHeight();
	float w = sprite->getFrameWidth();
	float h = sprite->getFrameHeight();
	spriteDrawable.setPosition((sw-w)/2,(sh-h)/2);
}

void ScientistEntity::draw(lbRectI camera)
{
	lbRectF rect = toRectF(getRenderArea());
	glColor3f(1.0f,1.0f,1.0f);
	_displaySprite->renderFrame(0,rect);
}

void ScientistEntity::handleEvent(lbGameEvent* event)
{
	DELEGATE_EVENT(event,lbKeyboardEvent,onKeyboardEvent)
}

void ScientistEntity::onKeyboardEvent(lbKeyboardEvent* event)
{
	switch(event->key)
	{
		case SDLK_z:
		{
			if(event->type == lbKeyboardEvent::KeyPressed)
			{
				if(_isActive && getGameContext()->gameCore->getCurrentFrameTime() - _activateTime > 10)
				{
					getGameContext()->gameState->getUILayer()->removeDrawable(&spriteDrawable);
					PlayerEntity* playerEnt = castEntity<PlayerEntity> (getGameContext()->gameState->getEntity(_playerId));
					if(playerEnt)
					{
						playerEnt->setInputEnabled(true);
					}
					_isActive = false;
					_activateTime = getGameContext()->gameCore->getCurrentFrameTime();
				}
			}
		}
		break;
	}
}

void ScientistEntity::onUse(GameEntity* user)
{
	if(!_isActive && getGameContext()->gameCore->getCurrentFrameTime() - _activateTime > 10)
	{
		getGameContext()->gameState->getUILayer()->addDrawable(&spriteDrawable);
		_isActive = true;
		_activateTime = getGameContext()->gameCore->getCurrentFrameTime();
		if(user->instanceOf<PlayerEntity> ())
		{
			((PlayerEntity*) user)->setInputEnabled(false);
			_playerId = user->getIndexId();
		}
	}
}

