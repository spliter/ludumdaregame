/*
 * ComputerEntity.h
 *
 *  Created on: 24-08-2013
 *      Author: Spliter
 */

#pragma once
#include "../GameEntity.h"
#include <engine/input/lbInputEvents.h>
#include <utils/graphics/helpers/lbSpriteDrawable.h>
#include <ui/lbTextBox.h>
class ComputerEntity: public GameEntity
{
public:
	ComputerEntity(GameContext* context);
	virtual ~ComputerEntity();

	virtual void onCreated();
	virtual void onSpawned();
	virtual void onKilled();
	virtual void onDestroy();

	virtual void update(float dt);
	virtual void draw(lbRectI camera);

	virtual void handleEvent(lbGameEvent* event);

	void onKeyboardEvent(lbKeyboardEvent* event);
	void onMouseMotionEvent(lbMouseMotionEvent* event){};
	void onMouseButtonEvent(lbMouseButtonEvent* event){};
	void onMouseWheelEvent(lbMouseWheelEvent* event){};
	virtual void onUse(GameEntity* user);
private:

	void activate();
	void deactivate();
	bool _isActive;
	long _activateTime;
	int _playerId;
	lbSpriteDrawable spriteDrawable;
	std::string _message;
	lbTextBox textBox;
	bool _passwordInserted;
	lbSprite* _displaySprite;
};

