/*
 * DoorEntity.cpp
 *
 *  Created on: 24-08-2013
 *      Author: Spliter
 */

#include "DoorEntity.h"
#include "PlayerEntity.h"
#include "../GameState.h"
#include <core/GameCore.h>
#include <engine/input/lbInputSystem.h>
#include <math/util/lbRect.h>
#include <utils/graphics/drawUtils.h>
#include <utils/graphics/lbDrawableLayer.h>
#include <utils/resources/lbDefaultResourceManagers.h>
#include <utils/resources/lbSpriteResourceManager.h>
#include <global.h>

DoorEntity::DoorEntity(GameContext* context) :
	GameEntity(context)
{
	_isActive = false;
	_activateTime = 0;
}

DoorEntity::~DoorEntity()
{
}

void DoorEntity::onCreated()
{
	setSize(lbVec2f(64, 32));
	getGameContext()->gameState->getCharacterLayer()->addDrawable(this);
	lbInputSystem::getInstance().addGameEventListener(this);
	lbSprite* screenSprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/numpad.txt");
	spriteDrawable.setSprite(screenSprite);
	spriteDrawable.setColour(1, 1, 1);

	_displaySprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/door.txt");

	textBox.setFont(getGameContext()->gameState->getBigFont());
	textBox.setSize(252, 64);
	textBox.setPosition(300, 300);
}

void DoorEntity::onSpawned()
{
}

void DoorEntity::onKilled()
{
	getGameContext()->gameState->getCharacterLayer()->removeDrawable(this);
	getGameContext()->gameState->getUILayer()->removeDrawable(&spriteDrawable);
}

void DoorEntity::onDestroy()
{
}

void DoorEntity::update(float dt)
{
	lbSprite* sprite = spriteDrawable.getSprite();
	float sw = getScreenWidth();
	float sh = getScreenHeight();
	float w = sprite->getFrameWidth();
	float h = sprite->getFrameHeight();
	float posX = (sw - w) / 2;
	float posY = (sh - h) / 2;
	spriteDrawable.setPosition(posX, posY);

	textBox.setPosition(posX + 150, posY + 60);
}

void DoorEntity::draw(lbRectI camera)
{
	lbRectF rect = toRectF(getRenderArea());
	glColor3f(1.0f,1.0f,1.0f);
	_displaySprite->renderFrame(0,rect);
}

void DoorEntity::handleEvent(lbGameEvent* event)
{
	DELEGATE_EVENT(event,lbKeyboardEvent,onKeyboardEvent)
}

void DoorEntity::onKeyboardEvent(lbKeyboardEvent* event)
{
	if(_isActive)
	{
		if(event->type == lbKeyboardEvent::KeyPressed)
		{
			if(event->key >= SDLK_0 && event->key <= SDLK_9)
			{
				if(_message.size() < 4)
				{
					_message += char('0' + event->key - SDLK_0);
					textBox.setText(_message);
				}
			}
			else if(event->key == SDLK_BACKSPACE)
			{
				if(_message.size() > 0)
				{
					_message = _message.substr(0, _message.size() - 1);
					textBox.setText(_message);
				}
			}
			else if(event->key == SDLK_RETURN)
			{
				if(_message == "4591")
				{

					Map* map = getGameContext()->gameState->getCurrentMap();
					int x = (getPosition().x + IMPORT_TILE_SIZE / 2) / IMPORT_TILE_SIZE;
					int y = (getPosition().y + IMPORT_TILE_SIZE / 2) / IMPORT_TILE_SIZE;
					map->getTileRef(x, y)->tileFlags &= ~GameConst::TFSolid;
					map->getTileRef(x + 1, y)->tileFlags &= ~GameConst::TFSolid;
					deactivate();
					kill();

					textBox.setText(_message);
				}
			}
			else
			{

				if(event->key >= SDLK_KP_0 && event->key <= SDLK_KP_9)
				{
					if(_message.size() < 4)
					{
						_message += char('0' + event->key - SDLK_KP_0);
						textBox.setText(_message);
					}
				}
				else
					switch(event->key)
					{
						case SDLK_KP_0:
							if(_message.size() < 4)
							{
								_message += '0';
								textBox.setText(_message);
							}
						break;
						case SDLK_KP_1:
							if(_message.size() < 4)
							{
								_message += '1';
								textBox.setText(_message);
							}
						break;
						case SDLK_KP_2:
							if(_message.size() < 4)
							{
								_message += '2';
								textBox.setText(_message);
							}
						break;
						case SDLK_KP_3:
							if(_message.size() < 4)
							{
								_message += '3';
								textBox.setText(_message);
							}
						break;
						case SDLK_KP_4:
							if(_message.size() < 4)
							{
								_message += '4';
								textBox.setText(_message);
							}
						break;
						case SDLK_KP_5:
							if(_message.size() < 4)
							{
								_message += '5';
								textBox.setText(_message);
							}
						break;
						case SDLK_KP_6:
							if(_message.size() < 4)
							{
								_message += '6';
								textBox.setText(_message);
							}
						break;
						case SDLK_KP_7:
							if(_message.size() < 4)
							{
								_message += '7';
								textBox.setText(_message);
							}
						break;
						case SDLK_KP_8:
							if(_message.size() < 4)
							{
								_message += '8';
								textBox.setText(_message);
							}
						break;
						case SDLK_KP_9:
							if(_message.size() < 4)
							{
								_message += '9';
								textBox.setText(_message);
							}
						break;

						case SDLK_z:
						case SDLK_ESCAPE:
						{
							if(getGameContext()->gameCore->getCurrentFrameTime() - _activateTime > 10)
							{
								deactivate();
							}
						}
						break;
					}
			}
		}
	}
}

void DoorEntity::onUse(GameEntity* user)
{
	if(!_isActive && getGameContext()->gameCore->getCurrentFrameTime() - _activateTime > 10)
	{
		if(user->instanceOf<PlayerEntity> ())
		{
			_playerId = user->getIndexId();
			activate();
		}
	}
}

void DoorEntity::activate()
{
	if(!_isActive)
	{
		_isActive = true;
		getGameContext()->gameState->getUILayer()->addDrawable(&spriteDrawable);
		_activateTime = getGameContext()->gameCore->getCurrentFrameTime();

		PlayerEntity* playerEnt = castEntity<PlayerEntity> (getGameContext()->gameState->getEntity(_playerId));
		if(playerEnt)
		{
			playerEnt->setInputEnabled(false);
		}
		getGameContext()->gameState->getUIRoot()->addElement(&textBox);
		_message = "";
	}
}

void DoorEntity::deactivate()
{
	if(_isActive)
	{
		_isActive = false;
		getGameContext()->gameState->getUILayer()->removeDrawable(&spriteDrawable);
		PlayerEntity* playerEnt = castEntity<PlayerEntity> (getGameContext()->gameState->getEntity(_playerId));
		if(playerEnt)
		{
			playerEnt->setInputEnabled(true);
		}
		_activateTime = getGameContext()->gameCore->getCurrentFrameTime();
		getGameContext()->gameState->getUIRoot()->removeElement(&textBox);
		_message = "";
	}
}
