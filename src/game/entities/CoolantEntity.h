/*
 * CoolantEntity.h
 *
 *  Created on: 24-08-2013
 *      Author: Spliter
 */

#pragma once

#pragma once
#include "../GameEntity.h"
#include <engine/input/lbInputEvents.h>
#include <utils/graphics/helpers/lbSpriteDrawable.h>
#include <utils/graphics/lbSprite.h>

class CoolantEntity: public GameEntity
{
public:
	CoolantEntity(GameContext* context);
	virtual ~CoolantEntity();

	virtual void onCreated();
	virtual void onSpawned();
	virtual void onKilled();
	virtual void onDestroy();

	virtual void update(float dt);
	virtual void draw(lbRectI camera);

	virtual void onUse(GameEntity* user);
private:
	bool _isActive;
	lbSprite* _displaySprite;
	bool _isJammed;
};

