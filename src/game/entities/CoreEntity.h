/*
 * CoreEntity.h
 *
 *  Created on: 24-08-2013
 *      Author: Spliter
 */

#pragma once

#pragma once
#include "../GameEntity.h"
#include <engine/input/lbInputEvents.h>
#include <utils/graphics/helpers/lbSpriteDrawable.h>
#include <utils/graphics/lbSprite.h>

class CoreEntity: public GameEntity
{
public:
	CoreEntity(GameContext* context);
	virtual ~CoreEntity();

	virtual void onCreated();
	virtual void onSpawned();
	virtual void onKilled();
	virtual void onDestroy();

	virtual void update(float dt);
	virtual void draw(lbRectI camera);

	void coolDown(){_isCooled = true;}
private:
	bool _isCooled;
	float _coolRatio;
	lbSprite* _displaySprite[3];
	float _animTime;
};

