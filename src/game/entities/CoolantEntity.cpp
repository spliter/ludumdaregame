/*
 * CoolantEntity.cpp
 *
 *  Created on: 24-08-2013
 *      Author: Spliter
 */

#include "CoolantEntity.h"
#include "PlayerEntity.h"
#include "../GameState.h"
#include <core/GameCore.h>
#include <engine/input/lbInputSystem.h>
#include <math/util/lbRect.h>
#include <utils/graphics/drawUtils.h>
#include <utils/graphics/lbDrawableLayer.h>
#include <utils/resources/lbDefaultResourceManagers.h>
#include <utils/resources/lbSpriteResourceManager.h>


CoolantEntity::CoolantEntity(GameContext* context):
	GameEntity(context)
{
	_isActive = false;
	_isJammed = false;
}

CoolantEntity::~CoolantEntity()
{
}

void CoolantEntity::onCreated()
{
	getGameContext()->gameState->getCharacterLayer()->addDrawable(this);
	lbInputSystem::getInstance().addGameEventListener(this);
	_displaySprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/coolant.txt");
}

void CoolantEntity::onSpawned()
{
	setSize(lbVec2f(32,32));
}

void CoolantEntity::onKilled()
{
	getGameContext()->gameState->getCharacterLayer()->removeDrawable(this);
}

void CoolantEntity::onDestroy()
{
}

void CoolantEntity::update(float dt)
{
}

void CoolantEntity::draw(lbRectI camera)
{
	lbRectF rect = toRectF(getRenderArea());
//	glColor3f(0.7f,0.7f,0.7f);
//	drawRect(rect.x1,rect.y1,rect.x2,rect.y2);
	glColor3f(1.0f,1.0f,1.0f);
	if(_isJammed)
	{
		_displaySprite->renderFrame(5,rect);
	}
	else
	{
		_displaySprite->renderFrame(_isActive?3:0,rect);
	}
}

void CoolantEntity::onUse(GameEntity* user)
{
	if(!_isActive)
	{
		_isActive = true;
		_isJammed = getGameContext()->gameState->onCoolantUsed(getName());
	}
}
