/*
 * PlayerEntity.cpp
 *
 *  Created on: 24-08-2013
 *      Author: Spliter
 */

#include "PlayerEntity.h"
#include "../GameState.h"
#include <engine/input/lbInputSystem.h>
#include <math/util/lbRect.h>
#include <math/lbMath.h>
#include <utils/graphics/drawUtils.h>
#include <utils/graphics/lbDrawableLayer.h>
#include <utils/graphics/lbSprite.h>
#include <utils/resources/lbDefaultResourceManagers.h>
#include <utils/resources/lbSpriteResourceManager.h>

enum
{
	ButtonUp = 0x01, ButtonDown = 0x02, ButtonLeft = 0x04, ButtonRight = 0x08,
};

PlayerEntity::PlayerEntity(GameContext* context) :
	GameEntity(context),
	_buttonsPressed(0),
	_inputEnabled(true),
	_facingDirection(GameUtil::dRight),
	_curSprite(SpritePlayerStand),
	_isMoving(false),
	_curFrame(0),
	_curFramef(0),
	_maxFrame(0)
{

}

PlayerEntity::~PlayerEntity()
{

}

void PlayerEntity::onCreated()
{
	setSize(lbVec2f(32, 32));
	getGameContext()->gameState->getCharacterLayer()->addDrawable(this);
	lbInputSystem::getInstance().addGameEventListener(this);

	_displaySprite[SpritePlayerStand] = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/spy_stand.txt");
	_displaySprite[SpritePlayerWalkUp] = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/spy_run_up.txt");
	_displaySprite[SpritePlayerWalkDown] = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/spy_run_down.txt");
	_displaySprite[SpritePlayerWalkLeft] = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/spy_run_left.txt");
	_displaySprite[SpritePlayerWalkRight] = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/spy_run_right.txt");

}

void PlayerEntity::onSpawned()
{
}

void PlayerEntity::onKilled()
{
	getGameContext()->gameState->getCharacterLayer()->removeDrawable(this);
}

void PlayerEntity::onDestroy()
{
}

void PlayerEntity::update(float dt)
{
	lbVec2f movement = 0;

	if(_inputEnabled)
	{
		if(_buttonsPressed)
		{
			getGameContext()->gameState->startCountdown();
		}
		if(_buttonsPressed & ButtonLeft)
		{
			movement.x -= 3;
		}
		if(_buttonsPressed & ButtonRight)
		{
			movement.x += 3;
		}
		if(_buttonsPressed & ButtonUp)
		{
			movement.y -= 3;
		}
		if(_buttonsPressed & ButtonDown)
		{
			movement.y += 3;
		}
	}

	if(movement.x != 0 || movement.y != 0)
	{
		_facingDirection = GameUtil::getDirectionFromPosition(movement.x, movement.y);

		switch(_facingDirection)
		{
			case GameUtil::dUp:
				_curSprite = SpritePlayerWalkUp;
			break;
			case GameUtil::dDown:
				_curSprite = SpritePlayerWalkDown;
			break;
			case GameUtil::dLeft:
				_curSprite = SpritePlayerWalkLeft;
			break;
			case GameUtil::dRight:
				_curSprite = SpritePlayerWalkRight;
			break;
		}
		_maxFrame = _displaySprite[_curSprite]->getFrameNum();
	}
	else
	{
		_curSprite = SpritePlayerStand;
		switch(_facingDirection)
		{
			case GameUtil::dUp:
				_curFrame = 0;
			break;
			case GameUtil::dDown:
				_curFrame = 1;
			break;
			case GameUtil::dLeft:
				_curFrame = 2;
			break;
			case GameUtil::dRight:
				_curFrame = 3;
			break;
		}
		_maxFrame = 0;
	}

	if(_maxFrame != 0)
	{
		_curFramef += dt * 8;
		_curFrame = _curFramef;
		if(_curFrame >= _maxFrame)
		{
			_curFrame = 0;
			_curFramef = 0;
		}
	}

	lbVec2f pos = getPosition();

	moveUntilCollisionHorizontally(movement.x);
	moveUntilCollisionVertically(movement.y);
	performCollisionCheck();

	glCamera* cam = getGameContext()->gameState->getGameCamera();
	cam->x = pos.x - cam->vp.w / 2;
	cam->y = pos.y - cam->vp.h / 2;
}

void PlayerEntity::draw(lbRectI camera)
{
	lbRectF rect = toRectF(getRenderArea());
	glColor3f(1.0f, 1.0f, 1.0f);
	if(_displaySprite[_curSprite])
	{
		_displaySprite[_curSprite]->renderFrame(_curFrame, rect);
	}
	else
	{
		drawRect(rect.x1, rect.y1, rect.x2, rect.y2);
	}
}

void PlayerEntity::handleEvent(lbGameEvent* event)
{
	DELEGATE_EVENT(event,lbKeyboardEvent,onKeyboardEvent)
	else DELEGATE_EVENT(event,lbMouseMotionEvent,onMouseMotionEvent)
	else DELEGATE_EVENT(event,lbMouseButtonEvent,onMouseButtonEvent)
	else DELEGATE_EVENT(event,lbMouseWheelEvent,onMouseWheelEvent)
}

void PlayerEntity::onKeyboardEvent(lbKeyboardEvent* event)
{
	switch(event->key)
	{
		case SDLK_UP:
		{
			if(event->type == lbKeyboardEvent::KeyPressed)
			{
				_buttonsPressed |= ButtonUp;
				//				_facingDirection = GameUtil::dUp;
			}
			else
			{
				_buttonsPressed &= ~ButtonUp;
			}
		}
		break;

		case SDLK_DOWN:
		{
			if(event->type == lbKeyboardEvent::KeyPressed)
			{
				_buttonsPressed |= ButtonDown;
				//				_facingDirection = GameUtil::dDown;
			}
			else
			{
				_buttonsPressed &= ~ButtonDown;
			}
		}
		break;

		case SDLK_LEFT:
		{
			if(event->type == lbKeyboardEvent::KeyPressed)
			{
				_buttonsPressed |= ButtonLeft;
				//				_facingDirection = GameUtil::dLeft;
			}
			else
			{
				_buttonsPressed &= ~ButtonLeft;
			}
		}
		break;

		case SDLK_RIGHT:
		{
			if(event->type == lbKeyboardEvent::KeyPressed)
			{
				_buttonsPressed |= ButtonRight;
				//				_facingDirection = GameUtil::dRight;
			}
			else
			{
				_buttonsPressed &= ~ButtonRight;
			}
		}
		break;

		case SDLK_z:
		{
			if(event->type == lbKeyboardEvent::KeyPressed && _inputEnabled)
			{
				int x = 0;
				int y = 0;
				GameUtil::getPositionFromDirection(_facingDirection, x, y);
				x = x * 32 + 16;
				y = y * 32 + 16;
				lbVec2f pos = getPosition();
				pos.x += x;
				pos.y += y;
				GameEntity* ent = getGameContext()->gameState->getEntityManager()->getHeadEntity();
				while(ent)
				{
					if(ent != this)
					{
						lbRectI otherRect = ent->getRenderArea();
						if(otherRect.isInside(pos.x, pos.y))
						{
							ent->onUse(this);
							break;
						}
					}
					ent = ent->getNext();
				}
			}
		}
		break;
	}
}

void PlayerEntity::onMouseMotionEvent(lbMouseMotionEvent* event)
{
}

void PlayerEntity::onMouseButtonEvent(lbMouseButtonEvent* event)
{
}

void PlayerEntity::onMouseWheelEvent(lbMouseWheelEvent* event)
{
}

void PlayerEntity::setInputEnabled(bool enabled)
{
	_inputEnabled = enabled;
//	if(_inputEnabled)
//	{
//		getGameContext()->gameState->startCountdown();
//	}
//	else
//	{
//		getGameContext()->gameState->pauseCountdown();
//	}
}

bool checkCollision(lbRectF &rect, Map* map)
{
	int minX = maxi(rect.x1 / IMPORT_TILE_SIZE, 0);
	int maxX = mini((rect.x2 - 1) / IMPORT_TILE_SIZE, map->width - 1);
	int minY = maxi(rect.y1 / IMPORT_TILE_SIZE, 0);
	int maxY = mini((rect.y2 - 1) / IMPORT_TILE_SIZE, map->height - 1);

	for(int x = minX; x <= maxX; x++)
	{
		for(int y = minY; y <= maxY; y++)
		{
			if(map->getTile(x, y).tileFlags & GameConst::TFSolid)
			{
				lbRectF cell(x * IMPORT_TILE_SIZE, y * IMPORT_TILE_SIZE, x * IMPORT_TILE_SIZE + IMPORT_TILE_SIZE, y * IMPORT_TILE_SIZE + IMPORT_TILE_SIZE);
				if(rect.isIntersecting(cell))
				{
					return true;
				}
			}
		}
	}
	return false;
}
void PlayerEntity::moveUntilCollisionHorizontally(float dx)
{
	if(dx > 0)
	{
		lbRectF rect = toRectF(getRenderArea());
		Map* map = getGameContext()->gameState->getCurrentMap();
		bool collision = false;
		float moved = 0;
		while(!collision && moved <= dx - 1)
		{
			rect.move(1, 0);
			collision = checkCollision(rect, map);
			if(!collision)
			{
				moved += 1;
			}
		}

		if(!collision)
		{
			moved = dx;
		}

		setPosition(getPosition() + lbVec2f(moved, 0));
	}
	else if(dx < 0)
	{
		lbRectF rect = toRectF(getRenderArea());
		Map* map = getGameContext()->gameState->getCurrentMap();
		bool collision = false;
		float moved = 0;
		while(!collision && moved >= dx + 1)
		{
			rect.move(-1, 0);
			collision = checkCollision(rect, map);
			if(!collision)
			{
				moved -= 1;
			}
		}

		if(!collision)
		{
			moved = dx;
		}

		setPosition(getPosition() + lbVec2f(moved, 0));
	}
}

void PlayerEntity::moveUntilCollisionVertically(float dy)
{
	if(dy > 0)
	{
		lbRectF rect = toRectF(getRenderArea());
		Map* map = getGameContext()->gameState->getCurrentMap();
		bool collision = false;
		float moved = 0;
		while(!collision && moved <= dy - 1)
		{
			rect.move(0, 1);
			collision = checkCollision(rect, map);
			if(!collision)
			{
				moved += 1;
			}
		}

		if(!collision)
		{
			moved = dy;
		}

		setPosition(getPosition() + lbVec2f(0, moved));
	}
	else if(dy < 0)
	{
		lbRectF rect = toRectF(getRenderArea());
		Map* map = getGameContext()->gameState->getCurrentMap();
		bool collision = false;
		float moved = 0;
		while(!collision && moved >= dy + 1)
		{
			rect.move(0, -1);
			collision = checkCollision(rect, map);
			if(!collision)
			{
				moved -= 1;
			}
		}

		if(!collision)
		{
			moved = dy;
		}

		setPosition(getPosition() + lbVec2f(0, moved));
	}
}

void PlayerEntity::performCollisionCheck()
{

}
