/*
 * PlayerEntity.h
 *
 *  Created on: 24-08-2013
 *      Author: Spliter
 */

#pragma once

#include "../GameEntity.h"
#include "../Constants.h"
#include <engine/input/lbInputEvents.h>

class lbSprite;

enum PlayerSprites
{
	SpritePlayerStand=0,
//	SpritePlayerStandUp = 0,
//	SpritePlayerStandDown,
//	SpritePlayerStandLeft,
//	SpritePlayerStandRight,
	SpritePlayerWalkUp,
	SpritePlayerWalkDown,
	SpritePlayerWalkLeft,
	SpritePlayerWalkRight,
	SpritePlayerDie,
	SpritePlayerNum,
};

class PlayerEntity: public GameEntity,public lbInputListener
{

public:
	PlayerEntity(GameContext* context);
	virtual ~PlayerEntity();

	virtual void onCreated();
	virtual void onSpawned();
	virtual void onKilled();
	virtual void onDestroy();

	virtual void update(float dt);
	virtual void draw(lbRectI camera);

	virtual void handleEvent(lbGameEvent* event);

	virtual void onKeyboardEvent(lbKeyboardEvent* event);
	virtual void onMouseMotionEvent(lbMouseMotionEvent* event);
	virtual void onMouseButtonEvent(lbMouseButtonEvent* event);
	virtual void onMouseWheelEvent(lbMouseWheelEvent* event);

	void setInputEnabled(bool enabled);

	void moveUntilCollisionHorizontally(float dx);
	void moveUntilCollisionVertically(float dy);
	void performCollisionCheck();
private:
	int _buttonsPressed;
	bool _inputEnabled;
	GameUtil::Direction _facingDirection;
	lbSprite* _displaySprite[SpritePlayerNum];
	int _curSprite;
	bool _isMoving;
	int _curFrame;
	float _curFramef;
	int _maxFrame;
};

