/*
 * CoreEntity.cpp
 *
 *  Created on: 24-08-2013
 *      Author: Spliter
 */

#include "CoreEntity.h"
#include "PlayerEntity.h"
#include "../GameState.h"
#include <core/GameCore.h>
#include <engine/input/lbInputSystem.h>
#include <math/util/lbRect.h>
#include <utils/graphics/drawUtils.h>
#include <utils/graphics/lbDrawableLayer.h>
#include <utils/resources/lbDefaultResourceManagers.h>
#include <utils/resources/lbSpriteResourceManager.h>

CoreEntity::CoreEntity(GameContext* context) :
	GameEntity(context)
{
	_isCooled = false;
	_animTime = 0;
}

CoreEntity::~CoreEntity()
{
}

void CoreEntity::onCreated()
{
	getGameContext()->gameState->getCharacterLayer()->addDrawable(this);
	lbInputSystem::getInstance().addGameEventListener(this);
	_displaySprite[0] = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/core_tank_bottom.txt");
	_displaySprite[1] = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/core_tank_top.txt");
	_displaySprite[2] = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/core_powerball.txt");
}

void CoreEntity::onSpawned()
{
	setSize(lbVec2f(128, 128));
}

void CoreEntity::onKilled()
{
	getGameContext()->gameState->getCharacterLayer()->removeDrawable(this);
}

void CoreEntity::onDestroy()
{
}

void CoreEntity::update(float dt)
{
	_animTime += dt;

	if(_isCooled)
	{
		_coolRatio += dt / 2;
		if(_coolRatio > 1.0f)
		{
			_coolRatio = 1.0f;
		}
	}
}

void CoreEntity::draw(lbRectI camera)
{
	lbRectF rect = toRectF(getRenderArea());
	float w = rect.x2 - rect.x1;
	float h = rect.y2 - rect.y1;
	lbVec2f pos = getPosition();
	lbRectF ballRect(pos.x, pos.y, pos.x + 64, pos.y + 64);
	ballRect.move((w - 64) / 2, (h - 64) / 2);
	if(_isCooled)
	{
		if(_coolRatio == 1.0f)
		{
			ballRect.move(0, (int)lengthDirY(_animTime * 90, 6));
			glColor3f(1.0f, 1.0f, 1.0f);
			_displaySprite[0]->renderFrame(1, rect);
			_displaySprite[2]->renderFrame(1, ballRect);
			_displaySprite[1]->renderFrame(1, rect);
		}
		else
		{
			lbVec2f srcMove(random(-7, 7), lengthDirY(_animTime * 180, 6)+random(-3, 3));
			lbVec2f dstMove(0, lengthDirY(_animTime * 90, 6));
			lbVec2f move = srcMove+(dstMove-srcMove)*_coolRatio;
			ballRect.move((int)move.x,(int)move.y);
			glColor3f(1.0f, 1.0f, 1.0f);
			_displaySprite[0]->renderFrame(0, rect);
			glColor4f(1.0f, 1.0f, 1.0f,_coolRatio);
			_displaySprite[0]->renderFrame(1, rect);
			glColor3f(1.0f, 1.0f, 1.0f);
			_displaySprite[2]->renderFrame(0, ballRect);
			glColor4f(1.0f, 1.0f, 1.0f,_coolRatio);
			_displaySprite[2]->renderFrame(1, ballRect);
			glColor3f(1.0f, 1.0f, 1.0f);
			_displaySprite[1]->renderFrame(0, rect);
			glColor4f(1.0f, 1.0f, 1.0f,_coolRatio);
			_displaySprite[1]->renderFrame(1, rect);
		}
	}
	else
	{
		ballRect.move((int)random(-7, 7), (int)(lengthDirY(_animTime * 180, 6)+random(-3, 3)));
		glColor3f(1.0f, 1.0f, 1.0f);
		_displaySprite[0]->renderFrame(0, rect);
		_displaySprite[2]->renderFrame(0, ballRect);
		_displaySprite[1]->renderFrame(0, rect);
	}
}
