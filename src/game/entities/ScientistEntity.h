/*
 * ScientistEntity.h
 *
 *  Created on: 24-08-2013
 *      Author: Spliter
 */

#pragma once
#include "../GameEntity.h"
#include <engine/input/lbInputEvents.h>
#include <utils/graphics/helpers/lbSpriteDrawable.h>

class ScientistEntity: public GameEntity
{
public:
	ScientistEntity(GameContext* context);
	virtual ~ScientistEntity();

	virtual void onCreated();
	virtual void onSpawned();
	virtual void onKilled();
	virtual void onDestroy();

	virtual void update(float dt);
	virtual void draw(lbRectI camera);

	virtual void handleEvent(lbGameEvent* event);

	void onKeyboardEvent(lbKeyboardEvent* event);
	void onMouseMotionEvent(lbMouseMotionEvent* event){};
	void onMouseButtonEvent(lbMouseButtonEvent* event){};
	void onMouseWheelEvent(lbMouseWheelEvent* event){};
	virtual void onUse(GameEntity* user);
private:
	bool _isActive;
	long _activateTime;
	int _playerId;
	lbSpriteDrawable spriteDrawable;
	lbSprite* _displaySprite;
};

