/*
 * ComputerEntity.cpp
 *
 *  Created on: 24-08-2013
 *      Author: Spliter
 */

#include "ComputerEntity.h"
#include "PlayerEntity.h"
#include "../GameState.h"
#include <core/GameCore.h>
#include <engine/input/lbInputSystem.h>
#include <math/util/lbRect.h>
#include <utils/graphics/drawUtils.h>
#include <utils/graphics/lbDrawableLayer.h>
#include <utils/resources/lbDefaultResourceManagers.h>
#include <utils/resources/lbSpriteResourceManager.h>
#include <global.h>

static bool iequals(const std::string& a, const std::string& b)
{
	unsigned int sz = a.size();
	if(b.size() != sz)
		return false;
	for(unsigned int i = 0; i < sz; ++i)
		if(tolower(a[i]) != tolower(b[i]))
			return false;
	return true;
}

ComputerEntity::ComputerEntity(GameContext* context) :
	GameEntity(context)
{
	_isActive = false;
	_activateTime = 0;
	_passwordInserted = false;
}

ComputerEntity::~ComputerEntity()
{
}

void ComputerEntity::onCreated()
{
	setSize(lbVec2f(64, 32));
	getGameContext()->gameState->getCharacterLayer()->addDrawable(this);
	lbInputSystem::getInstance().addGameEventListener(this);
	lbSprite* screenSprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/computer_screen.txt");
	spriteDrawable.setSprite(screenSprite);
	spriteDrawable.setColour(1, 1, 1);

	_displaySprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/computer.txt");

	textBox.setFont(getGameContext()->gameState->getBigFont());
	textBox.setSize(252, 64);
	textBox.setPosition(300, 300);
	textBox.setTextColour(0.8f, 1.0f, 0.2f, 1.0f);
}

void ComputerEntity::onSpawned()
{
}

void ComputerEntity::onKilled()
{
	getGameContext()->gameState->getCharacterLayer()->removeDrawable(this);
	getGameContext()->gameState->getUILayer()->removeDrawable(&spriteDrawable);
}

void ComputerEntity::onDestroy()
{
}

void ComputerEntity::update(float dt)
{
	lbSprite* sprite = spriteDrawable.getSprite();
	float sw = getScreenWidth();
	float sh = getScreenHeight();
	float w = sprite->getFrameWidth();
	float h = sprite->getFrameHeight();
	float posX = (sw - w) / 2;
	float posY = (sh - h) / 2;
	spriteDrawable.setPosition(posX, posY);

	textBox.setPosition(posX + 150, posY + 200);
	if(_passwordInserted)
	{
		spriteDrawable.setFrame(0);
	}
	else
	{
		spriteDrawable.setFrame(1);
	}
}

void ComputerEntity::draw(lbRectI camera)
{
	lbRectF rect = toRectF(getRenderArea());
	glColor3f(1.0f, 1.0f, 1.0f);
	_displaySprite->renderFrame(0, rect);
}

void ComputerEntity::handleEvent(lbGameEvent* event)
{
	DELEGATE_EVENT(event,lbKeyboardEvent,onKeyboardEvent)
}

void ComputerEntity::onKeyboardEvent(lbKeyboardEvent* event)
{
	if(_isActive)
	{
		if(event->type == lbKeyboardEvent::KeyPressed)
		{
			if(!_passwordInserted)
			{
				if(event->key >= SDLK_a && event->key <= SDLK_z)
				{
					if(_message.size() < 6)
					{
						_message += char('0' + event->key - SDLK_0);
						textBox.setText(_message);
					}
				}
				else if(event->key == SDLK_BACKSPACE)
				{
					if(_message.size() > 0)
					{
						_message = _message.substr(0, _message.size() - 1);
						textBox.setText(_message);
					}
				}
				else if(event->key == SDLK_RETURN)
				{
					if(iequals(_message, "phobos"))
					{
						_passwordInserted = true;
						textBox.setText(_message);
						getGameContext()->gameState->getUIRoot()->removeElement(&textBox);
					}
					else
					{
						deactivate();
					}
				}
				else
				{
					switch(event->key)
					{
						case SDLK_z:
						{
							if(getGameContext()->gameCore->getCurrentFrameTime() - _activateTime > 10)
							{
								deactivate();
							}
						}
						break;
					}
				}
			}
			else
			{
				switch(event->key)
				{
					case SDLK_z:
					{
						if(getGameContext()->gameCore->getCurrentFrameTime() - _activateTime > 10)
						{
							deactivate();
						}
					}
					break;
				}
			}

			if(event->key==SDLK_ESCAPE)
			{
				if(getGameContext()->gameCore->getCurrentFrameTime() - _activateTime > 10 && _isActive)
				{
					deactivate();
				}
			}
		}
	}
}

void ComputerEntity::onUse(GameEntity* user)
{
	if(!_isActive && getGameContext()->gameCore->getCurrentFrameTime() - _activateTime > 10)
	{
		if(user->instanceOf<PlayerEntity> ())
		{
			_playerId = user->getIndexId();
			activate();
		}
	}
}

void ComputerEntity::activate()
{
	if(!_isActive)
	{
		_isActive = true;
		getGameContext()->gameState->getUILayer()->addDrawable(&spriteDrawable);
		_activateTime = getGameContext()->gameCore->getCurrentFrameTime();

		PlayerEntity* playerEnt = castEntity<PlayerEntity> (getGameContext()->gameState->getEntity(_playerId));
		if(playerEnt)
		{
			playerEnt->setInputEnabled(false);
		}
		if(!_passwordInserted)
		{
			getGameContext()->gameState->getUIRoot()->addElement(&textBox);
		}
		_message = "";
	}
}

void ComputerEntity::deactivate()
{
	if(_isActive)
	{
		_isActive = false;
		getGameContext()->gameState->getUILayer()->removeDrawable(&spriteDrawable);
		PlayerEntity* playerEnt = castEntity<PlayerEntity> (getGameContext()->gameState->getEntity(_playerId));
		if(playerEnt)
		{
			playerEnt->setInputEnabled(true);
		}
		_activateTime = getGameContext()->gameCore->getCurrentFrameTime();
		getGameContext()->gameState->getUIRoot()->removeElement(&textBox);
		_message = "";
	}
}
