/*
 * DocumentsEntity.cpp
 *
 *  Created on: 24-08-2013
 *      Author: Spliter
 */

#include "DocumentsEntity.h"
#include "PlayerEntity.h"
#include "../GameState.h"
#include <core/GameCore.h>
#include <engine/input/lbInputSystem.h>
#include <math/util/lbRect.h>
#include <utils/graphics/drawUtils.h>
#include <utils/graphics/lbDrawableLayer.h>
#include <utils/resources/lbDefaultResourceManagers.h>
#include <utils/resources/lbSpriteResourceManager.h>
#include <global.h>

DocumentsEntity::DocumentsEntity(GameContext* context) :
	GameEntity(context)
{
	_isActive = false;
	_activateTime = 0;
}

DocumentsEntity::~DocumentsEntity()
{
}

void DocumentsEntity::onCreated()
{
	setSize(lbVec2f(64, 32));
	getGameContext()->gameState->getCharacterLayer()->addDrawable(this);
	lbInputSystem::getInstance().addGameEventListener(this);
	lbSprite* screenSprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/documents_screen.txt");
	spriteDrawable.setSprite(screenSprite);
	spriteDrawable.setColour(1,1,1);

	_displaySprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/documents.txt");
}

void DocumentsEntity::onSpawned()
{
}

void DocumentsEntity::onKilled()
{
	getGameContext()->gameState->getCharacterLayer()->removeDrawable(this);
	getGameContext()->gameState->getUILayer()->removeDrawable(&spriteDrawable);
}

void DocumentsEntity::onDestroy()
{
}

void DocumentsEntity::update(float dt)
{
	lbSprite* sprite = spriteDrawable.getSprite();
	float sw = getScreenWidth();
	float sh = getScreenHeight();
	float w = sprite->getFrameWidth();
	float h = sprite->getFrameHeight();
	spriteDrawable.setPosition((sw - w) / 2, (sh - h) / 2);
}

void DocumentsEntity::draw(lbRectI camera)
{
	lbRectF rect = toRectF(getRenderArea());
	glColor3f(1.0f,1.0f,1.0f);
	_displaySprite->renderFrame(0,rect);
}

void DocumentsEntity::handleEvent(lbGameEvent* event)
{
	DELEGATE_EVENT(event,lbKeyboardEvent,onKeyboardEvent)
}

void DocumentsEntity::onKeyboardEvent(lbKeyboardEvent* event)
{
	switch(event->key)
	{
		case SDLK_z:
		case SDLK_ESCAPE:
		{
			if(event->type == lbKeyboardEvent::KeyPressed)
			{
				if(_isActive && getGameContext()->gameCore->getCurrentFrameTime() - _activateTime > 10)
				{
					getGameContext()->gameState->getUILayer()->removeDrawable(&spriteDrawable);
					PlayerEntity* playerEnt = castEntity<PlayerEntity> (getGameContext()->gameState->getEntity(_playerId));
					if(playerEnt)
					{
						playerEnt->setInputEnabled(true);
					}
					_isActive = false;
					_activateTime = getGameContext()->gameCore->getCurrentFrameTime();
				}
			}
		}
		break;
	}
}

void DocumentsEntity::onUse(GameEntity* user)
{
	if(!_isActive && getGameContext()->gameCore->getCurrentFrameTime() - _activateTime > 10)
	{
		getGameContext()->gameState->getUILayer()->addDrawable(&spriteDrawable);
		_isActive = true;
		_activateTime = getGameContext()->gameCore->getCurrentFrameTime();
		if(user->instanceOf<PlayerEntity> ())
		{
			((PlayerEntity*) user)->setInputEnabled(false);
			_playerId = user->getIndexId();
		}
	}
}
