/*
 * MenuState.cpp
 *
 *  Created on: 23-08-2013
 *      Author: Spliter
 */

#include "GameWinState.h"

#include <GLee.h>
#include <iostream>
#include <sstream>
#include <FTGL/ftgl.h>
#include <FTGL/FTGLTextureFont.h>
#include <ft2build.h>

#include "GameState.h"

#include "../core/GameCore.h"
#include "../Global.h"
#include "../utils/graphics/drawUtils.h"

#include "../utils/graphics/lbTexture.h"
#include "../utils/resources/lbDefaultResourceManagers.h"
#include "../utils/resources/lbTextureResourceManager.h"

GameWinState::GameWinState(GameCore* owner) :
	State(), _owner(owner)
{

}

GameWinState::~GameWinState()
{
}

void GameWinState::onStart()
{
	lbTextureResourceManager* textureMgr = lbDefaultResourceManagers::instance().getTextureManager();
	_winScreen = textureMgr->getResource("data/graphics/textures/win_screen.png");

	_uiCamera.vp.set(0, 0, getScreenWidth(), getScreenHeight(), 0, -1, 1);
	_uiCamera.set(0, 0, 0, 0, 0, 0);
	_animTime = 0;
}

void GameWinState::onResume(State* state)
{
	_inputLocked = false;
}

void GameWinState::onPause(State* state)
{
}

void GameWinState::onFrameUpdate()
{
	_animTime += getGameCore().getFrameTimeDifferencef();
}

void GameWinState::onFrameRender()
{
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	glLoadIdentity();

	_uiCamera.push();
	glColor3f(1, 1, 1);

	float sWidth = getScreenWidth();
	float sHeight = getScreenHeight();

	float backWidth = _winScreen->getWidth();
	float backHeight = _winScreen->getHeight();

	_winScreen->bind();
	float x = (sWidth - backWidth) / 2;
	float y = (sHeight - backHeight) / 2;
	drawRect(x, y, x + backWidth, y + backWidth);
	_winScreen->unbind();

	if(_animTime < 2.0f)
	{
		float ratio = normalize(0, 2.0f, _animTime);
		float alpha = lerp(1.0f, 0.0f, ratio);
		glColor4f(0, 0, 0, alpha);
		drawRect(0, 0, sWidth, sHeight);
	}
	_uiCamera.pop();
}

void GameWinState::onEnd()
{
}

void GameWinState::onMouseMotionEvent(lbMouseMotionEvent* event)
{

}

void GameWinState::onMouseButtonEvent(lbMouseButtonEvent* event)
{

}

void GameWinState::onMouseWheelEvent(lbMouseWheelEvent* event)
{
}

void GameWinState::onKeyboardEvent(lbKeyboardEvent* event)
{
	if(event->key == SDLK_z)
	{
		if(event->type == lbKeyboardEvent::KeyPressed)
		{
			finish();
		}
	}
}

