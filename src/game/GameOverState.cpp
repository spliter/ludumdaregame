/*
 * MenuState.cpp
 *
 *  Created on: 23-08-2013
 *      Author: Spliter
 */

#include "GameOverState.h"

#include <GLee.h>
#include <iostream>
#include <sstream>
#include <FTGL/ftgl.h>
#include <FTGL/FTGLTextureFont.h>
#include <ft2build.h>

#include "GameState.h"

#include "../core/GameCore.h"
#include "../Global.h"
#include "../utils/graphics/drawUtils.h"

#include "../utils/graphics/lbTexture.h"
#include "../utils/resources/lbDefaultResourceManagers.h"
#include "../utils/resources/lbTextureResourceManager.h"
#include "../utils/resources/lbSpriteResourceManager.h"

GameOverState::GameOverState(GameCore* owner):
	State(),
	_owner(owner)
{

}

GameOverState::~GameOverState()
{
}

void GameOverState::onStart()
{
	_uiCamera.vp.set(0, 0, getScreenWidth(), getScreenHeight(), 0, -1, 1);
	_uiCamera.set(0, 0, 0, 0, 0, 0);

	_buttonSprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/z_key.txt");
	_earthSprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/earth.txt");
	_spySprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/spy_run_right_gameover.txt");
	_animTime = 0;
	_isRewinding = false;
	_inputLocked = false;
}

void GameOverState::onResume(State* state)
{
	_inputLocked = false;
}

void GameOverState::onPause(State* state)
{
}

void GameOverState::onFrameUpdate()
{
	if(_isRewinding)
	{
		float scale = 10.0f;
		scale = lerp(3.0f,0.5f,normalize(6,10,_animTime));
		_animTime-=getGameCore().getFrameTimeDifferencef()*scale;
		if(_animTime<0)
		{
			_animTime=0;
			finish();
			startState(new GameState(_owner));
			_inputLocked = true;
			_isRewinding = true;
		}
	}
	else
	{
		_animTime+=getGameCore().getFrameTimeDifferencef();
		if(_animTime>10)
		{
			_isRewinding = true;
		}
	}
}

void GameOverState::onFrameRender()
{
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glLoadIdentity();

	_uiCamera.push();
	float sWidth = getScreenWidth();
	float sHeight = getScreenHeight();

	if(_animTime<3)
	{
		drawSpy(sWidth/2,sHeight/2-100,_animTime);
	}
	else
	{
		drawEarth(sWidth/2,sHeight/2-100,_animTime-3);
	}

	glColor3f(1,1,1);
	float frameWidth = _buttonSprite->getFrameWidth();
	float frameHeight = _buttonSprite->getFrameHeight();


	long curTime = GameCore::getInstance().getCurrentFrameTime();
	int frame = 0;
	if(curTime%4000>2000)
	{
		frame = 1;
	}
	lbTexture::unbind();
	_buttonSprite->renderFrame(frame,(sWidth-frameWidth)/2,(sHeight-frameHeight*2),1.0f,1.0f);
	_uiCamera.pop();
}

void GameOverState::onEnd()
{
}


void GameOverState::onMouseMotionEvent(lbMouseMotionEvent* event)
{
}

void GameOverState::onMouseButtonEvent(lbMouseButtonEvent* event)
{
}

void GameOverState::onMouseWheelEvent(lbMouseWheelEvent* event)
{
}

void GameOverState::onKeyboardEvent(lbKeyboardEvent* event)
{
	if(event->key==SDLK_z)
	{
		if(event->type==lbKeyboardEvent::KeyPressed)
		{
			if(!_inputLocked)
			{
				_isRewinding = true;
//				startState(new GameState(_owner));
//				finish();
				_inputLocked = true;
			}
		}
	}
}

void GameOverState::drawSpy(float x, float y, float animTime)
{

	lbTexture::unbind();
	glColor4f(1.0f,1.0f,1.0f,1.0f);
	drawRect(0,0,getScreenWidth(),getScreenHeight());

	float startX = -200;
	float endX = 200;
	float ratio = normalize(0.0f,3.0f,animTime);
	float curX = lerp(startX,endX,ratio);

	glColor3f(1.0f,1.0f,1.0f);
	_spySprite->renderFrame((int)(animTime*2)%4,x+curX,y,8,8);

	if(animTime>1.5f)
	{
		lbTexture::unbind();
		ratio = normalize(1.5f,3.0f,animTime);
		float alpha = lerp(0.0f,1.0f,ratio);
		glColor4f(1.0f,1.0f,1.0f,alpha);
		drawRect(0,0,getScreenWidth(),getScreenHeight());
	}
}

void GameOverState::drawEarth(float x, float y, float animTime)
{
	float earthX[4]={0,0,0,0};
	float earthY[4]={0,0,0,0};

	float earthDstX[4]={-10,-26,32,0};
	float earthDstY[4]={24,-19,-12,0};

	float explosionStart = 3.0f;

	if(animTime<explosionStart)
	{
		if(animTime<0.5f)
		{
			lbTexture::unbind();
			float ratio = normalize(0.0f,0.5f,animTime);
			float alpha = lerp(1.0f,0.0f,ratio);
			glColor4f(1.0f,1.0f,1.0f,alpha);
			drawRect(0,0,getScreenWidth(),getScreenHeight());
		}

		glColor3f(1.0f,1.0f,1.0f);
		_earthSprite->renderFrame(0,x+earthX[0],y+earthY[0],1,1);
		_earthSprite->renderFrame(3,x+earthX[3],y+earthY[3],1,1);
		_earthSprite->renderFrame(1,x+earthX[1],y+earthY[1],1,1);
		_earthSprite->renderFrame(2,x+earthX[2],y+earthY[2],1,1);
		lbTexture::unbind();
		float ratio = normalize(1.0f,explosionStart-0.5f,animTime);
		float alpha = lerp(0.0f,1.0f,ratio);
		float radius = lerp(1024.0f,2.0f,slowdown(ratio));
		glColor4f(1.0f,1.0f,1.0f,alpha);
		drawCircle(x+30,y+80,radius,32,true);
	}
	else
	{
		float ratio = normalize(explosionStart+0.5,explosionStart+3,animTime);
		float animRatio = slowdown(ratio);

		for(int i=0;i<4;i++)
		{
			earthX[i] = int(earthDstX[i]*animRatio)/2*2;
			earthY[i] = int(earthDstY[i]*animRatio)/2*2;
		}
		glColor3f(1.0f,1.0f,1.0f);
		_earthSprite->renderFrame(0,x+earthX[0],y+earthY[0],1,1);
		_earthSprite->renderFrame(3,x+earthX[3],y+earthY[3],1,1);
		_earthSprite->renderFrame(1,x+earthX[1],y+earthY[1],1,1);
		_earthSprite->renderFrame(2,x+earthX[2],y+earthY[2],1,1);

		if(animTime<explosionStart+2)
		{
			lbTexture::unbind();
			ratio = normalize(explosionStart,explosionStart+2,animTime);
			float alpha = lerp(1.0f,0.0f,ratio);
			glColor4f(1.0f,1.0f,1.0f,alpha);
			drawRect(0,0,getScreenWidth(),getScreenHeight());
		}
	}
}
