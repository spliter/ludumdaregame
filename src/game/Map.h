/*
 * Map.h
 *
 *  Created on: 12-09-2012
 *      Author: Spliter
 */

#pragma once
#include <cstring>
#include "Constants.h"

#define MAX_TILE_CALLBACKS 8

struct Tile
{
	unsigned long tileFlags;

	Tile()
	{
		tileFlags = 0;
	}

	Tile(const Tile& other)
	{
		tileFlags = other.tileFlags;
	}
};

#define TILE_SIZE 32
#define IMPORT_TILE_SIZE 32

struct Map
{
public:
	Tile* tiles;
	int width;
	int height;

	Map(int width, int height)
	{
		this->width = width, this->height = height;
		tiles = new Tile[width*height];
	}

	~Map()
	{
		delete tiles;
	}

	inline Tile getTile(int idX, int idY) const
	{
		if(idX<0||idX>=width||idY<0||idY>=height)
		{
			return Tile();
		}
		return tiles[idX+idY*width];
	}

	inline Tile* getTileRef(int idX, int idY) const
	{
		if(idX<0||idX>=width||idY<0||idY>=height)
		{
			return NULL;
		}
		return &tiles[idX+idY*width];
	}

	inline Tile getTileAtPos(float x, float y) const
	{
		return getTile((int) (x/TILE_SIZE), (int) (y/TILE_SIZE));
	}
};
