/*
 * MenuState.cpp
 *
 *  Created on: 23-08-2013
 *      Author: Spliter
 */

#include "MainMenuState.h"

#include <GLee.h>
#include <iostream>
#include <sstream>
#include <FTGL/ftgl.h>
#include <FTGL/FTGLTextureFont.h>
#include <ft2build.h>

#include "GameState.h"

#include "../core/GameCore.h"
#include "../Global.h"
#include "../utils/graphics/drawUtils.h"

#include "../utils/graphics/lbTexture.h"
#include "../utils/resources/lbDefaultResourceManagers.h"
#include "../utils/resources/lbTextureResourceManager.h"
#include "../utils/resources/lbSpriteResourceManager.h"

MainMenuState::MainMenuState(GameCore* owner):
	State(),
	_owner(owner)
{

}

MainMenuState::~MainMenuState()
{
}

void MainMenuState::onStart()
{
	_uiCamera.vp.set(0, 0, getScreenWidth(), getScreenHeight(), 0, -1, 1);
	_uiCamera.set(0, 0, 0, 0, 0, 0);

	_buttonSprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/z_key.txt");
	_backgroundTexture = lbDefaultResourceManagers::instance().getTextureManager()->getResource("data/graphics/textures/main_menu_screen.png");
}

void MainMenuState::onResume(State* state)
{
	_inputLocked = false;
}

void MainMenuState::onPause(State* state)
{
}

void MainMenuState::onFrameUpdate()
{
}

void MainMenuState::onFrameRender()
{
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glLoadIdentity();

	_uiCamera.push();
	glColor3f(1,1,1);

	float sWidth = getScreenWidth();
	float sHeight = getScreenHeight();

	float backWidth = _backgroundTexture->getWidth();
	float backHeight = _backgroundTexture->getHeight();

	_backgroundTexture->bind();
	float x = (sWidth-backWidth)/2;
	float y = (sHeight-backHeight)/2;
	drawRect(x,y,x+backWidth,y+backWidth);
	_backgroundTexture->unbind();


	float frameWidth = _buttonSprite->getFrameWidth();
	float frameHeight = _buttonSprite->getFrameHeight();


	long curTime = GameCore::getInstance().getCurrentFrameTime();
	int frame = 0;
	if(curTime%2000>1000)
	{
		frame = 1;
	}
	lbTexture::unbind();
	_buttonSprite->renderFrame(frame,(sWidth-frameWidth)/2,(sHeight-frameHeight*3),1.0f,1.0f);
	_uiCamera.pop();
}

void MainMenuState::onEnd()
{
}


void MainMenuState::onMouseMotionEvent(lbMouseMotionEvent* event)
{
}

void MainMenuState::onMouseButtonEvent(lbMouseButtonEvent* event)
{
}

void MainMenuState::onMouseWheelEvent(lbMouseWheelEvent* event)
{
}

void MainMenuState::onKeyboardEvent(lbKeyboardEvent* event)
{
	if(event->key==SDLK_z)
	{
		if(event->type==lbKeyboardEvent::KeyPressed)
		{
			if(!_inputLocked)
			{
				startState(new GameState(_owner));
				_inputLocked = true;
			}
		}
	}
}
