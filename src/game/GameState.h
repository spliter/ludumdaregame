/*
 * GameState.h
 *
 *  Created on: 13-09-2012
 *      Author: Spliter
 */

#pragma once
#include <GLee.h>
#include <map>
#include <string>
#include "Constants.h"

#include "Map.h"
#include "../states/State.h"
#include "../ui/lbTextBox.h"
#include "../ui/UiElementGroup.h"
#include "../utils/lbVariableHolder.h"
#include "../utils/entities/lbEntityManager.h"
#include "../utils/graphics/lbDrawableLayer.h"
#include "../utils/graphics/lbDrawable.h"

#include <utils/graphics/basic/glCamera.h>
#include <utils/graphics/basic/Viewport.h>

#include "GameContext.h"

class GameCore;
class FTFont;
class GameEntity;
class lbTileMap;
class lbSprite;

typedef lbEntityManager<GameEntity> GameEntityManager;



class GameState: public State, public lbGameEventListener
{
public:
	GameState(GameCore *core);
	virtual ~GameState();

	virtual void onStart();
	virtual void onResume(State* state);
	virtual void onPause(State* state);
	virtual void onFrameUpdate();
	virtual void onFrameRender();
	virtual void onEnd();

	virtual void onMouseMotionEvent(lbMouseMotionEvent* event);
	virtual void onMouseButtonEvent(lbMouseButtonEvent* event);
	virtual void onMouseWheelEvent(lbMouseWheelEvent* event);
	virtual void onKeyboardEvent(lbKeyboardEvent* event);

	glCamera* getGameCamera(){return &_gameCamera;}
	glCamera* getUICamera(){return &_gameCamera;}

	lbDrawableLayer *getBackgroundLayer() {return &_backLayer;}
	lbDrawableLayer *getCharacterLayer() {return &_characterLayer;}
	lbDrawableLayer *getForegroundLayer() {return &_foregroundLayer;}
	lbDrawableLayer *getUILayer() {return &_uiLayer;}

	int spawnEntity(GameEntity* entity);

	GameEntity* findEntityByName(std::string name) const;
	GameEntity* getEntity(int id) const;
	GameEntity* getEntity(int id, int uniqueId) const;
	GameEntity* getEntityUid(int uniqueId) const;
	GameEntityManager* getEntityManager(){return &_entManager;}

	GameContext* getGameContext(){return &_gameContext;}

	Map* getCurrentMap(){return _currentMap;}
	FTFont* getBigFont(){return _bigFont;}
	UiElementGroup* getUIRoot(){return &_uiRoot;}

	bool onCoolantUsed(std::string coolantName);//returns true if jammed
	void startCountdown(){if(!_countdownStopped){_countdownStarted = true;}}
	void stopCountdown(){_countdownStarted = false;_countdownStopped = true;}
	void pauseCountdown(){_countdownStarted = false;}
private:
	void loadMapFromXml(std::string levelName);
	GameEntity* createEntity(std::string type);

	void drawTimer();

	GameCore* _gameCore;
	glCamera _uiCamera;
	glCamera _gameCamera;

	unsigned long _curGameTime;
	GameContext _gameContext;

	long _curSecondStart;
	int _framesEllapsedInCurSecond;
	float _fps;
	float _gameSpeed;



	float _outroTime;
	bool _isPlayingOutro;

	lbSprite* _timerSprite;
	long _timeLeft;
	bool _countdownStarted;
	bool _countdownStopped;
	bool _isSaved;
	int _coolantFlags;
	bool _coolantsJammed;
	//
	Map* _currentMap;
	bool _isLoaded;
	typedef std::list<lbTileMap*>::iterator TileMapIterator;
	std::list<lbTileMap*> _tileMaps;

	//game entities
	GameEntityManager _entManager;

	//ui
	UiElementGroup _uiRoot;
//	lbTextBox _fpsTextBox;
	FTFont* _messageFont;
	FTFont* _bigFont;

	//rendering
	lbDrawableLayer _backLayer;
	lbDrawableLayer _characterLayer;
	lbDrawableLayer _foregroundLayer;
	lbDrawableLayer _uiLayer;
};
