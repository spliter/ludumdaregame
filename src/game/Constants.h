/*
 * GameConstants.h
 *
 *  Created on: 01-12-2012
 *      Author: Spliter
 */

#pragma once

namespace GameConst
{
	enum TileFlags
	{
		TFSolid=0x0001,
		TFEmpty=0x0000
	};

	enum GameState
	{
		GSOverworld,GSKeypad,GSComputer,GSDocuments
	};
};

namespace GameUtil
{
	enum Direction
	{
		dLeft, dRight, dUp, dDown, dUnknown
	};

	inline void getPositionFromDirection(Direction direction, int& x, int& y)
	{
		x=0;
		y=0;
		switch(direction)
		{
			case dLeft:x=-1;y=0;break;
			case dRight:x=1;y=0;break;
			case dUp:x=0;y=-1;break;
			case dDown:x=0;y=1;break;
		}
	}

	inline Direction getDirectionFromPosition(float x, float y)
	{
		bool xby = x>y;
		bool mxby = x>-y;
		if(xby)
		{
			if(mxby)
			{
				return dRight;
			}
			else
			{
				return dUp;
			}
		}
		else
		{
			if(mxby)
			{
				return dDown;
			}
			else
			{
				return dLeft;
			}
		}
	}

	inline unsigned int rgbaFtoI(float r, float g, float b, float a)
	{
		int color = 0;
		color |= (int(r * 255) & 0xff) << 24;
		color |= (int(g * 255) & 0xff) << 16;
		color |= (int(b * 255) & 0xff) << 8;
		color |= (int(a * 255) & 0xff);
		return color;
	}

	inline void rgbaItoF(int color, float *r, float *g, float *b, float *a)
	{
		*r = ((color >> 24) & 0xff) / 255.0f;
		*g = ((color >> 16) & 0xff) / 255.0f;
		*b = ((color >> 8) & 0xff) / 255.0f;
		*a = ((color) & 0xff) / 255.0f;
	}


};
