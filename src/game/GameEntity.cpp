/*
 * GameEntity.cpp
 *
 *  Created on: 23-08-2013
 *      Author: Spliter
 */

#include "GameEntity.h"
#include <utils/file/TinyXml/tinyxml.h>
#include "Map.h"

static bool iequals(const std::string& a, const std::string& b)
{
	unsigned int sz = a.size();
	if(b.size() != sz)
		return false;
	for(unsigned int i = 0; i < sz; ++i)
		if(tolower(a[i]) != tolower(b[i]))
			return false;
	return true;
}

TiXmlElement* findProperty(TiXmlElement* rootElement, std::string propertyName)
{
	TiXmlElement* properties = rootElement->FirstChildElement("properties");
	if(properties)
	{
		TiXmlElement* property = properties->FirstChildElement("property");
		while(property)
		{
			std::string name;
			int result = property->QueryStringAttribute("name", &name);
			if(result == TIXML_SUCCESS && name == propertyName)
			{
				return property;
			}
			property = property->NextSiblingElement("property");
		}
	}
	return NULL;
}

bool GameEntity::parseBoolProperty(TiXmlElement* element, std::string propertyName, bool* retValue)
{
	TiXmlElement* property = findProperty(element, propertyName);
	if(property)
	{
		std::string value;
		if(property->QueryStringAttribute("value", &value) == TIXML_SUCCESS)
		{
			(*retValue) = iequals(value, std::string("true"));
			return true;
		}
	}
	return false;
}

bool GameEntity::parseIntProperty(TiXmlElement* element, std::string propertyName, int* retValue)
{
	TiXmlElement* property = findProperty(element, propertyName);
	if(property)
	{
		return property->QueryIntAttribute("value", retValue) == TIXML_SUCCESS;
	}
	return false;
}

bool GameEntity::parseFloatProperty(TiXmlElement* element, std::string propertyName, float* retValue)
{
	TiXmlElement* property = findProperty(element, propertyName);
	if(property)
	{
		return property->QueryFloatAttribute("value", retValue) == TIXML_SUCCESS;
	}
	return false;
}

bool GameEntity::parseStringProperty(TiXmlElement* element, std::string propertyName, std::string* retValue)
{
	TiXmlElement* property = findProperty(element, propertyName);
	if(property)
	{
		return property->QueryStringAttribute("value", retValue) == TIXML_SUCCESS;
	}
	return false;
}

GameEntity::GameEntity(GameContext* gameContext) :
	_id(0), _uniqueId(0), _isKilled(false), _prev(NULL), _next(NULL), _pos(0, 0), _size(0, 0), _name(""), _gameContext(gameContext)
{

}

GameEntity::~GameEntity()
{
}

void GameEntity::onSetupFromXmlElement(TiXmlElement* element)
{
	int x = 0, y = 0;
	element->QueryIntAttribute("x", &x);
	element->QueryIntAttribute("y", &y);
	element->QueryStringAttribute("name", &_name);

	int gid = 0;
	element->QueryIntAttribute("gid", &gid);
	//if we have a tile assigned then we must compensate for the fact that tiled entities in TilEd have a lower-left corner as the origin
	if(gid!=0)
	{
		y-=IMPORT_TILE_SIZE;//we must compensate for the fact that someone decided the origin of objects in TilEd is in lower left corner than the upper left
	}
	/*
	 * note: we adjust the position by half of the tile size
	 * so the middle of the icon in TilEd determines the cell
	 * the object is in and not the top left corner
	 */
	setPosition(lbVec2f(x, y));

	//	parseFloatProperty(element,"x",&_pos.x);
	//	parseFloatProperty(element,"y",&_pos.y);
	//	parseStringProperty(element,"name",&_name);
	//	_pos.x*=32;
	//	_pos.y*=32;
}

