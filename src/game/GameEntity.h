/*
 * GameEntity.h
 *
 *  Created on: 23-08-2013
 *      Author: Spliter
 */

#pragma once

#include <string>
#include <math/lbVec2f.h>
#include "GameContext.h"
#include "../utils/entities/lbEntityManager.h"
#include "../utils/graphics/lbDrawable.h"
#include <engine/lbGameEvent.h>

class TiXmlElement;

struct t_GameEntDef
{
	t_GameEntDef(std::string _className, t_GameEntDef* _parent){className=_className;parent=_parent;}
	std::string getClassName(){return className;}
	t_GameEntDef* getParent(){return parent;}
	bool inheritsFrom(t_GameEntDef* entDef){return (entDef==this) || (parent&&parent->inheritsFrom(entDef));}
	bool inheritsFromName(std::string _className){return (_className==className) || (parent&&parent->inheritsFromName(className));}
private:
	t_GameEntDef *parent;
	std::string className;
};

typedef t_GameEntDef *GameEntDef;

class GameEntity: public lbDrawable,public lbGameEventListener
{
public:
	static GameEntDef getClassDef(){static t_GameEntDef entDef("GameEntity",NULL); return &entDef;}
	static GameEntDef getParentClass(){return GameEntity::getClassDef()->getParent();}
	static std::string getClassName(){return GameEntity::getClassDef()->getClassName();}
	static std::string getParentClassName(){return std::string("");}
	static GameEntity* castEntity(GameEntity* ent, GameEntDef classDef){ if(ent && ent->instanceOf(classDef))return ent;return NULL;}//returns an entity only if it inherits from this class
	template <typename T> static T* castEntity(GameEntity* ent){ if(ent && ent->instanceOf(T::getClassDef()))return (T*)ent;return NULL;}
	static bool classInheritsFrom(GameEntDef classDef){return (classDef==GameEntity::getClassDef());}
	virtual GameEntDef getClass(){return GameEntity::getClassDef();}
	virtual bool instanceOf(GameEntDef classDef){return getClass()->inheritsFrom(classDef);}

	template <typename T> bool instanceOf(){return getClass()->inheritsFrom(T::getClassDef());}


	static bool parseBoolProperty(TiXmlElement* element, std::string propertyName, bool* retValue);
	static bool parseIntProperty(TiXmlElement* element, std::string propertyName, int* retValue);
	static bool parseFloatProperty(TiXmlElement* element, std::string propertyName, float* retValue);
	static bool parseStringProperty(TiXmlElement* element, std::string propertyName, std::string* retValue);

public:
	GameEntity(GameContext* gameContext);
	virtual ~GameEntity();

	virtual void onCreated(){};
	virtual void onSetupFromXmlElement(TiXmlElement* element);
	virtual void onSpawned(){};
	virtual void onKilled(){};
	virtual void onDestroy(){};

	virtual void update(float dt){}
	virtual void draw(lbRectI camera){};

	void setPosition(float x, float y){_pos.x=x;_pos.y=y;setRenderArea(_pos.x,_pos.y,_pos.x+_size.x,_pos.y+_size.y);}
	void setPosition(lbVec2f pos){setPosition(pos.x,pos.y);}
	void setSize(float width, float height){_size.x=width;_size.y=height;setRenderArea(_pos.x,_pos.y,_pos.x+_size.x,_pos.y+_size.y);}
	void setSize(lbVec2f size){setSize(size.x,size.y);}
	lbVec2f getPosition(){return _pos;}
	lbVec2f getSize(){return _size;}

	virtual void onUse(GameEntity* other){};

	std::string getName(){return _name;}
	void setName(std::string newName){_name = newName;}

	GameContext* getGameContext(){return _gameContext;}

	void kill(){_isKilled = true;onKilled();}

	GameEntity* getPrevious(){return _prev;}
	GameEntity* getNext(){return _next;}

	int getIndexId(){return _id;}
	int getUniqueId(){return _uniqueId;}

private:
	friend class lbEntityManager<GameEntity>;
	int _id;
	int _uniqueId;
	bool _isKilled;
	GameEntity* _prev;
	GameEntity* _next;
	lbVec2f _pos;
	lbVec2f _size;
	std::string _name;
	GameContext* _gameContext;
};


#define DEFINE_GAME_ENTITY(className, parentClassName)\
	public:\
		static GameEntDef getClassDef(){static t_GameEntDef entDef(#className,parentClassName::getClassDef()); return &entDef;}\
		static GameEntDef getParentClass(){return className::getClassDef()->getParent();}\
		static std::string getClassName(){return className::getClassDef()->getClassName();}\
		static std::string getParentClassName(){return parentClassName::getClassName();}\
		static className* castEntity(GameEntity* ent){ return ent?(ent->instanceOf(className::getClassDef())?(className*)ent:NULL):NULL;}\
		static bool classInheritsFrom(GameEntDef classDef){return className::getClassDef()->inheritsFrom(classDef);}\
		virtual GameEntDef getClass(){return className::getClassDef();}
