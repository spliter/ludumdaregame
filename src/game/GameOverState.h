/*
 * MenuState.h
 *
 *  Created on: 23-08-2013
 *      Author: Spliter
 */

#pragma once
#include <GLee.h>
#include <map>
#include <string>
#include "Constants.h"

#include "../states/State.h"
#include "../ui/UiButton.h"

#include <utils/graphics/basic/glCamera.h>
#include <utils/graphics/lbSprite.h>
#include "GameContext.h"


class GameCore;
class GameState;
class lbTexture;

class GameOverState: public State
{
public:
	GameOverState(GameCore* owner);
	virtual ~GameOverState();

	virtual void onStart();
	virtual void onResume(State* state);
	virtual void onPause(State* state);
	virtual void onFrameUpdate();
	virtual void onFrameRender();
	virtual void onEnd();

	virtual void onMouseMotionEvent(lbMouseMotionEvent *event);
	virtual void onMouseButtonEvent(lbMouseButtonEvent *event);
	virtual void onMouseWheelEvent(lbMouseWheelEvent *event);
	virtual void onKeyboardEvent(lbKeyboardEvent *event);

private:
	void drawSpy(float x, float y, float animTime);
	void drawEarth(float x, float y, float animTime);
	glCamera _uiCamera;
	GameCore* _owner;

	bool _inputLocked;

	lbTexture* _startButtonPressedTexture;
	lbTexture* _startButtonReleasedTexture;

	lbSprite* _buttonSprite;

	lbSprite* _earthSprite;
	lbSprite* _spySprite;

	float _animTime;

	bool _isRewinding;
};

