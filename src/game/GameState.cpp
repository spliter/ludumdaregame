/*
 * GameState.cpp
 *
 *  Created on: 13-09-2012
 *      Author: Spliter
 */

#include "GameState.h"

#include <GLee.h>
#include <iostream>
#include <sstream>
#include <FTGL/ftgl.h>
#include <FTGL/FTGLTextureFont.h>
#include <ft2build.h>

#include "Constants.h"
#include "GameEntity.h"
#include "GameOverState.h"
#include "GameWinState.h"
#include "../core/GameCore.h"
#include "../Global.h"
#include "../utils/graphics/drawUtils.h"
#include "../utils/graphics/lbSprite.h"
#include "../utils/graphics/lbTileMap.h"
#include "../utils/graphics/lbTexture.h"
#include "../utils/resources/lbResourceManager.h"
#include "../utils/resources/lbDefaultResourceManagers.h"
#include "../utils/resources/lbTextureResourceManager.h"
#include "../utils/resources/lbSpriteResourceManager.h"
#include "../utils/file/TinyXml/tinyxml.h"
#include <strstream>
#include <utils/debug/Logger.h>

#include "entities/ComputerEntity.h"
#include "entities/CoolantEntity.h"
#include "entities/CoreEntity.h"
#include "entities/DocumentsEntity.h"
#include "entities/DoorEntity.h"
#include "entities/GuardEntity.h"
#include "entities/PlayerEntity.h"
#include "entities/ScientistEntity.h"

enum
{
	Coolant1Inserted = 0x01, Coolant2Inserted = 0x02, Coolant3Inserted = 0x04, Coolant4Inserted = 0x08,
};

#define LogMessage( message ) std::cout<<__FILE__<<"@"<<__LINE__<<" : "<<message<<std::endl
//#define LogMessage( message )

GameState::GameState(GameCore *core) :
	State(), _entManager(2048)
{
	_gameCore = core;
	_timeLeft = 10000;
	_coolantFlags = 0;
	_countdownStarted = false;
	_countdownStopped = false;

	_curGameTime=0;
	_curSecondStart=0;
	_framesEllapsedInCurSecond=0;
	_fps=0;
	_gameSpeed=1.0f;
	_isSaved = false;
	_coolantFlags=0;
	_coolantsJammed = false;
	_isLoaded= false;
}

GameState::~GameState()
{
}

void GameState::onStart()
{

	_outroTime = 0.0f;
	_isPlayingOutro = false;

	_gameSpeed = 1.0f;
	_uiCamera.vp.set(0, 0, getScreenWidth(), getScreenHeight(), 0, -1, 1);
	_uiCamera.set(0, 0, 0, 0, 0, 0);

	_gameCamera.vp.set(0, 0, getScreenWidth(), getScreenHeight(), 0, -1, 1);
	_gameCamera.set(0, 0, 0, 0, 0, 0);

	_messageFont = new FTGLTextureFont("data/fonts/alphbeta.ttf");
	if(_messageFont->Error())
	{
		std::cout << "Failure when loading font: " << std::hex << _messageFont->Error() << std::dec << std::endl;
	}

	if(!_messageFont->FaceSize(16))
	{
		std::cout << "Failure setting font face size, error :" << std::hex << _messageFont->Error() << std::dec << std::endl;
	}

	_bigFont = new FTGLTextureFont("data/fonts/alphbeta.ttf");
	if(_bigFont->Error())
	{
		std::cout << "Failure when loading font: " << std::hex << _bigFont->Error() << std::dec << std::endl;
	}

	if(!_bigFont->FaceSize(48))
	{
		std::cout << "Failure setting font face size, error :" << std::hex << _bigFont->Error() << std::dec << std::endl;
	}

	_uiRoot.setup();

	//	_fpsTextBox.setText("Basic Text Box");
	//	_fpsTextBox.setPosition(0, 0);
	//	_fpsTextBox.setFont(_messageFont);
	//	_uiRoot.addElement(&_fpsTextBox);

	_curSecondStart = GameCore::getInstance().getCurrentFrameTime();
	_framesEllapsedInCurSecond = 0;
	_fps = 0;

	_curGameTime = 0;

	_gameContext.gameCore = &GameCore::getInstance();
	_gameContext.gameState = this;

	_entManager.reset();

	loadMapFromXml("reactor");
	_gameCore->enableFPSLimit(30);
//	_gameCore->disableFPSLimit();
	_timerSprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource("data/graphics/sprites/timer.txt");
}

void GameState::onResume(State* state)
{

}

void GameState::onPause(State* state)
{

}

void GameState::onFrameUpdate()
{
	unsigned long frameDiffMillis = GameCore::getInstance().getFrameTimeDifference();
	float frameDif = ((float) frameDiffMillis / 1000.0f) * _gameSpeed;
	_curGameTime += frameDiffMillis;

	_framesEllapsedInCurSecond++;

	if(_isPlayingOutro)
	{
		_outroTime+=frameDif;
		if(_outroTime>10.0f)
		{
			finish();
			startState(new GameWinState(_gameCore));
		}
	}

	if(_countdownStarted)
	{
		long prevTimeLeft = _timeLeft;
		_timeLeft -= frameDiffMillis * 0.8f;
		if(_timeLeft <= 0)
		{
			_timeLeft = 0;
			if(prevTimeLeft > 0)
			{
				if(!_isSaved)
				{
					startState(new GameOverState(_gameCore));
					finish();
				}
			}
		}
	}

	long curTime = GameCore::getInstance().getCurrentFrameTime();
	if(curTime - _curSecondStart > 1000)
	{
		double secTime = double(curTime - _curSecondStart) / 1000.0;

		_fps = double(_framesEllapsedInCurSecond) / secTime;

		char fpsStr[100];
		fpsStr[0] = 0;
		sprintf(fpsStr, "FPS: %6.2f", _fps);

		//		_fpsTextBox.setText(fpsStr);
		_curSecondStart = curTime;
		_framesEllapsedInCurSecond = 0;
	}
	GameEntity* curEnt = _entManager.getHeadEntity();
	while(curEnt)
	{
		curEnt->update(frameDif);
		curEnt = curEnt->getNext();
	}
	_uiRoot.update(frameDiffMillis);
	_entManager.deleteKilledEntities();
}

void GameState::onFrameRender()
{
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	glLoadIdentity();

	lbRectI gameCameraRect;
	gameCameraRect.set(_gameCamera.x, _gameCamera.y, _gameCamera.x + _gameCamera.vp.w, _gameCamera.y + _gameCamera.vp.h);
	_gameCamera.push();
	_backLayer.draw(gameCameraRect);
	_characterLayer.draw(gameCameraRect);
	_foregroundLayer.draw(gameCameraRect);
	_gameCamera.pop();

	lbRectI uiCameraRect;
	uiCameraRect.set(_uiCamera.x, _uiCamera.y, _uiCamera.x + _uiCamera.vp.w, _uiCamera.y + _uiCamera.vp.h);
	_uiCamera.push();
	_uiLayer.draw(uiCameraRect);
	_uiRoot.draw();
	drawTimer();
	if(_isPlayingOutro)
	{
		lbTexture::unbind();
		float ratio = normalize(6,10,_outroTime);
		float alpha = lerp(0,1,ratio);
		glColor4f(0,0,0,alpha);
		drawRect(0,0,getScreenWidth(),getScreenHeight());
	}
	_uiCamera.pop();
}

void GameState::onEnd()
{
	_entManager.deleteAllEntities();
	delete _messageFont;
}

void GameState::onMouseMotionEvent(lbMouseMotionEvent* event)
{

}

void GameState::onMouseButtonEvent(lbMouseButtonEvent* event)
{

}

void GameState::onMouseWheelEvent(lbMouseWheelEvent* event)
{

}

void GameState::onKeyboardEvent(lbKeyboardEvent *event)
{
//	if(event->key==SDLK_ESCAPE)
//	{
//		if(event->type==lbKeyboardEvent::KeyPressed)
//		{
//			finish();
//			startState(new GameState(_gameCore));
//		}
//	}
//	else if(event->key==SDLK_F1)
//	{
//		if(event->type==lbKeyboardEvent::KeyPressed)
//		{
//			finish();
//			startState(new GameOverState(_gameCore));
//		}
//	}
}

int GameState::spawnEntity(GameEntity* entity)
{
	int id = _entManager.addEntity(entity);
	entity->onSpawned();
	return id;
}

GameEntity* GameState::findEntityByName(std::string name) const
{
	GameEntity* curEnt = _entManager.getHeadEntity();
	while(curEnt)
	{
		if(curEnt->getName() == name)
		{
			return curEnt;
		}
		curEnt = curEnt->getNext();
	}
	return NULL;
}

GameEntity* GameState::getEntity(int id) const
{
	return _entManager.getEntity(id);
}

GameEntity* GameState::getEntity(int id, int uniqueId) const
{
	GameEntity* ent = _entManager.getEntity(id);
	if(ent && ent->getUniqueId() == uniqueId)
	{
		return ent;
	}
	return NULL;
}

GameEntity* GameState::getEntityUid(int uniqueId) const
{
	return _entManager.getEntityUid(uniqueId);
}

bool GameState::onCoolantUsed(std::string coolantName)
{
	std::cout << "Coolant Used: " << coolantName << std::endl;
	if(!_coolantsJammed)
	{
		if(coolantName == "coolant1")
		{
			if(_coolantFlags == 0)
			{
				_coolantFlags |= Coolant1Inserted;
				return false;
			}
			else
			{
				return true;
			}
		}
		else if(coolantName == "coolant3")
		{
			if(_coolantFlags & Coolant1Inserted)
			{
				_coolantFlags |= Coolant3Inserted;
				return false;
			}
			else
			{
				return true;
			}
		}
		else if(coolantName == "coolant4")
		{
			if(_coolantFlags & Coolant3Inserted)
			{
				_coolantFlags |= Coolant4Inserted;
				return false;
			}
			else
			{
				return true;
			}
		}
		else if(coolantName == "coolant2")
		{
			if(_coolantFlags & Coolant4Inserted)
			{
				_coolantFlags |= Coolant2Inserted;

				std::cout << "You've won the game!\n";
				_isSaved = true;

				CoreEntity* core = GameEntity::castEntity<CoreEntity>(findEntityByName("core"));
				if(core)
				{
					core->coolDown();
				}
				stopCountdown();
				_isPlayingOutro = true;
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	return true;
}

////////////////////////map load Helper Functions

struct TileSet
{
	int tileWidth, tileHeight;
	int firstId;
	int width, height;
	int columns, rows;
	std::string textureFname;
	lbTexture* texture;

	TileSet() :
		tileWidth(0), tileHeight(0), firstId(-1), width(0), height(0), columns(0), rows(0), texture(NULL)
	{

	}

	void setup(int _firstId, int _tileWidth, int _tileHeight, int _width, int _height, std::string _textureFname)
	{
		firstId = _firstId;
		width = _width;
		height = _height;
		tileWidth = _tileWidth;
		tileHeight = _tileHeight;
		columns = _width / _tileWidth;
		rows = _height / _tileHeight;
		textureFname = _textureFname;
	}

	void getTileCoord(int tileId, int &retX, int &retY)
	{
		retX = (tileId - firstId) % columns;
		retY = (tileId - firstId) / columns;
	}
};

int getTilesetUsed(TileSet* tileSets, int sampleTileId)
{
	int highestFirstId = -1;
	int usedId = -1;
	for(int i = 0; tileSets[i].firstId != -1; i++)
	{
		if(tileSets[i].firstId < sampleTileId && tileSets[i].firstId > highestFirstId)
		{
			highestFirstId = tileSets[i].firstId;
			usedId = i;
		}
	}
	return usedId;
}

TileSet getTileSetFromXmlElement(TiXmlElement* element, std::string levelDir)
{
	TileSet tileset;
	int tileWidth, tileHeight, firstId;
	int imageWidth, imageHeight;
	std::string texture;

	if(element->QueryIntAttribute("tilewidth", &tileWidth) != TIXML_SUCCESS)
	{
		tileWidth = 8;
		LogMessage("Failed to query tileWidth");
	}

	if(element->QueryIntAttribute("tileheight", &tileHeight) != TIXML_SUCCESS)
	{
		tileHeight = 8;
		LogMessage("Failed to query tileHeight");
	}

	if(element->QueryIntAttribute("firstgid", &firstId) != TIXML_SUCCESS)
	{
		LogMessage("Failed to query firstId");
		firstId = 0;
	}

	TiXmlElement* imageElement = element->FirstChildElement("image");
	if(imageElement)
	{
		std::string fname;

		if(imageElement->QueryIntAttribute("width", &imageWidth) != TIXML_SUCCESS)
		{
			imageWidth = 8;
			LogMessage("Failed to query imageWidth");
		}
		if(imageElement->QueryIntAttribute("height", &imageHeight) != TIXML_SUCCESS)
		{
			imageHeight = 8;
			LogMessage("Failed to query imageheight");
		}
		if(imageElement->QueryStringAttribute("source", &fname) != TIXML_SUCCESS)
		{
			fname = "";
			LogMessage("Failed to query image filename");
		}
		else
		{
			std::string texFname = fname;
			fname = levelDir + "/";
			fname.append(texFname);
		}

		texture = fname;
	}
	else
	{
		imageWidth = 8;
		imageHeight = 8;
		texture = "";
	}

	tileset.setup(firstId, tileWidth, tileHeight, imageWidth, imageHeight, texture);

	return tileset;
}

lbTileMap* createTileMapFromXmlElement(TiXmlElement* element, TileSet* tilesets)
{
	int collumns, rows;
	bool failed = false;

	if(element->QueryIntAttribute("height", &rows) != TIXML_SUCCESS)
	{
		failed = true;
		LogMessage("Failed to query map height");
	}

	if(element->QueryIntAttribute("width", &collumns) != TIXML_SUCCESS)
	{
		failed = true;
		LogMessage("Failed to query map width");
	}
	TiXmlElement* dataElement = element->FirstChildElement("data");

	lbTileMap* tileMap = NULL;

	if(dataElement && !failed)
	{

		const char* tileData;
		tileData = dataElement->GetText();
		std::string encoding;
		dataElement->QueryStringAttribute("encoding", &encoding);
		if(encoding == "csv")
		{
			int tileId = 0;
			std::string str(tileData);
			std::stringstream ss(str);

			int curTileset = -1;
			char peeked;
			int curTile = 0;

			while(!ss.fail() && curTile < collumns * rows)
			{
				peeked = ss.peek();
				while((peeked < '0' || peeked > '9') && peeked != '-' && peeked != '+')
				{
					ss.ignore();
					peeked = ss.peek();
				}
				ss >> tileId;
				if(tileId != 0)
				{
					curTileset = getTilesetUsed(tilesets, tileId);
					break;
				}
				curTile++;
			}

			tileMap = new lbTileMap(collumns, rows, TILE_SIZE, TILE_SIZE);
			lbTileMap::Tile *tiles = tileMap->getTiles();
			if(curTileset != -1)
			{
				int tcx, tcy;
				curTile = 0;

				std::stringstream ss(str);

				while(!ss.fail() && curTile < collumns * rows)
				{
					peeked = ss.peek();
					while((peeked < '0' || peeked > '9') && peeked != '-' && peeked != '+')
					{
						ss.ignore();
						peeked = ss.peek();
					}
					ss >> tileId;

					tilesets[curTileset].getTileCoord(tileId, tcx, tcy);
					if(tileId == 0 || tcx < 0 || tcx > tilesets[curTileset].columns || tcy < 0 || tcy > tilesets[curTileset].rows)
					{
						tcx = 255;
						tcy = 255;
					}
					tiles[curTile].renderInfo = lbTileMap::getTileTextureCoord(tcx, tcy);

					curTile++;
				}
				tileMap->setTexture(tilesets[curTileset].texture, tilesets[curTileset].columns, tilesets[curTileset].rows);
			}
			else
			{
				LogMessage("No tileset is used for tileid "<<tileId);
				int invisibleTileRenderInfo = lbTileMap::getTileTextureCoord(255, 255);
				for(int i = 0; i < collumns * rows; i++)
				{
					tiles[i].renderInfo = invisibleTileRenderInfo;
				}
				//failed = true;
			}
		}
		else
		{
			LogMessage("Encoding is wrong");
			failed = true;
		}
	}

	if(tileMap && failed)
	{
		LogMessage("Loading map failed");
		delete tileMap;
		tileMap = NULL;
	}

	return tileMap;
}

Map* createMapFromXmlElement(TiXmlElement* element, int solidTileId)
{
	int collumns, rows;
	bool failed = false;

	if(element->QueryIntAttribute("height", &rows) != TIXML_SUCCESS)
	{
		failed = true;
		LogMessage("Loading collision map height failed");
	}

	if(element->QueryIntAttribute("width", &collumns) != TIXML_SUCCESS)
	{
		failed = true;
		LogMessage("Loading collision map width failed");
	}
	TiXmlElement* dataElement = element->FirstChildElement("data");

	Map* map = NULL;

	if(dataElement && !failed)
	{
		const char* tileData;
		tileData = dataElement->GetText();
		std::string encoding;
		dataElement->QueryStringAttribute("encoding", &encoding);
		if(encoding == "csv")
		{
			int tileId = 0;
			std::string str(tileData);
			std::stringstream ss(str);

			int curTile = 0;

			map = new Map(collumns, rows);

			while(!ss.fail())
			{
				ss >> tileId;
				if(solidTileId != 0)
				{
					map->tiles[curTile].tileFlags = (tileId == solidTileId ? GameConst::TFSolid : GameConst::TFEmpty);
				}
				else
				{
					map->tiles[curTile].tileFlags = (tileId ? GameConst::TFSolid : GameConst::TFEmpty);
				}
				curTile++;
				if(ss.peek() == ',')
					ss.ignore();
			}
		}
		else
		{
			LogMessage("Wrong encoding used");
			failed = true;
		}
	}

	if(map && failed)
	{
		LogMessage("Loading collision map failed");
		delete map;
		map = NULL;
	}

	return map;
}

//


void GameState::loadMapFromXml(std::string levelName)
{
	const int MaxTileSets = 10;
	int tileSetNumber = 0;
	TileSet tileSets[MaxTileSets + 1];
	int solidTileId = 0;
	int playerSpawnX = -1, playerSpawnY = -1;

	std::string fname = buildLevelFilename(levelName);
	std::string logicFname = "";

	TiXmlDocument doc(fname);
	if(doc.LoadFile())
	{
		TiXmlElement* mapElement = doc.FirstChildElement("map");
		if(mapElement)
		{
			std::string levelDir = retrieveDirectory(fname);
			//			size_t pos = levelFile.rfind('/', levelFile.length() - 1);
			//			if(pos != std::string::npos)
			//			{
			//				levelDir = levelFile.substr(0, pos);
			//			}
			//			else
			//			{
			//				size_t pos = levelFile.rfind('\\', levelFile.length() - 1);
			//				if(pos != std::string::npos)
			//				{
			//					levelDir = levelFile.substr(0, pos);
			//				}
			//			}
			std::cout << "Level directory: " << levelDir << std::endl;

			TiXmlElement* tileSetElement = mapElement->FirstChildElement("tileset");
			while(tileSetElement)
			{
				tileSets[tileSetNumber] = getTileSetFromXmlElement(tileSetElement, levelDir);

				std::string name;
				int result;
				result = tileSetElement->QueryStringAttribute("name", &name);
				if(name == "util_tileset")
				{
					solidTileId = tileSets[tileSetNumber].firstId + 0;
				}
				else
				{
					tileSets[tileSetNumber].texture = lbDefaultResourceManagers::instance().getTextureManager()->getResource(tileSets[tileSetNumber].textureFname.c_str());
				}

				tileSetElement = tileSetElement->NextSiblingElement("tileset");
				if(tileSets[tileSetNumber].firstId >= 0)
					tileSetNumber++;
			}

			TiXmlElement* layerElement = mapElement->FirstChildElement("layer");
			while(layerElement)
			{
				std::string name;
				int result;
				result = layerElement->QueryStringAttribute("name", &name);
				if(result == TIXML_SUCCESS)
				{
					if(name.substr(0, std::string("Background").size()) == "Background")
					{
						lbTileMap* tileMap = createTileMapFromXmlElement(layerElement, tileSets);
						if(tileMap)
						{
							_tileMaps.push_back(tileMap);
							_backLayer.addDrawable(tileMap);
						}
					}
					else if(name.substr(0, std::string("Foreground").size()) == "Foreground")
					{
						lbTileMap* tileMap = createTileMapFromXmlElement(layerElement, tileSets);
						if(tileMap)
						{
							_tileMaps.push_back(tileMap);
							_foregroundLayer.addDrawable(tileMap);
						}
					}
					else if(name == "Collision")
					{
						_currentMap = createMapFromXmlElement(layerElement, solidTileId);
					}
				}

				layerElement = layerElement->NextSiblingElement("layer");
			}

			TiXmlElement* objectGroupElement = mapElement->FirstChildElement("objectgroup");
			while(objectGroupElement)
			{
				TiXmlElement* objectElement = objectGroupElement->FirstChildElement("object");
				while(objectElement)
				{
					std::string type;
					int result = objectElement->QueryStringAttribute("type", &type);
					if(result == TIXML_SUCCESS)
					{
						if(type == "player_spawn")
						{
							int pSpawnX = -1;
							int pSpawnY = -1;
							int result1 = objectElement->QueryIntAttribute("x", &pSpawnX);
							int result2 = objectElement->QueryIntAttribute("y", &pSpawnY);

							if(result1 != TIXML_SUCCESS || result2 != TIXML_SUCCESS)
							{
								playerSpawnX = -1;
								playerSpawnY = -1;
							}
							else
							{
								playerSpawnX = pSpawnX;
								playerSpawnY = (pSpawnY - IMPORT_TILE_SIZE);
							}LogMessage("Added player spawn on "<<playerSpawnX<<" , "<<playerSpawnY);
						}
						else
						{
							GameEntity* entity = createEntity(type);
							if(entity)
							{
								LogMessage("Created an entity of type "<<type);
								int id = _entManager.addEntity(entity);
								entity->onSetupFromXmlElement(objectElement);
							}
						}
					}
					objectElement = objectElement->NextSiblingElement("object");
				}
				objectGroupElement = objectGroupElement->NextSiblingElement("objectgroup");
			}
		}
	}
	else
	{
		LogMessage("Failed to find file "<<fname);
	}

	doc.Clear();

	if(_currentMap)
	{
		_isLoaded = true;
		if(playerSpawnX > 0 || playerSpawnY > 0)
		{
			GameEntity* entity = createEntity("player_entity");
			if(entity)
			{
				entity->setPosition(playerSpawnX, playerSpawnY);
				_entManager.addEntity(entity);
			}
		}

		GameEntity* ent = _entManager.getHeadEntity();
		while(ent)
		{
			ent->onSpawned();
			ent = ent->getNext();
		}

	}
	else
	{
		_entManager.deleteAllEntities();
		_isLoaded = false;
		if(_currentMap)
		{
			delete _currentMap;
		}
	}
}

GameEntity* GameState::createEntity(std::string type)
{
	if(type == "player_entity")
	{
		return new PlayerEntity(getGameContext());
	}
	else if(type == "guard_entity")
	{
		//		return new GuardEntity(getGameContext());
	}
	else if(type == "scientist_entity")
	{
		return new ScientistEntity(getGameContext());
	}
	else if(type == "computer_entity")
	{
		return new ComputerEntity(getGameContext());
	}
	else if(type == "documents_entity")
	{
		return new DocumentsEntity(getGameContext());
	}
	else if(type == "door_entity")
	{
		return new DoorEntity(getGameContext());
	}
	else if(type == "coolant_entity")
	{
		return new CoolantEntity(getGameContext());
	}
	else if(type == "core_entity")
	{
		return new CoreEntity(getGameContext());
	}

	return NULL;
}

void GameState::drawTimer()
{
	float frameWidth = 32;
	float frameHeight = 64;
	float totalWidth = frameWidth * 12;
	float totalHeight = frameHeight;

	float curX = (getScreenWidth() - totalWidth) / 2;
	float curY = getScreenHeight() - totalHeight * 1.5f;
	bool blink = false;

	if(_isSaved)
	{
		long curTime = GameCore::getInstance().getCurrentFrameTime();
		if(curTime%1000<300)
		{
			blink = true;
		}
	}

	if(!blink)
	{
		lbRectF rect(0, 0, frameWidth, frameHeight);
		rect.move(curX, curY);
		_timerSprite->renderFrame(0, rect);//0H
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(0, rect);//0H
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(10, rect);//:
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(0, rect);//0M
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(0, rect);//0M
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(10, rect);//:
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame((_timeLeft % 100000) / 10000, rect);//10s
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame((_timeLeft % 10000) / 1000, rect);//1s
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(11, rect);//.
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame((_timeLeft % 1000) / 100, rect);//100ms
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame((_timeLeft % 100) / 10, rect);//10ms
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame((_timeLeft % 10), rect);//1ms
		rect.move(frameWidth, 0);
	}
	else
	{
		lbRectF rect(0, 0, frameWidth, frameHeight);
		rect.move(curX, curY);
		_timerSprite->renderFrame(12, rect);//.
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(12, rect);//.
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(12, rect);//.
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(12, rect);//.
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(12, rect);//.
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(12, rect);//.
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(12, rect);//.
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(12, rect);//.
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(12, rect);//.
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(12, rect);//.
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(12, rect);//.
		rect.move(frameWidth, 0);
		_timerSprite->renderFrame(12, rect);//.
		rect.move(frameWidth, 0);
	}
}
