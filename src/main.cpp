/*
 * main.cpp
 *
 *  Created on: 12-02-2011
 *      Author: Spliter
 */

#include <GLee.h>
#include <string>
#include <iostream>

#include <utils/debug/Logger.h>
#include "core/GameCore.h"
#include <math/lbMatrix4x4f.h>
#include <math/lbQuaternion.h>
#include <math/lbVec4f.h>
#include <SDL2/SDL.h>

#include "utils/resources/lbResourceManager.h"
#include <utils/graphics/lbTexture.h>

using namespace std;


int main(int argc,char** argv)
{
	//Note: for some stupid reason we have to put these here, otherwise it'll complain in linker
	lbMatrix4x4f mat;
	lbQuaternion quat;
	lbVec4f	vec4;


	Logger::logText("Logging successful");
	try
	{
		srand(time(NULL));
		GameCore &core = GameCore::getInstance();
		if(core.init(1000,600))
		{
			core.run();
			core.deinit();
		}
	}
	catch(...)
	{
		Logger::logText("Program crashed");
		Logger::flush();
	}
	return 0;
}

std::string retrieveDirectory(std::string path)
{
	std::string levelDir = "";
	size_t pos = path.rfind('/', path.length() - 1);
	if(pos != std::string::npos)
	{
		levelDir = path.substr(0, pos);
		if(!levelDir.empty() && levelDir.at(levelDir.length()-1)!='/')
		{
			levelDir+='/';
		}
	}
	else
	{
		size_t pos = path.rfind('\\', path.length() - 1);
		if(pos != std::string::npos)
		{
			levelDir = path.substr(0, pos);
			if(!levelDir.empty() && levelDir.at(levelDir.length()-1)!='\\')
			{
				levelDir+='\\';
			}
		}
	}
	return levelDir;
}

std::string getSpriteCommonDirectory()
{
	return "data/graphics/sprites/";
}

std::string getSpriteFileExtension()
{
	return ".txt";
}

std::string buildSpriteFilename(std::string spriteName)
{
	return getSpriteCommonDirectory()+spriteName+getSpriteFileExtension();
}

std::string getLevelCommonDirectory()
{
	return "data/levels/";
}

std::string getLevelFileExtension()
{
	return ".tmx";
}

std::string buildLevelFilename(std::string levelName)
{
	return getLevelCommonDirectory()+levelName+getLevelFileExtension();
}
