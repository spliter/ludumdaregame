/*
 * State.h
 *
 *  Created on: 12-09-2012
 *      Author: Spliter
 */

#pragma once
#include "../engine/input/lbInputEvents.h"
#include "StateManager.h"

class State: public lbInputListener
{
public:
	State();
	virtual ~State();

	virtual void onStart(){}//after the state has been created and right before it's run for the first time
	virtual void onResume(State* prevState){}//when resuming from another state (note: state is not deleted) or when starting for the first time with NULL as argument
	virtual void onFrameBegin(){}
	virtual void onFrameUpdate(){}
	virtual void onFrameRender(){}
	virtual void onFrameEnd(){}
	virtual void onPause(State* nextState){resetInput();}//when either going to another state or exiting closing state
	virtual void onEnd();//when closing this state, right before disconnecting, state will be deleted right after this function is called

	static void startState(State* state){getStateManager().startState(state);}
	void finish(){_isFinished = true;}

	virtual void onKeyboardEvent(lbKeyboardEvent *event){}
	virtual void onMouseMotionEvent(lbMouseMotionEvent* event){}
	virtual void onMouseButtonEvent(lbMouseButtonEvent* event){}
	virtual void onMouseWheelEvent(lbMouseWheelEvent* event){}


	virtual void resetInput(){}

	State* getChildState(){return _childState;}
	State* getParentState(){return _parentState;}

	bool isFinished(){return _isFinished;}
private:
	friend class StateManager;
	State* _parentState;
	State* _childState;
	bool _isFinished;
};
