/*
 * StateManager.cpp
 *
 *  Created on: 13-09-2012
 *      Author: Spliter
 */

#include "StateManager.h"
#include "State.h"
#include <iostream>
using namespace std;

StateManager& StateManager::getInstance()
{
	static StateManager mgr;
	return mgr;
}

StateManager::StateManager():
	_bottomState(0),
	_topState(0),
	_startedState(0)
{

}

StateManager::~StateManager()
{
}
void StateManager::update()
{
	cleanFinishedStates();
	if(_startedState)//we have a state to start
	{
		State* newState = _startedState;
		_startedState = NULL;
		if(!_bottomState)
		{
			_bottomState = newState;
			_topState = newState;
			newState->_parentState = NULL;
			newState->_childState = NULL;
		}
		else
		{
			newState->_parentState = _topState;
			_topState->_childState = newState;
			State* prevTopState = _topState;
			_topState = newState;
			newState->_childState=NULL;
			prevTopState->onPause(newState);
		}

		newState->onStart();
		newState->onResume(NULL);
	}

	if(_topState)
	{
		_topState->onFrameUpdate();
	}
}

void StateManager::startFrame()
{
	if(_topState)
	{
		_topState->onFrameBegin();
	}
}




void StateManager::renderFrame()
{
	if(_topState)
	{
		_topState->onFrameRender();
	}
}

void StateManager::endFrame()
{
	if(_topState)
	{
		_topState->onFrameEnd();
	}
}

void StateManager::onKeyboardEvent(lbKeyboardEvent *event)
{
	if(_topState)
	{
		_topState->onKeyboardEvent(event);
	}
}

void StateManager::onMouseMotionEvent(lbMouseMotionEvent *event)
{
	if(_topState)
	{
		_topState->onMouseMotionEvent(event);
	}
}

void StateManager::onMouseButtonEvent(lbMouseButtonEvent *event)
{
	if(_topState)
	{
		_topState->onMouseButtonEvent(event);
	}
}

void StateManager::onMouseWheelEvent(lbMouseWheelEvent *event)
{
	if(_topState)
	{
		_topState->onMouseWheelEvent(event);
	}
}

void StateManager::startState(State* state)
{
	_startedState = state;
}


void StateManager::cleanFinishedStates()
{
	State* state = _topState;
	while(state)
	{
		State* nextState = state->getParentState();
		if(state->isFinished())
		{
			if(state==_bottomState)
			{
				_bottomState = state->getChildState();
			}

			if(state==_topState)
			{
				_topState = state->getParentState();
			}
			bool hasChild = state->getChildState();
			bool hasParent = state->getParentState();

			if(hasParent)
			{
				if(!hasChild)
				{
					state->onPause(NULL);
					state->getParentState()->onResume(state);
				}
				state->getParentState()->_childState = state->getChildState();
			}
			else
			{
				if(hasChild)
				{
					state->getChildState()->_parentState = state->getParentState();
				}
				else
				{
					state->onPause(NULL);
				}
			}
			state->onEnd();
			delete state;
		}
		state=nextState;
	}
}
