/*
 * State.cpp
 *
 *  Created on: 12-09-2012
 *      Author: Spliter
 */

#include "State.h"
#include <iostream>

State::State():
	_parentState(NULL),
	_childState(NULL),
	_isFinished(false)
{

}

State::~State()
{
}

void State::onEnd()
{
}
