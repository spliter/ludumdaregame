/*
 * StateManager.h
 *
 *  Created on: 13-09-2012
 *      Author: Spliter
 */

#pragma once
#include <stack>
#include "../engine/input/lbInputEvents.h"

class State;
class StateManager: public lbInputListener
{
public:
	static StateManager& getInstance();
	virtual ~StateManager();


	void update();
	void startFrame();
	void renderFrame();
	void endFrame();

	void finish();

	void onKeyboardEvent(lbKeyboardEvent *event);
	void onMouseMotionEvent(lbMouseMotionEvent *event);
	void onMouseButtonEvent(lbMouseButtonEvent *event);
	void onMouseWheelEvent(lbMouseWheelEvent *event);

	void startState(State* state);//Starts the state and adds it as a child of existing state (if there is one)

	//NOTE: StateManager will not delete states, this is left to the states themselves
private:
	StateManager();
	void cleanFinishedStates();
	State* _bottomState;
	State* _topState;

	State* _startedState;
};

inline StateManager& getStateManager(){return StateManager::getInstance();}
