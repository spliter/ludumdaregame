/*
 * lbInputSystem.h
 *
 *  Created on: 05-09-2012
 *      Author: Spliter
 */

#pragma once
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keyboard.h>
#include <SDL2/SDL_mouse.h>
#include <list>
#include "../lbGameEvent.h"

enum lbMouseMode
{
	lbMouseModeRelative=1,
	lbMouseModeAbsolute=1
};

struct Mouse
{
	bool lb,mb,rb;
	int x,y;
};

class lbGameEvent;

class lbInputSystem: public lbGameEventListener
{
public:
	static lbInputSystem& getInstance();

	virtual ~lbInputSystem();
	virtual void setup();
	virtual void update(long curTime);
	virtual void clear();

	bool handleSDLEvent(SDL_Event& event);//returns true if this was an input event and was handled

	void handleSDLKeyboardEvent(SDL_KeyboardEvent& event);
	void handleSDLMouseMotionEvent(SDL_MouseMotionEvent& event);
	void handleSDLMouseButtonEvent(SDL_MouseButtonEvent& event);
	void handleSDLMouseWheelEvent(SDL_MouseWheelEvent& event);

	int getSDLKeyNumber();
	const uint8_t* getSDLKeyboardState();
	const uint8_t* getSDLKeyboardPrevState();
	Mouse getSDLMouseState();
	Mouse getSDLMousePrevState();
	Mouse getSDLMouseState(int mouseId);
	Mouse getSDLMousePrevState(int mouseId);

	bool keyCheck(uint32_t sdlKey);
	bool keyPressed(uint32_t sdlKey);
	bool keyReleased(uint32_t sdlKey);
	void warpMouse(int x,int y);//Caution: will provoke an event
	void setMouseMode(lbMouseMode mode);

private:
	lbInputSystem();
	Mouse mouse;
	Mouse prevmouse;
	const uint8_t* keyState;
	uint8_t* prevKeyState;
	int keynum;
};
