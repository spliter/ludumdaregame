/*
 * lbInputSystem.cpp
 *
 *  Created on: 05-09-2012
 *      Author: Spliter
 */

#include "lbInputSystem.h"
#include "lbInputEvents.h"
#include <iostream>
#include <SDL2/SDL_mouse.h>
#include <string.h>
#include "../../core/lbSDLAppDriver.h"
#include "../../states/StateManager.h"


using namespace std;

lbInputSystem& lbInputSystem::getInstance()
{
	static lbInputSystem instance;
	return instance;
}

lbInputSystem::lbInputSystem()
{
}

lbInputSystem::~lbInputSystem()
{
}

void lbInputSystem::setup()
{
	keyState = SDL_GetKeyboardState(&keynum);
	prevKeyState = new Uint8[keynum];
	memcpy(prevKeyState, keyState, sizeof(Uint8)*keynum);
	mouse.x = prevmouse.x = 0;
	mouse.y = prevmouse.y = 0;
	mouse.lb = prevmouse.lb = 0;
	mouse.rb = prevmouse.rb = 0;
	mouse.mb = prevmouse.mb = 0;
}

void lbInputSystem::update(long curTime)
{
	memcpy(prevKeyState, keyState, sizeof(Uint8)*keynum);
	SDL_PumpEvents();
	prevmouse.x = mouse.x;
	prevmouse.y = mouse.y;
	prevmouse.lb = mouse.lb;
	prevmouse.mb = mouse.mb;
	prevmouse.rb = mouse.rb;

	Uint8 mstate = SDL_GetMouseState(&mouse.x, &mouse.y);
	mouse.lb = mstate&SDL_BUTTON(1);
	mouse.mb = mstate&SDL_BUTTON(2);
	mouse.rb = mstate&SDL_BUTTON(3);
}

void lbInputSystem::clear()
{
	delete[] prevKeyState;
}

bool lbInputSystem::handleSDLEvent(SDL_Event& event)
{
	if(event.type==SDL_KEYUP||event.type==SDL_KEYDOWN)
	{
		handleSDLKeyboardEvent(event.key);
		return true;
	}
	else if(event.type==SDL_MOUSEMOTION)
	{
		handleSDLMouseMotionEvent(event.motion);
		return true;
	}
	else if(event.type==SDL_MOUSEBUTTONDOWN||event.type==SDL_MOUSEBUTTONUP)
	{
		handleSDLMouseButtonEvent(event.button);
		return true;
	}
	return false;
}

void lbInputSystem::handleSDLKeyboardEvent(SDL_KeyboardEvent& event)
{
	lbKeyboardEvent keyEvent;

	keyEvent.key = event.keysym.sym;
	keyEvent.keyMod = event.keysym.mod;
	keyEvent.type = event.state==SDL_PRESSED ? keyEvent.KeyPressed : keyEvent.KeyReleased;
	StateManager::getInstance().onKeyboardEvent(&keyEvent);
	fireEvent(&keyEvent);
}

void lbInputSystem::handleSDLMouseMotionEvent(SDL_MouseMotionEvent& event)
{
	lbMouseMotionEvent motionEvent;
	motionEvent.x = event.x;
	motionEvent.y = event.y;
	motionEvent.relX = event.xrel;
	motionEvent.relY = event.yrel;
	StateManager::getInstance().onMouseMotionEvent(&motionEvent);
	fireEvent(&motionEvent);

}

void lbInputSystem::handleSDLMouseButtonEvent(SDL_MouseButtonEvent& event)
{
	lbMouseButtonEvent buttonEvent;
	switch(event.button)
	{
	case SDL_BUTTON_LEFT:
		buttonEvent.button = buttonEvent.ButtonLeft;
		break;
	case SDL_BUTTON_MIDDLE:
		buttonEvent.button = buttonEvent.ButtonMiddle;
		break;
	case SDL_BUTTON_RIGHT:
		buttonEvent.button = buttonEvent.ButtonRight;
		break;
	case SDL_BUTTON_X1:
		buttonEvent.button = buttonEvent.ButtonExtra1;
		break;
	case SDL_BUTTON_X2:
		buttonEvent.button = buttonEvent.ButtonExtra2;
		break;
	}
	buttonEvent.type = event.state==SDL_PRESSED?buttonEvent.MousePressed:buttonEvent.MouseReleased;

	buttonEvent.x = event.x;
	buttonEvent.y = event.y;
	StateManager::getInstance().onMouseButtonEvent(&buttonEvent);
	fireEvent(&buttonEvent);
}

void lbInputSystem::handleSDLMouseWheelEvent(SDL_MouseWheelEvent& event)
{
	lbMouseWheelEvent buttonEvent;
	buttonEvent.x = event.x;
	buttonEvent.y = event.y;

	StateManager::getInstance().onMouseWheelEvent(&buttonEvent);
	fireEvent(&buttonEvent);
}

int lbInputSystem::getSDLKeyNumber()
{
	return keynum;
}

const uint8_t* lbInputSystem::getSDLKeyboardState()
{
	return keyState;
}

const uint8_t* lbInputSystem::getSDLKeyboardPrevState()
{
	return prevKeyState;
}

Mouse lbInputSystem::getSDLMouseState()
{
	return mouse;
}

Mouse lbInputSystem::getSDLMousePrevState()
{
	return prevmouse;
}

bool lbInputSystem::keyCheck(uint32_t key)
{
	if(key<keynum)
		return keyState[key];
	else
		return false;
}

bool lbInputSystem::keyPressed(uint32_t key)
{
	if(key>=keynum)
		return false;
	return keyState[key]&&!prevKeyState[key];
}

bool lbInputSystem::keyReleased(uint32_t key)
{
	if(key>=keynum)
		return false;
	return !keyState[key]&&prevKeyState[key];
}

void lbInputSystem::warpMouse(int x, int y)
{
	SDL_WarpMouseInWindow(lbSDLAppDriver::getWindow(),x,y);
	mouse.x = x;
	mouse.y = y;
}

void lbInputSystem::setMouseMode(lbMouseMode mode)
{
	SDL_SetRelativeMouseMode(mode==lbMouseModeRelative?SDL_TRUE:SDL_FALSE);
}
