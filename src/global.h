/*
 * Global.h
 *
 *  Created on: 20-02-2011
 *      Author: Spliter
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_
#include <string>
#include "core/lbSDLAppDriver.h"
inline int getScreenWidth(){return lbSDLAppDriver::getWindowWidth();}
inline int getScreenHeight(){return lbSDLAppDriver::getWindowHeight();}
inline unsigned long getElapsedMilliseconds(){return lbSDLAppDriver::getEllapsedMillis();}

std::string retrieveDirectory(std::string path);//note: quarantees that if directory is not empty it will end in / or \ depending on which one has been used before

std::string getSpriteCommonDirectory();
std::string getSpriteFileExtension();
std::string buildSpriteFilename(std::string spriteName);

std::string getLevelCommonDirectory();
std::string getLevelFileExtension();
std::string buildLevelFilename(std::string levelName);
#endif /* GLOBAL_H_ */
