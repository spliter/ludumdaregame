/*
 * lbSDLAppDriver.h
 *
 *  Created on: 11-09-2011
 *      Author: Spliter
 */

#pragma once

class SDL_Window;

class lbSDLAppDriver
{
public:
	static void clear();
	static bool setup(int width, int height, bool fullscreen);

	static void handleEvents();

	static void beginFrame();
	static void finalizeFrame();

	static SDL_Window* getWindow(){return _window;}
	static int getWindowWidth(){return _windowWidth;}
	static int getWindowHeight(){return _windowHeight;}

	static unsigned long getEllapsedMillis();

	static void wait(unsigned long millis);

private:
	lbSDLAppDriver(){}
	~lbSDLAppDriver(){}

	static bool _fullscreen;
	static int _windowWidth,_windowHeight;
	static int _bpp;
	static int _stencilBits;
	static int _depthBits;
	static int _multisampleLevels;

	static SDL_Window* _window;
	static void* _glContext;
};
