/*
 * DemoApp.cpp
 *
 *  Created on: 15-12-2011
 *      Author: Spliter
 */
#include <GLee.h>

#include "GameCore.h"

#include "utils/debug/Logger.h"
#include "lbSDLAppDriver.h"
#include "../game/MainMenuState.h"
#include "../engine/lbGameEvent.h"
#include "../utils/base.h"
#include "../utils/resources/lbDefaultResourceManagers.h"
#include <FTGL/ftgl.h>
#include <FTGL/FTGLBitmapFont.h>
#include <ft2build.h>
#include <IL/il.h>
#include <IL/ilut.h>
#include <iostream>
#include <string>
#include <vector>

using namespace std;
extern int screenWidth;
extern int screenHeight;
extern float timef;


GameCore& GameCore::getInstance()
{
	static GameCore instance;
	return instance;
}

GameCore::GameCore() :
	_prevFrameTime(0),
	_curFrameTime(0),
	_frameDif(0),
	_frameDifSeconds(0),
	_quit(false),
	_screenWidth(0),
	_screenHeight(0),
	_isFPSLimitEnabled(false),
	_targetFPS(120)
{
}

bool GameCore::init(int screenWidth, int screenHeight)
{
	LogState("initializing game");

	lbSDLAppDriver::setup(screenWidth, screenHeight, false);
	ilInit();
	lbDefaultResourceManagers::instance().setup();

	this->_screenWidth = screenWidth;
	this->_screenHeight = screenHeight;

	_curFrameTime = lbSDLAppDriver::getEllapsedMillis();
	_prevFrameTime = _curFrameTime;
	_frameDif = _curFrameTime - _prevFrameTime;
	_frameDifSeconds = (float) _frameDif / 1000.0f;
	lbInputSystem::getInstance().setup();
	getStateManager().startState(new MainMenuState(this));
	LogState("init finished");
	return true;
}

void GameCore::deinit()
{
	lbSDLAppDriver::clear();
	lbInputSystem::getInstance().clear();
	Logger::flush();
}

void GameCore::run()
{
	while(!_quit)
	{
		_prevFrameTime = _curFrameTime;
		_curFrameTime = lbSDLAppDriver::getEllapsedMillis();
		_frameDif = _curFrameTime - _prevFrameTime;
		_frameDifSeconds = (float) _frameDif / 1000.0f;

		if(_isFPSLimitEnabled)
		{
			long idealFrameLength = 1000 / _targetFPS;
			if(idealFrameLength > _frameDif)
			{
				lbSDLAppDriver::wait(idealFrameLength - _frameDif);
			}
		}

		lbSDLAppDriver::handleEvents();

		lbInputSystem::getInstance().update(_curFrameTime);
		getStateManager().update();

		lbSDLAppDriver::beginFrame();
		getStateManager().startFrame();

		//rendering code here
		getStateManager().renderFrame();

		getStateManager().endFrame();
		lbSDLAppDriver::finalizeFrame();
	}
}

void GameCore::enableFPSLimit(int targetFPS)
{
	_isFPSLimitEnabled = true;
	if(targetFPS>=1)
	{
		_targetFPS = targetFPS;
	}
	else
	{
		_targetFPS = 1;
	}
}

void GameCore::handleMouseMotionEvent(SDL_MouseMotionEvent& event)
{
	lbInputSystem::getInstance().handleSDLMouseMotionEvent(event);
}

void GameCore::handleMouseButtonEvent(SDL_MouseButtonEvent& event)
{
	lbInputSystem::getInstance().handleSDLMouseButtonEvent(event);
}

void GameCore::handleMouseWheelEvent(SDL_MouseWheelEvent& event)
{
	lbInputSystem::getInstance().handleSDLMouseWheelEvent(event);
}

void GameCore::handleKeyboardEvent(SDL_Event &event)
{
	lbInputSystem::getInstance().handleSDLKeyboardEvent(event.key);
}

