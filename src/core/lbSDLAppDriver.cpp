/*
 * lbSDLDisplaySystem.cpp
 *
 *  Created on: 11-09-2011
 *      Author: Spliter
 */
#include <GLee.h>
#include "lbSDLAppDriver.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <utils/debug/Logger.h>
#include <iostream>
#include "GameCore.h"
#include "../engine/input/lbInputSystem.h"


using namespace std;

bool lbSDLAppDriver::_fullscreen;
int lbSDLAppDriver::_windowWidth;
int lbSDLAppDriver::_windowHeight;
int lbSDLAppDriver::_bpp;
int lbSDLAppDriver::_stencilBits;
int lbSDLAppDriver::_depthBits;
int lbSDLAppDriver::_multisampleLevels;
SDL_Window* lbSDLAppDriver::_window;
void* lbSDLAppDriver::_glContext;

void lbSDLAppDriver::clear()
{
	SDL_GL_DeleteContext(_glContext);
	SDL_Quit();
}


bool lbSDLAppDriver::setup(int windowWidth, int windowHeight, bool fullscreen)
{
	_fullscreen = fullscreen;
	_windowWidth = windowWidth;
	_windowHeight = windowHeight;
	_bpp = 0;
	_stencilBits = 0;
	_depthBits = 0;
	_multisampleLevels = 0;

	_window = NULL;

	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS|SDL_INIT_TIMER))
	{
		Logger::getBuffer() << "SDL initialization failed:" << SDL_GetError() << endl;
		return false;
	}

	unsigned int videoFlags = 0;
	videoFlags|=SDL_WINDOW_OPENGL;
	if(fullscreen)
		videoFlags |= SDL_WINDOW_FULLSCREEN;
	//videoFlags|=SDL_NOFRAME;
	_window = SDL_CreateWindow("Out Of Time",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,_windowWidth,_windowHeight,videoFlags);
	if(!_window)
	{
		Logger::getBuffer() << "SDL Set video mode failed:" << SDL_GetError() << endl;
		return false;
	}

	_glContext = SDL_GL_CreateContext(_window);

	return true;
}

void lbSDLAppDriver::handleEvents()
{
	SDL_Event event;
	GameCore &game = GameCore::getInstance();
	while(SDL_PollEvent(&event))
	{
		if(event.type == SDL_KEYDOWN)
		{
			if(event.key.keysym.sym == SDLK_F5)
			{
				//make screenshot
			}
			game.handleKeyboardEvent(event);
		}
		else if(event.type == SDL_KEYUP)
		{
			game.handleKeyboardEvent(event);
		}
		else if(event.type == SDL_MOUSEBUTTONDOWN || event.type == SDL_MOUSEBUTTONUP)
		{
			game.handleMouseButtonEvent(event.button);
		}
		else if(event.type == SDL_MOUSEMOTION)
		{
			game.handleMouseMotionEvent(event.motion);
		}
		else if(event.type == SDL_MOUSEWHEEL)
		{
			game.handleMouseWheelEvent(event.wheel);
		}
		else if(event.type == SDL_QUIT)
		{
			cout<<"Quitting game...\n";
			cout.flush();
			game.quit();
		}
	}
}

void lbSDLAppDriver::beginFrame()
{

}

void lbSDLAppDriver::finalizeFrame()
{
	glFlush();
	glFinish();
	SDL_GL_SwapWindow(_window);
}

unsigned long lbSDLAppDriver::getEllapsedMillis()
{
	return SDL_GetTicks();
}

void lbSDLAppDriver::wait(unsigned long millis)
{
	SDL_Delay(millis);
}
