/*
 * DemoApp.h
 *
 *  Created on: 15-12-2011
 *      Author: Spliter
 */

#pragma once
#include <SDL2/SDL.h>
#include <math/lbMath.h>
#include "../engine/input/lbInputSystem.h"
class GameState;

class GameCore
{
public:
	static GameCore& getInstance();
	bool init(int screenWidth, int screenHeight);
	void deinit();
	void run();

	long getPreviousFrameTime(){return _prevFrameTime;}//in millis
	long getCurrentFrameTime(){return _curFrameTime;}//in millis
	long getFrameTimeDifference(){return _frameDif;}//in millis
	float getFrameTimeDifferencef(){return _frameDifSeconds;}//in millis

	int getScreenWidth(){return _screenWidth;}
	int getScreenHeight(){return _screenHeight;}

	void enableFPSLimit(int targetFPS);
	void disableFPSLimit(){_isFPSLimitEnabled = false;}

	void handleMouseMotionEvent(SDL_MouseMotionEvent& event);
	void handleMouseButtonEvent(SDL_MouseButtonEvent& event);
	void handleMouseWheelEvent(SDL_MouseWheelEvent& event);
	void handleKeyboardEvent(SDL_Event &event);

	void quit(){_quit=true;}
private:
	GameCore();
	GameCore(GameCore& core){};

	long _prevFrameTime;
	long _curFrameTime;
	long _frameDif;
	float _frameDifSeconds;

	bool _quit;

	int _screenWidth;
	int _screenHeight;

	bool _isFPSLimitEnabled;
	int _targetFPS;
};

inline GameCore& getGameCore(){return GameCore::getInstance();}
