/*
 * lbVec2f.h
 *
 *  Created on: 11-04-2011
 *      Author: Spliter
 */

#ifndef LBVEC2F_H_
#define LBVEC2F_H_
#include "lbBaseMath.h"
#include <ostream>

class lbVec2f
{
public:
	union
	{
		float me[2];
		struct
		{
			float x;
			float y;
		};
	};

	lbVec2f();
	lbVec2f(const lbVec2f& mom);
	lbVec2f(float _x,float _y);
	lbVec2f(float _x);

	lbVec2f& set(float _x,float _y);
	lbVec2f& set(const lbVec2f& b);
	lbVec2f& set(float _x);

	float length() const;
	float squareLength() const;
	float direction() const;
	float directionDeg() const;

	lbVec2f& normalize();
	lbVec2f normalized() const;

	lbVec2f& perpendicular();
	lbVec2f perpendiculared() const;

	lbVec2f& scale(float scale);
	lbVec2f scaled(float scale) const;

	lbVec2f& stretch(float newLength);
	lbVec2f stretched(float newLength) const;


	float dot(const lbVec2f& b) const;

	lbVec2f& rotate(float rad);
	lbVec2f rotated(float rad) const;

	lbVec2f& rotateDeg(float deg);
	lbVec2f rotatedDeg(float deg) const;

	lbVec2f& reflect(const lbVec2f& n);
	lbVec2f reflected(const lbVec2f& n) const;

	//piecewise operators:

	bool operator==(const lbVec2f& rh) const;
	bool operator!=(const lbVec2f& rh) const;

	lbVec2f& operator=(const lbVec2f& rh);
	lbVec2f& operator*=(const lbVec2f& rh);
	lbVec2f& operator/=(const lbVec2f& rh);
	lbVec2f& operator+=(const lbVec2f& rh);
	lbVec2f& operator-=(const lbVec2f& rh);

	lbVec2f operator*(const lbVec2f& rh) const;
	lbVec2f operator/(const lbVec2f& rh) const;
	lbVec2f operator+(const lbVec2f& rh) const;
	lbVec2f operator-(const lbVec2f& rh) const;

	lbVec2f operator-() const;

	//float operators:

	lbVec2f& operator=(float rh);
	lbVec2f& operator*=(float rh);
	lbVec2f& operator/=(float rh);

	lbVec2f operator*(float rh) const;
	lbVec2f operator/(float rh) const;

	float operator[](unsigned int index);

	friend std::ostream &operator<<(std::ostream &stream, lbVec2f &vec);
};

std::ostream &operator<<(std::ostream &stream, lbVec2f &vec);
lbVec2f operator*(const float& lh,const lbVec2f& rh);
lbVec2f operator/(const float& lh,const lbVec2f& rh);
inline float getAngle(const lbVec2f& from, const lbVec2f& to){return acosf(from.dot(to));}
#endif /* LBVEC2F_H_ */
