/*
 * lbMatrix3x3f.cpp
 *
 *  Created on: 15-04-2011
 *      Author: Spliter
 */

#include "lbMatrix3x3f.h"
#include "lbBaseMath.h"
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

lbMatrix3x3f::lbMatrix3x3f()
{
	me[0]=1.0;me[3]=0.0;me[6]=0.0;
	me[1]=0.0;me[4]=1.0;me[7]=0.0;
	me[2]=0.0;me[5]=0.0;me[8]=1.0;
}

lbMatrix3x3f::lbMatrix3x3f(const lbMatrix3x3f& mb)
{
	a=mb.a;d=mb.d;g=mb.g;
	b=mb.b;e=mb.e;h=mb.h;
	c=mb.c;f=mb.f;i=mb.i;
}

lbMatrix3x3f::lbMatrix3x3f(	float _a,float _b,float _c,
				float _d,float _e,float _f,
				float _g,float _h,float _i)
{
	a=_a;d=_d;g=_g;
	b=_b;e=_e;h=_h;
	c=_c;f=_f;i=_i;
}

void lbMatrix3x3f::set(float val)
{
	a=val;d=val;g=val;
	b=val;e=val;h=val;
	c=val;f=val;i=val;
}

void lbMatrix3x3f::set(	float _a,float _b,float _c,
			float _d,float _e,float _f,
			float _g,float _h,float _i)
{
	a=_a;d=_d;g=_g;
	b=_b;e=_e;h=_h;
	c=_c;f=_f;i=_i;
}

void lbMatrix3x3f::set(const lbMatrix3x3f& mb)
{
	a=mb.a;d=mb.d;g=mb.g;
	b=mb.b;e=mb.e;h=mb.h;
	c=mb.c;f=mb.f;i=mb.i;
}

lbMatrix3x3f& lbMatrix3x3f::makeIdentity()
{
	me[0]=1.0;me[3]=0.0;me[6]=0.0;
	me[1]=0.0;me[4]=1.0;me[7]=0.0;
	me[2]=0.0;me[5]=0.0;me[8]=1.0;
	return *this;
}

lbMatrix3x3f& lbMatrix3x3f::makeRotation(float rad)
{
	makeIdentity();
	float c=cos(rad);
	float s=sin(rad);
	me[0]=c;me[3]=-s;
	me[1]=s;me[4]=c;

//	cout<<setiosflags(ios::fixed)<<setprecision(3);
//	cout<<"make rotation\n"
//	<<a<<" "<<d<<" "<<g<<"\n"
//	<<b<<" "<<e<<" "<<h<<"\n"
//	<<c<<" "<<f<<" "<<i<<"\n"
//	<<endl;
	return *this;
}

lbMatrix3x3f& lbMatrix3x3f::makeRotationDeg(float deg)
{
	return makeRotation(deg*DEG_TO_RAD_CONST);
}

lbMatrix3x3f& lbMatrix3x3f::makeScale(float xscale,float yscale)
{
	makeIdentity();
	me[0]=xscale;
	me[4]=yscale;
	return *this;
}

lbMatrix3x3f& lbMatrix3x3f::makeTranslation(float xtranslate,float ytranslate)
{
	me[6]=xtranslate;
	me[7]=ytranslate;
//	cout<<setiosflags(ios::fixed)<<setprecision(3);
//	cout<<"make translation\n"
//	<<a<<" "<<d<<" "<<g<<"\n"
//	<<b<<" "<<e<<" "<<h<<"\n"
//	<<c<<" "<<f<<" "<<i<<"\n"
//	<<endl;

	return *this;
}

lbMatrix3x3f& lbMatrix3x3f::transpose()
{
	b+=d;d=b-d;b-=d;
	c+=g;g=c-g;c-=g;
	f+=h;h=f-h;f-=h;
	return *this;
}

lbMatrix3x3f lbMatrix3x3f::transposed() const
{
	return lbMatrix3x3f(a,d,g,b,e,h,c,f,i);
}

lbMatrix3x3f& lbMatrix3x3f::inverse()
{
	return *this;
}

lbMatrix3x3f lbMatrix3x3f::inversed() const
{
	return lbMatrix3x3f();
}

lbMatrix3x3f& lbMatrix3x3f::rotate(float rad)
{
	lbMatrix3x3f m;
	m.makeRotation(rad);
	return transform(m);
}

lbMatrix3x3f lbMatrix3x3f::rotated(float rad) const
{
	lbMatrix3x3f m;
	m.makeRotation(rad);
	return transformed(m);
}

lbMatrix3x3f& lbMatrix3x3f::rotateDeg(float deg)
{
	lbMatrix3x3f m;
	m.makeRotationDeg(deg);
	return transform(m);
}

lbMatrix3x3f lbMatrix3x3f::rotatedDeg(float deg) const
{
	lbMatrix3x3f m;
	m.makeRotationDeg(deg);
	return transformed(m);
}

lbMatrix3x3f& lbMatrix3x3f::scale(float scale)
{
	lbMatrix3x3f m;
	m.makeScale(scale,scale);
	return transform(m);
}

lbMatrix3x3f lbMatrix3x3f::scaled(float scale) const
{
	lbMatrix3x3f m;
	m.makeScale(scale,scale);
	return transformed(m);
}

lbMatrix3x3f& lbMatrix3x3f::scale(float xscale,float yscale)
{
	lbMatrix3x3f m;
	m.makeScale(xscale,yscale);
	return transform(m);
}

lbMatrix3x3f lbMatrix3x3f::scaled(float _xscale,float _yscale) const
{
	lbMatrix3x3f m;
	m.makeScale(_xscale,_yscale);
	return transformed(m);
}

lbMatrix3x3f& lbMatrix3x3f::translate(float x,float y)
{
	lbMatrix3x3f m;
	m.makeTranslation(x,y);
	return transform(m);
}

lbMatrix3x3f lbMatrix3x3f::translated(float x,float y) const
{
	lbMatrix3x3f m;
	m.makeTranslation(x,y);
	return transformed(m);
}

lbMatrix3x3f& lbMatrix3x3f::transform(const lbMatrix3x3f& mb)
{
	lbMatrix3x3f tmpm(	a*mb.a+d*mb.b+g*mb.c,
						b*mb.a+e*mb.b+h*mb.c,
						c*mb.a+f*mb.b+i*mb.c,

						a*mb.d+d*mb.e+g*mb.f,
						b*mb.d+e*mb.e+h*mb.f,
						c*mb.d+f*mb.e+i*mb.f,

						a*mb.g+d*mb.h+g*mb.i,
						b*mb.g+e*mb.h+h*mb.i,
						c*mb.g+f*mb.h+i*mb.i);
	set(tmpm);
	return *this;
}

lbMatrix3x3f lbMatrix3x3f::transformed(const lbMatrix3x3f& mb) const
{
	return lbMatrix3x3f(a*mb.a+d*mb.b+g*mb.c,
						b*mb.a+e*mb.b+h*mb.c,
						c*mb.a+f*mb.b+i*mb.c,

						a*mb.d+d*mb.e+g*mb.f,
						b*mb.d+e*mb.e+h*mb.f,
						c*mb.d+f*mb.e+i*mb.f,

						a*mb.g+d*mb.h+g*mb.i,
						b*mb.g+e*mb.h+h*mb.i,
						c*mb.g+f*mb.h+i*mb.i);
}

lbMatrix3x3f& lbMatrix3x3f::operator=(const lbMatrix3x3f& mb)
{
	a=mb.a;d=mb.d;g=mb.g;
	b=mb.b;e=mb.e;h=mb.h;
	c=mb.c;f=mb.f;i=mb.i;
	return *this;
}

lbMatrix3x3f& lbMatrix3x3f::operator*=(const lbMatrix3x3f& mb)
{
//	a*=mb.a;d*=mb.d;g*=mb.g;
//	b*=mb.b;e*=mb.e;h*=mb.h;
//	c*=mb.c;f*=mb.f;i*=mb.i;
//	return *this;

//	lbMatrix3x3f tmpm(	a*mb.a+d*mb.b+g*mb.c,a*mb.d+d*mb.e+g*mb.f,a*mb.g+d*mb.h+g*mb.i,
//						b*mb.a+e*mb.b+h*mb.c,b*mb.d+e*mb.e+h*mb.f,b*mb.g+e*mb.h+c*mb.i,
//						c*mb.a+f*mb.b+i*mb.c,c*mb.d+f*mb.e+i*mb.f,c*mb.g+f*mb.h+i*mb.i);
//
//	set(tmpm);
	return transform(mb);
}

lbMatrix3x3f& lbMatrix3x3f::operator/=(const lbMatrix3x3f& mb)
{
	a/=mb.a;d/=mb.d;g/=mb.g;
	b/=mb.b;e/=mb.e;h/=mb.h;
	c/=mb.c;f/=mb.f;i/=mb.i;
	return *this;
}

lbMatrix3x3f& lbMatrix3x3f::operator+=(const lbMatrix3x3f& mb)
{
	a+=mb.a;d+=mb.d;g+=mb.g;
	b+=mb.b;e+=mb.e;h+=mb.h;
	c+=mb.c;f+=mb.f;i+=mb.i;
	return *this;
}

lbMatrix3x3f& lbMatrix3x3f::operator-=(const lbMatrix3x3f& mb)
{
	a-=mb.a;d-=mb.d;g-=mb.g;
	b-=mb.b;e-=mb.e;h-=mb.h;
	c-=mb.c;f-=mb.f;i-=mb.i;
	return *this;
}

lbMatrix3x3f lbMatrix3x3f::operator*(const lbMatrix3x3f& mb) const
{
	//TODO: redo this with proper matrix multiplication
//	return lbMatrix3x3f(a*mb.a+d*mb.b+g*mb.c,a*mb.d+d*mb.e+g*mb.f,a*mb.g+d*mb.h+g*mb.i,
//						b*mb.a+e*mb.b+h*mb.c,b*mb.d+e*mb.e+h*mb.f,b*mb.g+e*mb.h+c*mb.i,
//						c*mb.a+f*mb.b+i*mb.c,c*mb.d+f*mb.e+i*mb.f,c*mb.g+f*mb.h+i*mb.i);

	return transformed(mb);
//	return lbMatrix3x3f(a*mb.a,b*mb.b,c*mb.c,
//						d*mb.d,e*mb.e,f*mb.f,
//						g*mb.g,h*mb.h,j*mb.j);

//	lbMatrix3x3f tmpm(a*mb.a+d*mb.b+g*mb.c,
//						b*mb.a+e*mb.b+h*mb.c,
//						c*mb.a+f*mb.b+i*mb.c,
//
//						a*mb.d+d*mb.e+g*mb.f,
//						b*mb.d+e*mb.e+h*mb.f,
//						c*mb.d+f*mb.e+i*mb.f,
//
//						a*mb.g+d*mb.h+g*mb.i,
//						b*mb.g+e*mb.h+c*mb.i,
//						c*mb.g+f*mb.h+i*mb.i);
//	set(tmpm);
}

lbMatrix3x3f lbMatrix3x3f::operator/(const lbMatrix3x3f& mb) const
{
	return lbMatrix3x3f(a/mb.a,b/mb.b,c/mb.c,
						d/mb.d,e/mb.e,f/mb.f,
						g/mb.g,h/mb.h,i/mb.i);
}

lbMatrix3x3f lbMatrix3x3f::operator+(const lbMatrix3x3f& mb) const
{
	return lbMatrix3x3f(a+mb.a,b+mb.b,c+mb.c,
						d+mb.d,e+mb.e,f+mb.f,
						g+mb.g,h+mb.h,i+mb.i);
}

lbMatrix3x3f lbMatrix3x3f::operator-(const lbMatrix3x3f& mb) const
{
	return lbMatrix3x3f(a-mb.a,b-mb.b,c-mb.c,
						d-mb.d,e-mb.e,f-mb.f,
						g-mb.g,h-mb.h,i-mb.i);
}

lbMatrix3x3f& lbMatrix3x3f::operator=(float val)
{
	set(val);
	return *this;
}

lbMatrix3x3f& lbMatrix3x3f::operator*=(float val)
{
	a*=val;d*=val;g*=val;
	b*=val;e*=val;h*=val;
	c*=val;f*=val;i*=val;
	return *this;
}

lbMatrix3x3f& lbMatrix3x3f::operator/=(float val)
{
	a/=val;d/=val;g/=val;
	b/=val;e/=val;h/=val;
	c/=val;f/=val;i/=val;
	return *this;
}

lbMatrix3x3f lbMatrix3x3f::operator*(float val) const
{
	return lbMatrix3x3f(a*val,b*val,c*val,
						d*val,e*val,f*val,
						g*val,h*val,i*val);
}

lbMatrix3x3f lbMatrix3x3f::operator/(float val) const
{
	return lbMatrix3x3f(a/val,b/val,c/val,
						d/val,e/val,f/val,
						g/val,h/val,i/val);
}


lbVec2f lbMatrix3x3f::operator*(const lbVec2f& v) const
{
	return lbVec2f(	v.x*a+v.y*d+1.0*g,
					v.x*b+v.y*e+1.0*h);
}

lbMatrix3x3f operator*(float val,const lbMatrix3x3f& m)
{
	return lbMatrix3x3f(m.a*val,m.b*val,m.c*val,
						m.d*val,m.e*val,m.f*val,
						m.g*val,m.h*val,m.i*val);
}
lbMatrix3x3f operator/(float f, const lbMatrix3x3f& m)
{
	return lbMatrix3x3f(f/m.a,f/m.b,f/m.c,
						f/m.d,f/m.e,f/m.f,
						f/m.g,f/m.h,f/m.i);
}
