/*
 * lbQuaternion.h
 *
 *  Created on: 17-05-2011
 *      Author: Spliter
 */

#ifndef LBQUATERNION_H_
#define LBQUATERNION_H_

#include <math.h>
#include "lbMath.h"
#include "lbVec3f.h"
#include "lbMatrix4x4f.h"

struct lbAxisAngle
{
	lbVec3f axis;
	float angle;//rad
};

class lbQuaternion
{
public:
	union
	{
		struct
		{
			float x,y,z,w;
		};
		float v[4];
	};

	lbQuaternion();
	lbQuaternion(const lbQuaternion& quat);
	lbQuaternion(float _x, float _y, float _z, float _w);
	lbQuaternion(float deg, lbVec3f axis);

	lbQuaternion& set(float _x, float _y, float _z, float _w);
	lbQuaternion& setRot(float pitch, float yaw, float roll);
	lbQuaternion& setRotDeg(float pitch, float yaw, float roll);
	lbQuaternion& setDeg(float pitch, float yaw, float roll);
	lbQuaternion& setRot(float rad, const lbVec3f& vec);
	lbQuaternion& setRot(float rad, float nx, float ny, float nz);
	lbQuaternion& setRotDeg(float deg, const lbVec3f& vec);
	lbQuaternion& setRotDeg(float deg, float nx, float ny, float nz);

	lbQuaternion& normalize(float precision=1e-7);
	lbQuaternion normalized(float precision=1e-7);

	lbQuaternion& makeIdentity();

	lbQuaternion& rotateByDeg(float deg, const lbVec3f& vec);
	lbQuaternion& rotateBy(float rad, const lbVec3f& vec);
	lbQuaternion& rotateBy(const lbQuaternion& quat);//same as operator*=
	lbQuaternion rotatedBy(const lbQuaternion& quat) const;//same as operator*

	lbQuaternion conjugate();

	bool operator==(const lbQuaternion& quat);
	bool operator!=(const lbQuaternion& quat);

	lbQuaternion& operator=(const lbQuaternion& quat);
	lbQuaternion& operator*=(const lbQuaternion& quat);
	lbQuaternion operator*(const lbQuaternion& quat) const;
	lbQuaternion& operator-=(const lbQuaternion& quat);
	lbQuaternion operator-() const;
	lbQuaternion operator-(const lbQuaternion& quat) const;
	lbQuaternion& operator+=(const lbQuaternion& quat);
	lbQuaternion operator+(const lbQuaternion& quat) const;

	lbQuaternion operator*(const float& f) const;
	lbQuaternion operator-(const float& f) const;
	lbQuaternion operator+(const float& f) const;
	lbQuaternion operator/(const float& f) const;

	lbVec3f operator*(const lbVec3f& v) const;

	lbMatrix4x4f getRotationMatrix();

	lbAxisAngle toAxisAngle();

	static lbQuaternion getRotation(lbVec3f vecFrom, lbVec3f vecTo);
	friend std::ostream &operator<<(std::ostream &stream, lbQuaternion &quat);

};

lbQuaternion slerp(const lbQuaternion& lh, const lbQuaternion& rh, float ratio);
lbQuaternion trislerp(const lbQuaternion& a, const lbQuaternion& b, const lbQuaternion& c, float u, float v);
lbQuaternion slerpZero(const lbQuaternion from, float ratio);
lbQuaternion nlerp(const lbQuaternion& lh, const lbQuaternion& rh, float ratio);
lbQuaternion optimalLerp(const lbQuaternion& lh, const lbQuaternion& rh, float ratio);//chooses between slerp or lerp

lbQuaternion operator*(const float& f,const lbQuaternion& quat);
lbQuaternion operator-(const float& f,const lbQuaternion& quat);
lbQuaternion operator+(const float& f,const lbQuaternion& quat);
lbQuaternion operator/(const float& f,const lbQuaternion& quat);
std::ostream &operator<<(std::ostream &stream, lbQuaternion &quat);
#endif /* LBQUATERNION_H_ */
