/*
 * lbVec4f.h
 *
 *  Created on: 12-04-2011
 *      Author: Spliter
 */

#pragma once
#include "lbBaseMath.h"
#include <ostream>

class lbVec4f
{
public:
	union
	{
		float me[4];
		struct
		{
			float x;
			float y;
			float z;
			float w;
		};
	};

	lbVec4f();
	lbVec4f(const lbVec4f& mom);
	lbVec4f(float _x,float _y,float _z, float _w);
	lbVec4f(float _x);

	lbVec4f& set(float _x,float _y,float _z, float _w);
	lbVec4f& set(const lbVec4f& b);
	lbVec4f& set(float _x);

	float length() const;
	float squareLength() const;

	lbVec4f& normalize();
	lbVec4f normalized() const;

	lbVec4f& scale(float scale);
	lbVec4f scaled(float scale) const;

	lbVec4f& stretch(float newLength);
	lbVec4f stretched(float newLength) const;

	float dot(const lbVec4f& b) const;


	// piecewise operators
	bool operator==(const lbVec4f& rh) const;
	bool operator!=(const lbVec4f& rh) const;

	lbVec4f& operator=(const lbVec4f& rh);
	lbVec4f& operator*=(const lbVec4f& rh);
	lbVec4f& operator/=(const lbVec4f& rh);
	lbVec4f& operator+=(const lbVec4f& rh);
	lbVec4f& operator-=(const lbVec4f& rh);

	lbVec4f operator*(const lbVec4f& rh) const;
	lbVec4f operator/(const lbVec4f& rh) const;
	lbVec4f operator+(const lbVec4f& rh) const;
	lbVec4f operator-(const lbVec4f& rh) const;

	lbVec4f operator-() const;
	lbVec4f operator+() const;

	//float operators:

	lbVec4f& operator=(const float& rh);
	lbVec4f& operator*=(const float& rh);
	lbVec4f& operator/=(const float& rh);

	lbVec4f operator*(const float& rh) const;
	lbVec4f operator/(const float& rh) const;

	float operator[](unsigned int index) const;

	friend std::ostream &operator<<(std::ostream &stream, lbVec4f &vec);
};

std::ostream &operator<<(std::ostream &stream, lbVec4f &vec);
lbVec4f operator*(const float& lh,const lbVec4f& rh);
lbVec4f operator/(const float& lh,const lbVec4f& rh);
float getAngle(const lbVec4f& from, const lbVec4f& to);
