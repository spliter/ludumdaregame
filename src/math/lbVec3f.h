/*
 * lbVec3f.h
 *
 *  Created on: 12-04-2011
 *      Author: Spliter
 */

#ifndef LBVEC3F_H_
#define LBVEC3F_H_
#include "lbBaseMath.h"
#include <ostream>

class lbVec3f
{
public:
	union
	{
		float me[3];
		struct
		{
			float x;
			float y;
			float z;
		};
	};

	lbVec3f();
	lbVec3f(const lbVec3f& mom);
	lbVec3f(float _x,float _y,float _z);
	lbVec3f(float _x);

	lbVec3f& set(float _x,float _y,float _z);
	lbVec3f& set(const lbVec3f& b);
	lbVec3f& set(float _x);

	float length() const;
	float squareLength() const;

	lbVec3f& normalize();
	lbVec3f normalized() const;

	lbVec3f& perpendicular(const lbVec3f& b);
	lbVec3f perpendiculared(const lbVec3f& b) const;

	lbVec3f& scale(float scale);
	lbVec3f scaled(float scale) const;

	lbVec3f& stretch(float newLength);
	lbVec3f stretched(float newLength) const;

	float dot(const lbVec3f& b) const;


	lbVec3f& cross(const lbVec3f& b);
	lbVec3f crossed(const lbVec3f& b) const;

	lbVec3f& rotate(float rad,float ax,float ay,float az);
	lbVec3f rotated(float rad,float ax,float ay,float az) const;
	lbVec3f& rotate(float rad,const lbVec3f& axis);
	lbVec3f rotated(float rad,const lbVec3f& axis) const;

	lbVec3f& rotateDeg(float deg,float ax,float ay,float az);
	lbVec3f rotatedDeg(float deg,float ax,float ay,float az) const;
	lbVec3f& rotateDeg(float deg,const lbVec3f& axis);
	lbVec3f rotatedDeg(float deg,const lbVec3f& axis) const;

	lbVec3f& reflect(const lbVec3f& n);
	lbVec3f reflected(const lbVec3f& n) const;

	// piecewise operators
	bool operator==(const lbVec3f& rh) const;
	bool operator!=(const lbVec3f& rh) const;

	lbVec3f& operator=(const lbVec3f& rh);
	lbVec3f& operator*=(const lbVec3f& rh);
	lbVec3f& operator/=(const lbVec3f& rh);
	lbVec3f& operator+=(const lbVec3f& rh);
	lbVec3f& operator-=(const lbVec3f& rh);

	lbVec3f operator*(const lbVec3f& rh) const;
	lbVec3f operator/(const lbVec3f& rh) const;
	lbVec3f operator+(const lbVec3f& rh) const;
	lbVec3f operator-(const lbVec3f& rh) const;

	lbVec3f operator-() const;
	lbVec3f operator+() const;

	//float operators:

	lbVec3f& operator=(const float& rh);
	lbVec3f& operator*=(const float& rh);
	lbVec3f& operator/=(const float& rh);

	lbVec3f operator*(const float& rh) const;
	lbVec3f operator/(const float& rh) const;

	float operator[](unsigned int index) const;

	friend std::ostream &operator<<(std::ostream &stream, lbVec3f &vec);
};

std::ostream &operator<<(std::ostream &stream, lbVec3f &vec);
lbVec3f operator*(const float& lh,const lbVec3f& rh);
lbVec3f operator/(const float& lh,const lbVec3f& rh);
float getAngle(const lbVec3f& from, const lbVec3f& to);
#endif /* LBVEC3F_H_ */
