/*
 * lbVec2f.cpp
 *
 *  Created on: 12-04-2011
 *      Author: Spliter
 */
#include "lbVec2f.h"

lbVec2f::lbVec2f():
	x(0),
	y(0)
{
}
lbVec2f::lbVec2f(const lbVec2f& mom)
{
	x=mom.x;
	y=mom.y;
}

lbVec2f::lbVec2f(float _x,float _y)
{
	x=_x;
	y=_y;
}

lbVec2f::lbVec2f(float _x)
{
	x=_x;
	y=_x;
}

lbVec2f& lbVec2f::set(float _x,float _y)
{
	x=_x;
	y=_y;
	return *this;
}
lbVec2f& lbVec2f::set(const lbVec2f& b)
{
	x=b.x;
	y=b.y;
	return *this;
}


lbVec2f& lbVec2f::set(float _x)
{
	x=_x;
	y=_x;

	return *this;
}

float lbVec2f::length() const
{
	return sqrt(sqr(x)+sqr(y));
}

float lbVec2f::squareLength() const
{
	return sqr(x)+sqr(y);
}

float lbVec2f::direction() const
{
	return atan2(x,y);
}
float lbVec2f::directionDeg() const
{
	return atan2(x,y)*RAD_TO_DEG_CONST;
}

lbVec2f& lbVec2f::normalize()
{
	float d=length();
	if(d<ERROR_MARGIN)
	{
		x=1.0;
		y=0.0;
	}
	else
	{
		x=x/d;
		y=y/d;
	}
	return *this;
}
lbVec2f lbVec2f::normalized() const
{
	float d=length();
	if(d<ERROR_MARGIN)
	{
		return lbVec2f(1.0,0.0);
	}
	else
	{
		return lbVec2f(x/d,y/d);
	}
}

lbVec2f& lbVec2f::perpendicular()
{
	float aux=x;
	x=y;
	y=-aux;
	return *this;
}
lbVec2f lbVec2f::perpendiculared() const
{
	return lbVec2f(y,-x);
}

lbVec2f& lbVec2f::scale(float scale)
{
	x*=scale;
	y*=scale;
	return *this;
}
lbVec2f lbVec2f::scaled(float scale) const
{
	return lbVec2f(x*scale,y*scale);
}

lbVec2f& lbVec2f::stretch(float newLength)
{
	normalize();
	x*=newLength;
	y*=newLength;
	return *this;
}
lbVec2f lbVec2f::stretched(float newLength) const
{
	float d=length();
	if(d<ERROR_MARGIN)
	{
		return lbVec2f(newLength,0.0);
	}
	else
	{
		return lbVec2f(x/d*newLength,y/d*newLength);
	}
}


float lbVec2f::dot(const lbVec2f& b) const
{
	return x*b.x+y*b.y;
}

lbVec2f& lbVec2f::rotate(float rad)
{
	float c=cos(rad);
	float s=sin(rad);
	float ax=x;
	x=c*x-s*y;
	y=c*y+s*ax;
	return *this;
}
lbVec2f lbVec2f::rotated(float rad) const
{
	float c=cos(rad);
	float s=sin(rad);
	return lbVec2f(c*x-s*y,c*y+s*x);
}

lbVec2f& lbVec2f::rotateDeg(float deg)
{
	float r=deg*DEG_TO_RAD_CONST;
	return rotate(r);
}
lbVec2f lbVec2f::rotatedDeg(float deg) const
{
	float r=deg*DEG_TO_RAD_CONST;
	return rotated(r);
}
lbVec2f& lbVec2f::reflect(const lbVec2f& n)
{
	float d=2*dot(n);
	(*this)-=d*n;
	return *this;
}
lbVec2f lbVec2f::reflected(const lbVec2f& n) const
{
	float d=2*dot(n);
	return lbVec2f(x-d*n.x,y-d*n.y);
}
bool lbVec2f::operator==(const lbVec2f& rh) const
{
	return fabs(rh.x-x)<ERROR_MARGIN && fabs(rh.y-y)<ERROR_MARGIN;
}
bool lbVec2f::operator!=(const lbVec2f& rh) const
{
	return !(x==rh.y && y==rh.y);
}
//piecewise operators:
lbVec2f& lbVec2f::operator=(const lbVec2f& rh)
{
	x=rh.x;
	y=rh.y;
	return *this;
}
lbVec2f& lbVec2f::operator*=(const lbVec2f& rh)
{
	x*=rh.x;
	y*=rh.y;
	return *this;
}
lbVec2f& lbVec2f::operator/=(const lbVec2f& rh)
{
	x/=rh.x;
	y/=rh.y;
	return *this;
}
lbVec2f& lbVec2f::operator+=(const lbVec2f& rh)
{
	x+=rh.x;
	y+=rh.y;
	return *this;
}
lbVec2f& lbVec2f::operator-=(const lbVec2f& rh)
{
	x-=rh.x;
	y-=rh.y;
	return *this;
}

lbVec2f lbVec2f::operator*(const lbVec2f& rh) const
{
	return lbVec2f(x*rh.x,y*rh.y);
}

lbVec2f lbVec2f::operator/(const lbVec2f& rh) const
{
	return lbVec2f(x/rh.x,y/rh.y);
}
lbVec2f lbVec2f::operator+(const lbVec2f& rh) const
{
	return lbVec2f(x+rh.x,y+rh.y);
}
lbVec2f lbVec2f::operator-(const lbVec2f& rh) const
{
	return lbVec2f(x-rh.x,y-rh.y);
}

lbVec2f lbVec2f::operator-() const
{
	return lbVec2f(-x,-y);
}

lbVec2f& lbVec2f::operator=(float rh)
{
	x=rh;
	y=rh;
	return *this;
}

lbVec2f& lbVec2f::operator*=(float rh)
{
	x*=rh;
	y*=rh;
	return *this;
}

lbVec2f& lbVec2f::operator/=(float rh)
{
	x/=rh;
	y/=rh;
	return *this;
}

lbVec2f lbVec2f::operator*(float rh) const
{
	return lbVec2f(x*rh,y*rh);
}
lbVec2f lbVec2f::operator/(float rh) const
{
	return lbVec2f(x/rh,y/rh);
}


float lbVec2f::operator[](unsigned int index)
{
	return (index<2?me[index]:0);
}

std::ostream &operator<<(std::ostream &stream, lbVec2f &vec)
{
	stream<<"vec("<<vec.x<<" , "<<vec.y<<")";
	return stream;
}

lbVec2f operator*(const float& lh,const lbVec2f& rh)
{
	return lbVec2f(lh*rh.x,lh*rh.y);
}
lbVec2f operator/(const float& lh,const lbVec2f& rh)
{
	return lbVec2f(lh/rh.x,lh/rh.y);
}
