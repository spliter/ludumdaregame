/*
 * lbVec4f.cpp
 *
 *  Created on: 12-04-2011
 *      Author: Spliter
 */

#include "lbVec4f.h"

lbVec4f::lbVec4f()
{
	x=0.0;
	y=0.0;
	z=0.0;
	w=0.0;
}
lbVec4f::lbVec4f(const lbVec4f& mom)
{
	x=mom.x;
	y=mom.y;
	z=mom.z;
	w=mom.w;
}
lbVec4f::lbVec4f(float _x,float _y,float _z,float _w)
{
	x=_x;
	y=_y;
	z=_z;
	w=_w;
}
lbVec4f::lbVec4f(float _x)
{
	x=_x;
	y=_x;
	z=_x;
	w=_x;
}

lbVec4f& lbVec4f::set(float _x,float _y,float _z, float _w)
{
	x=_x;y=_y;z=_z;w=_w;
	return *this;
}

lbVec4f& lbVec4f::set(const lbVec4f& b)
{
	x=b.x;y=b.y;z=b.z;w=b.w;
	return *this;
}

lbVec4f& lbVec4f::set(float _x)
{
	x=_x;y=_x;z=_x;w=_x;
	return *this;
}

float lbVec4f::length() const
{
	return sqrt(sqr(x)+sqr(y)+sqr(z)+sqr(w));
}
float lbVec4f::squareLength() const
{
	return sqr(x)+sqr(y)+sqr(z)+sqr(w);
}

lbVec4f& lbVec4f::normalize()
{
	float len=squareLength();
	if(len>ERROR_MARGIN)
	{
		len=1.0/sqrt(len);
		x*=len;
		y*=len;
		z*=len;
		w*=len;
	}
	else
	{
		x=1.0;
		y=0.0;
		z=0.0;
		w=0.0;
	}
	return *this;
}
lbVec4f lbVec4f::normalized() const
{
	float len=squareLength();
	if(len>ERROR_MARGIN)
	{
		len=1.0/sqrt(len);
		return lbVec4f(x*len,y*len,z*len,w*len);
	}
	else
	{
		return lbVec4f(1.0,0.0,0.0,0.0);
	}
}

lbVec4f& lbVec4f::scale(float scale)
{
	x*=scale;
	y*=scale;
	z*=scale;
	w*=scale;
	return *this;
}
lbVec4f lbVec4f::scaled(float scale) const
{
	return lbVec4f(x*scale,y*scale,z*scale,w*scale);
}

lbVec4f& lbVec4f::stretch(float newLength)
{
	normalize();
	return scale(newLength);
}
lbVec4f lbVec4f::stretched(float newLength) const
{
	lbVec4f tmp=normalized();
	return tmp.scaled(newLength);
}

float lbVec4f::dot(const lbVec4f& b) const
{
	return x*b.x+y*b.y+z*b.z+w*b.w;
}


// piecewise operators
bool lbVec4f::operator==(const lbVec4f& rh) const
{
	return fabs(rh.x-x)<ERROR_MARGIN && fabs(rh.y-y)<ERROR_MARGIN && fabs(rh.z-z)<ERROR_MARGIN && fabs(rh.w-w)<ERROR_MARGIN;
}
bool lbVec4f::operator!=(const lbVec4f& rh) const
{
	return fabs(rh.x-x)>=ERROR_MARGIN || fabs(rh.y-y)>=ERROR_MARGIN || fabs(rh.z-z)>=ERROR_MARGIN || fabs(rh.w-w)>=ERROR_MARGIN;
}

lbVec4f& lbVec4f::operator=(const lbVec4f& rh)
{
	x=rh.x;
	y=rh.y;
	z=rh.z;
	w=rh.w;
	return *this;
}
lbVec4f& lbVec4f::operator*=(const lbVec4f& rh)
{
	x*=rh.x;
	y*=rh.y;
	z*=rh.z;
	w*=rh.w;
	return *this;
}
lbVec4f& lbVec4f::operator/=(const lbVec4f& rh)
{
	x/=rh.x;
	y/=rh.y;
	z/=rh.z;
	w/=rh.w;
	return *this;
}
lbVec4f& lbVec4f::operator+=(const lbVec4f& rh)
{
	x+=rh.x;
	y+=rh.y;
	z+=rh.z;
	w+=rh.w;
	return *this;
}
lbVec4f& lbVec4f::operator-=(const lbVec4f& rh)
{
	x-=rh.x;
	y-=rh.y;
	z-=rh.z;
	w-=rh.w;
	return *this;
}

lbVec4f lbVec4f::operator*(const lbVec4f& rh) const
{
	return lbVec4f(	x*rh.x,
					y*rh.y,
					z*rh.z,
					w*rh.w);
}
lbVec4f lbVec4f::operator/(const lbVec4f& rh) const
{
	return lbVec4f(	x/rh.x,
					y/rh.y,
					z/rh.z,
					w/rh.w);
}
lbVec4f lbVec4f::operator+(const lbVec4f& rh) const
{
	return lbVec4f(	x+rh.x,
					y+rh.y,
					z+rh.z,
					w+rh.w);
}
lbVec4f lbVec4f::operator-(const lbVec4f& rh) const
{
	return lbVec4f(	x-rh.x,
					y-rh.y,
					z-rh.z,
					w-rh.w);
}

lbVec4f lbVec4f::operator-() const
{
	return lbVec4f(	-x,
					-y,
					-z,
					-w);
}

lbVec4f lbVec4f::operator+() const
{
	return lbVec4f(*this);
}

//float operators:

lbVec4f& lbVec4f::operator=(const float& rh)
{
	x=rh;
	y=rh;
	z=rh;
	w=rh;
	return *this;
}
lbVec4f& lbVec4f::operator*=(const float& rh)
{
	x*=rh;
	y*=rh;
	z*=rh;
	w*=rh;
	return *this;
}
lbVec4f& lbVec4f::operator/=(const float& rh)
{
	x/=rh;
	y/=rh;
	z/=rh;
	w/=rh;
	return *this;
}

lbVec4f lbVec4f::operator*(const float& rh) const
{
	return lbVec4f(x*rh,y*rh,z*rh,w*rh);
}
lbVec4f lbVec4f::operator/(const float& rh) const
{
	return lbVec4f(x/rh,y/rh,z/rh,w/rh);
}

float lbVec4f::operator[](unsigned int index) const
{
	return me[index];
}

std::ostream &operator<<(std::ostream &stream, lbVec4f &vec)
{
	stream<<"vec("<<vec.x<<" , "<<vec.y<<" , "<<vec.z<<" , "<<vec.w<<")";
	return stream;
}

lbVec4f operator*(const float& lh,const lbVec4f& rh)
{
	return lbVec4f(rh.x*lh,rh.y*lh,rh.z*lh,rh.w*lh);
}
lbVec4f operator/(const float& lh,const lbVec4f& rh)
{
	return lbVec4f(lh/rh.x,lh/rh.y,lh/rh.z,lh/rh.w);
}
