/*
 * lbVec3f.cpp
 *
 *  Created on: 12-04-2011
 *      Author: Spliter
 */

#include "lbVec3f.h"

lbVec3f::lbVec3f()
{
	x=0.0;
	y=0.0;
	z=0.0;
}
lbVec3f::lbVec3f(const lbVec3f& mom)
{
	x=mom.x;
	y=mom.y;
	z=mom.z;
}
lbVec3f::lbVec3f(float _x,float _y,float _z)
{
	x=_x;
	y=_y;
	z=_z;
}
lbVec3f::lbVec3f(float _x)
{
	x=_x;
	y=_x;
	z=_x;
}

lbVec3f& lbVec3f::set(float _x,float _y,float _z)
{
	x=_x;y=_y;z=_z;
	return *this;
}

lbVec3f& lbVec3f::set(const lbVec3f& b)
{
	x=b.x;y=b.y;z=b.z;
	return *this;
}

lbVec3f& lbVec3f::set(float _x)
{
	x=_x;y=_x;z=_x;
	return *this;
}

float lbVec3f::length() const
{
	return sqrt(sqr(x)+sqr(y)+sqr(z));
}
float lbVec3f::squareLength() const
{
	return sqr(x)+sqr(y)+sqr(z);
}

lbVec3f& lbVec3f::normalize()
{
	float len=squareLength();
	if(len>ERROR_MARGIN)
	{
		len=1.0/sqrt(len);
		x*=len;
		y*=len;
		z*=len;
	}
	else
	{
		x=1.0;
		y=0.0;
		z=0.0;
	}
	return *this;
}
lbVec3f lbVec3f::normalized() const
{
	float len=squareLength();
	if(len>ERROR_MARGIN)
	{
		len=1.0/sqrt(len);
		return lbVec3f(x*len,y*len,z*len);
	}
	else
	{
		return lbVec3f(1.0,0.0,0.0);
	}
}

lbVec3f& lbVec3f::perpendicular(const lbVec3f& b)
{
	return cross(b);
}

lbVec3f lbVec3f::perpendiculared(const lbVec3f& b) const
{
	return crossed(b);
}

lbVec3f& lbVec3f::scale(float scale)
{
	x*=scale;
	y*=scale;
	z*=scale;
	return *this;
}
lbVec3f lbVec3f::scaled(float scale) const
{
	return lbVec3f(x*scale,y*scale,z*scale);
}

lbVec3f& lbVec3f::stretch(float newLength)
{
	normalize();
	return scale(newLength);
}
lbVec3f lbVec3f::stretched(float newLength) const
{
	lbVec3f tmp=normalized();
	return tmp.scaled(newLength);
}

float lbVec3f::dot(const lbVec3f& b) const
{
	return x*b.x+y*b.y+z*b.z;
}


lbVec3f& lbVec3f::cross(const lbVec3f& b)
{
	float cx=y*b.z-z*b.y;
	float cy=z*b.x-x*b.z;
	float cz=x*b.y-y*b.x;
	x=cx;
	y=cy;
	z=cz;
	return *this;
}
lbVec3f lbVec3f::crossed(const lbVec3f& b) const
{
	return lbVec3f(	y*b.z-z*b.y,
					z*b.x-x*b.z,
					x*b.y-y*b.x);
}

lbVec3f& lbVec3f::rotate(float rad,float ax,float ay,float az)
{
	float sina=sin(rad);
	float cosa=cos(rad);
	float cosb=1.0-cosa;

	float nx=	x*(ax*ax*cosb+cosa)+
				y*(ax*ay*cosb-az*sina)+
				z*(ax*az*cosb+ay*sina);

	float ny=	x*(ay*ax*cosb+az*sina)+
				y*(ay*ay*cosb+cosa)+
				z*(ay*az*cosb-ax*sina);

	float nz=	x*(az*ax*cosb-ay*sina)+
				y*(az*ay*cosb+ax*sina)+
				z*(az*az*cosb+cosa);
	x=nx;
	y=ny;
	z=nz;
	return *this;
}

lbVec3f lbVec3f::rotated(float rad,float ax,float ay,float az) const
{
	float sina=sin(rad);
	float cosa=cos(rad);
	float cosb=1.0-cosa;

	float nx=	x*(ax*ax*cosb+cosa)+
				y*(ax*ay*cosb-az*sina)+
				z*(ax*az*cosb+ay*sina);

	float ny=	x*(ay*ax*cosb+az*sina)+
				y*(ay*ay*cosb+cosa)+
				z*(ay*az*cosb-ax*sina);

	float nz=	x*(az*ax*cosb-ay*sina)+
				y*(az*ay*cosb+ax*sina)+
				z*(az*az*cosb+cosa);
	return lbVec3f(nx,ny,nz);
}

lbVec3f& lbVec3f::rotate(float rad,const lbVec3f& axis)
{
	return rotate(rad,axis.x,axis.y,axis.z);
}

lbVec3f lbVec3f::rotated(float rad,const lbVec3f& axis) const
{
	return rotated(rad,axis.x,axis.y,axis.z);
}

lbVec3f& lbVec3f::rotateDeg(float deg,float ax,float ay,float az)
{
	return rotate(deg*DEG_TO_RAD_CONST,ax,ay,az);
}
lbVec3f lbVec3f::rotatedDeg(float deg,float ax,float ay,float az) const
{
	return rotated(deg*DEG_TO_RAD_CONST,ax,ay,az);
}
lbVec3f& lbVec3f::rotateDeg(float deg,const lbVec3f& axis)
{
	return rotate(deg*DEG_TO_RAD_CONST,axis.x,axis.y,axis.z);
}
lbVec3f lbVec3f::rotatedDeg(float deg,const lbVec3f& axis) const
{
	return rotated(deg*DEG_TO_RAD_CONST,axis.x,axis.y,axis.z);
}

lbVec3f& lbVec3f::reflect(const lbVec3f& n)
{
	*this=(*this-(n*2.0*(n.dot(*this))));
	return *this;
}
lbVec3f lbVec3f::reflected(const lbVec3f& n) const
{
	lbVec3f aux;
	aux=*this;

	return (*this-n*2.0*(n.dot(*this)));
}

// piecewise operators
bool lbVec3f::operator==(const lbVec3f& rh) const
{
	return fabs(rh.x-x)<ERROR_MARGIN && fabs(rh.y-y)<ERROR_MARGIN && fabs(rh.z-z)<ERROR_MARGIN;
}
bool lbVec3f::operator!=(const lbVec3f& rh) const
{
	return fabs(rh.x-x)>=ERROR_MARGIN || fabs(rh.y-y)>=ERROR_MARGIN || fabs(rh.z-z)>=ERROR_MARGIN;
}

lbVec3f& lbVec3f::operator=(const lbVec3f& rh)
{
	x=rh.x;
	y=rh.y;
	z=rh.z;
	return *this;
}
lbVec3f& lbVec3f::operator*=(const lbVec3f& rh)
{
	x*=rh.x;
	y*=rh.y;
	z*=rh.z;
	return *this;
}
lbVec3f& lbVec3f::operator/=(const lbVec3f& rh)
{
	x/=rh.x;
	y/=rh.y;
	z/=rh.z;
	return *this;
}
lbVec3f& lbVec3f::operator+=(const lbVec3f& rh)
{
	x+=rh.x;
	y+=rh.y;
	z+=rh.z;
	return *this;
}
lbVec3f& lbVec3f::operator-=(const lbVec3f& rh)
{
	x-=rh.x;
	y-=rh.y;
	z-=rh.z;
	return *this;
}

lbVec3f lbVec3f::operator*(const lbVec3f& rh) const
{
	return lbVec3f(	x*rh.x,
					y*rh.y,
					z*rh.z);
}
lbVec3f lbVec3f::operator/(const lbVec3f& rh) const
{
	return lbVec3f(	x/rh.x,
					y/rh.y,
					z/rh.z);
}
lbVec3f lbVec3f::operator+(const lbVec3f& rh) const
{
	return lbVec3f(	x+rh.x,
					y+rh.y,
					z+rh.z);
}
lbVec3f lbVec3f::operator-(const lbVec3f& rh) const
{
	return lbVec3f(	x-rh.x,
					y-rh.y,
					z-rh.z);
}

lbVec3f lbVec3f::operator-() const
{
	return lbVec3f(	-x,
					-y,
					-z);
}

lbVec3f lbVec3f::operator+() const
{
	return lbVec3f(*this);
}

//float operators:

lbVec3f& lbVec3f::operator=(const float& rh)
{
	x=rh;
	y=rh;
	z=rh;
	return *this;
}
lbVec3f& lbVec3f::operator*=(const float& rh)
{
	x*=rh;
	y*=rh;
	z*=rh;
	return *this;
}
lbVec3f& lbVec3f::operator/=(const float& rh)
{
	x/=rh;
	y/=rh;
	z/=rh;
	return *this;
}

lbVec3f lbVec3f::operator*(const float& rh) const
{
	return lbVec3f(x*rh,y*rh,z*rh);
}
lbVec3f lbVec3f::operator/(const float& rh) const
{
	return lbVec3f(x/rh,y/rh,z/rh);
}

float lbVec3f::operator[](unsigned int index) const
{
	return me[index];
}

std::ostream &operator<<(std::ostream &stream, lbVec3f &vec)
{
	stream<<"vec("<<vec.x<<" , "<<vec.y<<" , "<<vec.z<<")";
	return stream;
}

lbVec3f operator*(const float& lh,const lbVec3f& rh)
{
	return lbVec3f(rh.x*lh,rh.y*lh,rh.z*lh);
}
lbVec3f operator/(const float& lh,const lbVec3f& rh)
{
	return lbVec3f(lh/rh.x,lh/rh.y,lh/rh.z);
}

float getAngle(const lbVec3f& from, const lbVec3f& to)
{
	return acosf(from.dot(to));
}
