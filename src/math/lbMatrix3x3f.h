/*
 * lbMatrix3x3f.h
 *
 *  Created on: 13-04-2011
 *      Author: Spliter
 */

#ifndef LBMATRIX3X3F_H_
#define LBMATRIX3X3F_H_
#include "lbBaseMath.h"
#include "lbVec2f.h"
/* Note: Column major matrices
 * pre-multiply for the desired result
 * ie: to rotate a translated matrix, you'd have to do:
 * Result=rotation*translation;
 */
class lbMatrix3x3f
{
public:
	union
	{
		float me[9];
		struct
		{
			float a;
			float b;
			float c;

			float d;
			float e;
			float f;

			float g;
			float h;
			float i;
		};
	};

	lbMatrix3x3f();
	lbMatrix3x3f(const lbMatrix3x3f& mb);
	lbMatrix3x3f(	float _a,float _b,float _c,
					float _d,float _e,float _f,
					float _g,float _h,float _i);

	void set(float val);
	void set(	float _a,float _b,float _c,
				float _d,float _e,float _f,
				float _g,float _h,float _i);
	void set(const lbMatrix3x3f& mb);

	lbMatrix3x3f& makeIdentity();
	lbMatrix3x3f& makeRotation(float rad);
	lbMatrix3x3f& makeRotationDeg(float deg);
	lbMatrix3x3f& makeScale(float xscale,float yscale);
	lbMatrix3x3f& makeTranslation(float xtranslate,float ytranslate);

	lbMatrix3x3f& transpose();
	lbMatrix3x3f transposed() const;

	lbMatrix3x3f& inverse();
	lbMatrix3x3f inversed() const;

	lbMatrix3x3f& rotate(float rad);
	lbMatrix3x3f rotated(float rad) const;
	lbMatrix3x3f& rotateDeg(float deg);
	lbMatrix3x3f rotatedDeg(float deg) const;

	lbMatrix3x3f& scale(float _scale);
	lbMatrix3x3f scaled(float _scale) const;
	lbMatrix3x3f& scale(float xscale,float yscale);
	lbMatrix3x3f scaled(float xscale,float _yscale) const;

	lbMatrix3x3f& translate(float x,float y);
	lbMatrix3x3f translated(float x,float y) const;


	lbMatrix3x3f& transform(const lbMatrix3x3f& b);
	lbMatrix3x3f transformed(const lbMatrix3x3f& b) const;

	lbMatrix3x3f& operator=(const lbMatrix3x3f& b);
	lbMatrix3x3f& operator*=(const lbMatrix3x3f& b);
	lbMatrix3x3f& operator/=(const lbMatrix3x3f& b);
	lbMatrix3x3f& operator+=(const lbMatrix3x3f& b);
	lbMatrix3x3f& operator-=(const lbMatrix3x3f& b);

	lbMatrix3x3f operator*(const lbMatrix3x3f& b) const;
	lbMatrix3x3f operator/(const lbMatrix3x3f& b) const;
	lbMatrix3x3f operator+(const lbMatrix3x3f& b) const;
	lbMatrix3x3f operator-(const lbMatrix3x3f& b) const;

	lbMatrix3x3f& operator=(float val);
	lbMatrix3x3f& operator*=(float val);
	lbMatrix3x3f& operator/=(float val);

	lbMatrix3x3f operator*(float val) const;
	lbMatrix3x3f operator/(float val) const;

	lbVec2f operator*(const lbVec2f& v) const;
};

lbMatrix3x3f operator*(float f,const lbMatrix3x3f& m);
lbMatrix3x3f operator/(float f, const lbMatrix3x3f& m);
#endif /* LBMATRIX3X3F_H_ */
