/*
 * lbRect.h
 *
 *  Created on: 13-09-2012
 *      Author: Spliter
 */

#pragma once

struct lbRectF
{
	float x1, y1, x2,y2;

	lbRectF()
	{
		x1=0;y1=0;x2=0;y2=0;
	}

	lbRectF(float _x1, float _y1, float _x2, float _y2)
	{
		set(_x1,_y1,_x2,_y2);
	}

	lbRectF(const lbRectF& rect)
	{
		set(rect);
	}

	lbRectF& set(float _x1, float _y1, float _x2, float _y2)
	{
		x1=_x1;y1=_y1;x2=_x2;y2=_y2;
		return *this;
	}

	lbRectF& set(const lbRectF& rect)
	{
		x1=rect.x1;y1=rect.y1;x2=rect.x2;y2=rect.y2;
		return *this;
	}

	lbRectF& scale(float sx, float sy)
	{
		x1*=sx;y1*=sy;x2*=sx;y2*=sy;
		return *this;
	}

	lbRectF scaled(float sx, float sy)
	{
		return lbRectF(*this).scale(sx,sy);
	}

	lbRectF& move(float dx, float dy)
	{
		x1+=dx;y1+=dy;x2+=dx;y2+=dy;
		return *this;
	}

	lbRectF moved(float dx, float dy)
	{
		return lbRectF(*this).move(dx,dy);
	}

	lbRectF& crop(float minx, float miny, float maxx, float maxy)
	{
		x1=x1>minx?(x1<maxx?x1:maxx):minx;
		x2=x2>minx?(x2<maxx?x2:maxx):minx;
		y1=y1>miny?(y1<maxy?y1:maxy):miny;
		y2=y2>miny?(y2<maxy?y2:maxy):miny;
		return *this;
	}

	lbRectF cropped(float minx, float miny, float maxx, float maxy)
	{
		return lbRectF(*this).crop(minx,miny,maxx,maxy);
	}

	static lbRectF getTotalRange(const lbRectF &rect1, const lbRectF &rect2)
	{
		lbRectF rect;
		rect.x1 = rect1.x1<rect2.x1?rect1.x1:rect2.x1;
		rect.y1 = rect1.y1<rect2.y1?rect1.y1:rect2.y1;
		rect.x2 = rect1.x2>rect2.x2?rect1.x2:rect2.x2;
		rect.y2 = rect1.y2>rect2.y2?rect1.y2:rect2.y2;
		return rect;
	}

	bool isInside(float x, float y)
	{
		return (x>=x1 && x<x2) && (y>y1 && y<y2);
	}

	bool isIntersecting(lbRectF rect)
	{
		return (x1<=rect.x2 && x2>=rect.x1)&&(y1<=rect.y2 && y2>=rect.y1);
	}
};

struct lbRectI
{
	int x1, y1, x2,y2;

	lbRectI()
	{
		x1=0;y1=0;x2=0;y2=0;
	}

	lbRectI(int _x1, int _y1, int _x2, int _y2)
	{
		set(_x1,_y1,_x2,_y2);
	}

	lbRectI(const lbRectI& rect)
	{
		set(rect);
	}

	lbRectI& set(int _x1, int _y1, int _x2, int _y2)
	{
		x1=_x1;y1=_y1;x2=_x2;y2=_y2;
		return *this;
	}

	lbRectI& set(const lbRectI& rect)
	{
		x1=rect.x1;y1=rect.y1;x2=rect.x2;y2=rect.y2;
		return *this;
	}

	lbRectI& scale(float sx, float sy)
	{
		x1*=sx;y1*=sy;x2*=sx;y2*=sy;
		return *this;
	}

	lbRectI scaled(float sx, float sy)
	{
		return lbRectI(*this).scale(sx,sy);
	}

	lbRectI& move(int dx, int dy)
	{
		x1+=dx;y1+=dy;x2+=dx;y2+=dy;
		return *this;
	}

	lbRectI moved(float dx, float dy)
	{
		return lbRectI(*this).move(dx,dy);
	}

	lbRectI& crop(int minx, int miny, int maxx, int maxy)
	{
		x1=x1>minx?(x1<maxx?x1:maxx):minx;
		x2=x2>minx?(x2<maxx?x2:maxx):minx;
		y1=y1>miny?(y1<maxy?y1:maxy):miny;
		y2=y2>miny?(y2<maxy?y2:maxy):miny;
		return *this;
	}

	lbRectI cropped(int minx, int miny, int maxx, int maxy)
	{
		return lbRectI(*this).crop(minx,miny,maxx,maxy);
	}

	static lbRectI getTotalRange(const lbRectI &rect1, const lbRectI &rect2)
	{
		lbRectI rect;
		rect.x1 = rect1.x1<rect2.x1?rect1.x1:rect2.x1;
		rect.y1 = rect1.y1<rect2.y1?rect1.y1:rect2.y1;
		rect.x2 = rect1.x2>rect2.x2?rect1.x2:rect2.x2;
		rect.y2 = rect1.y2>rect2.y2?rect1.y2:rect2.y2;
		return rect;
	}

	bool isInside(float x, float y)
	{
		return (x>=x1 && x<x2) && (y>y1 && y<y2);
	}

	bool isIntersecting(lbRectI rect)
	{
		return (x1<=rect.x2 && x2>=rect.x1)&&(y1<=rect.y2 && y2>=rect.y1);
	}
};

inline lbRectF toRectF(lbRectI rect)
{
	return lbRectF(rect.x1,rect.y1,rect.x2,rect.y2);
}

inline lbRectI toRectI(lbRectF rect)
{
	return lbRectI(rect.x1,rect.y1,rect.x2,rect.y2);
}
