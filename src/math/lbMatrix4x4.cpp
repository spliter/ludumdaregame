/*
 * lbMatrix4x4.cpp
 *
 *  Created on: 23-04-2011
 *      Author: Spliter
 */
#include "lbMatrix4x4f.h"
lbMatrix4x4f::lbMatrix4x4f()
{
	a=1;e=0;i=0;m=0;
	b=0;f=1;j=0;n=0;
	c=0;g=0;k=1;o=0;
	d=0;h=0;l=0;p=1;
}

lbMatrix4x4f::lbMatrix4x4f(const lbMatrix4x4f& mb)
{
	a=mb.a;e=mb.e;i=mb.i;m=mb.m;
	b=mb.b;f=mb.f;j=mb.j;n=mb.n;
	c=mb.c;g=mb.g;k=mb.k;o=mb.o;
	d=mb.d;h=mb.h;l=mb.l;p=mb.p;
}

lbMatrix4x4f::lbMatrix4x4f(	float _a, float _b, float _c, float _d,
				float _e, float _f, float _g, float _h,
				float _i, float _j, float _k, float _l,
				float _m, float _n, float _o, float _p)
{
	a=_a;e=_e;i=_i;m=_m;
	b=_b;f=_f;j=_j;n=_n;
	c=_c;g=_g;k=_k;o=_o;
	d=_d;h=_h;l=_l;p=_p;
}

void lbMatrix4x4f::lbMatrix4x4f::set(float val)
{
	a=val;e=val;i=val;m=val;
	b=val;f=val;j=val;n=val;
	c=val;g=val;k=val;o=val;
	d=val;h=val;l=val;p=val;
}

void lbMatrix4x4f::set(	float _a, float _b, float _c, float _d,
			float _e, float _f, float _g, float _h,
			float _i, float _j, float _k, float _l,
			float _m, float _n, float _o, float _p)
{
	a=_a;e=_e;i=_i;m=_m;
	b=_b;f=_f;j=_j;n=_n;
	c=_c;g=_g;k=_k;o=_o;
	d=_d;h=_h;l=_l;p=_p;
}

void lbMatrix4x4f::set(const lbMatrix4x4f& mb)
{
	a=mb.a;e=mb.e;i=mb.i;m=mb.m;
	b=mb.b;f=mb.f;j=mb.j;n=mb.n;
	c=mb.c;g=mb.g;k=mb.k;o=mb.o;
	d=mb.d;h=mb.h;l=mb.l;p=mb.p;
}

lbMatrix4x4f& lbMatrix4x4f::makeIdentity()
{
	a=1;e=0;i=0;m=0;
	b=0;f=1;j=0;n=0;
	c=0;g=0;k=1;o=0;
	d=0;h=0;l=0;p=1;
	return *this;
}

lbMatrix4x4f& lbMatrix4x4f::makeRotation(float rad,lbVec3f axis)
{
	makeRotation(rad,axis.x,axis.y,axis.z);
	return *this;
}

lbMatrix4x4f& lbMatrix4x4f::makeRotation(float rad,float ax,float ay,float az)
{
	//TODO
	float cs=cos(rad);
	float s=sin(rad);
	float cc=1.0-cs;

	a=cs+ax*ax*cc;		e=ax*ay*cc-az*s;	i=ax*az*cc+ay*s;	m=0;
	b=ay*ax*cc+az*s;	f=cs+ay*ay*cc;		j=ay*az*cc-ax*s;	n=0;
	c=az*ax*cc-ay*s;	g=az*ay*cc+ax*s;	k=cs+az*az*cc;		o=0;
	d=0;				h=0;				l=0;				p=1;
	return *this;
}

lbMatrix4x4f& lbMatrix4x4f::makeRotationDeg(float deg,lbVec3f axis)
{
	makeRotation(deg*DEG_TO_RAD_CONST,axis.x,axis.y,axis.z);
	return *this;
}

lbMatrix4x4f& lbMatrix4x4f::makeRotationDeg(float deg,float ax,float ay,float az)
{
	makeRotation(deg*DEG_TO_RAD_CONST,ax,ay,az);
	return *this;
}

lbMatrix4x4f& lbMatrix4x4f::makeScale(float xscale, float yscale, float zscale)
{
	a=xscale;e=0;	  i=0;		m=0;
	b=0;	 f=yscale;j=0;		n=0;
	c=0;	 g=0;	  k=zscale; o=0;
	d=0;	 h=0;	  l=0;		p=1;
	return *this;
}

lbMatrix4x4f& lbMatrix4x4f::makeTranslation(float xtranslate,float ytranslate, float ztranslate)
{
	a=1;e=0;i=0;m=xtranslate;
	b=0;f=1;j=0;n=ytranslate;
	c=0;g=0;k=1;o=ztranslate;
	d=0;h=0;l=0;p=1;
	return *this;
}

lbMatrix4x4f& lbMatrix4x4f::transpose()
{
	set(transposed());
	return *this;
}
lbMatrix4x4f lbMatrix4x4f::transposed() const
{
	return lbMatrix4x4f(a,e,i,m,
						b,f,j,n,
						c,g,k,o,
						d,h,l,p);
}

lbMatrix4x4f& lbMatrix4x4f::inverse()
{
	float inv[16], det;
    int i;

    inv[0] = me[5]  * me[10] * me[15] -
             me[5]  * me[11] * me[14] -
             me[9]  * me[6]  * me[15] +
             me[9]  * me[7]  * me[14] +
             me[13] * me[6]  * me[11] -
             me[13] * me[7]  * me[10];

    inv[4] = -me[4]  * me[10] * me[15] +
              me[4]  * me[11] * me[14] +
              me[8]  * me[6]  * me[15] -
              me[8]  * me[7]  * me[14] -
              me[12] * me[6]  * me[11] +
              me[12] * me[7]  * me[10];

    inv[8] = me[4]  * me[9] * me[15] -
             me[4]  * me[11] * me[13] -
             me[8]  * me[5] * me[15] +
             me[8]  * me[7] * me[13] +
             me[12] * me[5] * me[11] -
             me[12] * me[7] * me[9];

    inv[12] = -me[4]  * me[9] * me[14] +
               me[4]  * me[10] * me[13] +
               me[8]  * me[5] * me[14] -
               me[8]  * me[6] * me[13] -
               me[12] * me[5] * me[10] +
               me[12] * me[6] * me[9];

    inv[1] = -me[1]  * me[10] * me[15] +
              me[1]  * me[11] * me[14] +
              me[9]  * me[2] * me[15] -
              me[9]  * me[3] * me[14] -
              me[13] * me[2] * me[11] +
              me[13] * me[3] * me[10];

    inv[5] = me[0]  * me[10] * me[15] -
             me[0]  * me[11] * me[14] -
             me[8]  * me[2] * me[15] +
             me[8]  * me[3] * me[14] +
             me[12] * me[2] * me[11] -
             me[12] * me[3] * me[10];

    inv[9] = -me[0]  * me[9] * me[15] +
              me[0]  * me[11] * me[13] +
              me[8]  * me[1] * me[15] -
              me[8]  * me[3] * me[13] -
              me[12] * me[1] * me[11] +
              me[12] * me[3] * me[9];

    inv[13] = me[0]  * me[9] * me[14] -
              me[0]  * me[10] * me[13] -
              me[8]  * me[1] * me[14] +
              me[8]  * me[2] * me[13] +
              me[12] * me[1] * me[10] -
              me[12] * me[2] * me[9];

    inv[2] = me[1]  * me[6] * me[15] -
             me[1]  * me[7] * me[14] -
             me[5]  * me[2] * me[15] +
             me[5]  * me[3] * me[14] +
             me[13] * me[2] * me[7] -
             me[13] * me[3] * me[6];

    inv[6] = -me[0]  * me[6] * me[15] +
              me[0]  * me[7] * me[14] +
              me[4]  * me[2] * me[15] -
              me[4]  * me[3] * me[14] -
              me[12] * me[2] * me[7] +
              me[12] * me[3] * me[6];

    inv[10] = me[0]  * me[5] * me[15] -
              me[0]  * me[7] * me[13] -
              me[4]  * me[1] * me[15] +
              me[4]  * me[3] * me[13] +
              me[12] * me[1] * me[7] -
              me[12] * me[3] * me[5];

    inv[14] = -me[0]  * me[5] * me[14] +
               me[0]  * me[6] * me[13] +
               me[4]  * me[1] * me[14] -
               me[4]  * me[2] * me[13] -
               me[12] * me[1] * me[6] +
               me[12] * me[2] * me[5];

    inv[3] = -me[1] * me[6] * me[11] +
              me[1] * me[7] * me[10] +
              me[5] * me[2] * me[11] -
              me[5] * me[3] * me[10] -
              me[9] * me[2] * me[7] +
              me[9] * me[3] * me[6];

    inv[7] = me[0] * me[6] * me[11] -
             me[0] * me[7] * me[10] -
             me[4] * me[2] * me[11] +
             me[4] * me[3] * me[10] +
             me[8] * me[2] * me[7] -
             me[8] * me[3] * me[6];

    inv[11] = -me[0] * me[5] * me[11] +
               me[0] * me[7] * me[9] +
               me[4] * me[1] * me[11] -
               me[4] * me[3] * me[9] -
               me[8] * me[1] * me[7] +
               me[8] * me[3] * me[5];

    inv[15] = me[0] * me[5] * me[10] -
              me[0] * me[6] * me[9] -
              me[4] * me[1] * me[10] +
              me[4] * me[2] * me[9] +
              me[8] * me[1] * me[6] -
              me[8] * me[2] * me[5];

    det = me[0] * inv[0] + me[1] * inv[4] + me[2] * inv[8] + me[3] * inv[12];

    if (det != 0)
    {
		det = 1.0 / det;
		for (i = 0; i < 16; i++)
			me[i] = inv[i] * det;
    }
    else
    {
    	return makeIdentity();
    }

	return *this;
}

lbMatrix4x4f lbMatrix4x4f::inversed() const
{
	lbMatrix4x4f mat(*this);
	mat.inverse();
	return mat;
}

lbMatrix4x4f& lbMatrix4x4f::rotate(float rad,lbVec3f axis)
{
	lbMatrix4x4f mat;
	mat.makeRotation(rad,axis);
	transform(mat);
	return *this;
}

lbMatrix4x4f& lbMatrix4x4f::rotate(float rad, float ax, float ay, float az)
{
	lbMatrix4x4f mat;
	mat.makeRotation(rad,ax,ay,az);
	transform(mat);
	return *this;
}

lbMatrix4x4f lbMatrix4x4f::rotated(float rad,lbVec3f axis) const
{
	lbMatrix4x4f mat;
	mat.makeRotation(rad,axis);
	return transformed(mat);
}

lbMatrix4x4f lbMatrix4x4f::rotated(float rad, float ax, float ay, float az) const
{
	lbMatrix4x4f mat;
	mat.makeRotation(rad,ax,ay,az);
	return transformed(mat);
}

lbMatrix4x4f& lbMatrix4x4f::rotateDeg(float deg,lbVec3f axis)
{
	return rotate(deg*DEG_TO_RAD_CONST,axis);
}

lbMatrix4x4f& lbMatrix4x4f::rotateDeg(float deg, float ax, float ay, float az)
{
	return rotate(deg*DEG_TO_RAD_CONST,ax,ay,az);
}

lbMatrix4x4f lbMatrix4x4f::rotatedDeg(float deg,lbVec3f axis) const
{
	return rotated(deg*DEG_TO_RAD_CONST,axis);
}
lbMatrix4x4f lbMatrix4x4f::rotatedDeg(float deg, float ax, float ay, float az) const
{
	return rotated(deg*DEG_TO_RAD_CONST,ax,ay,az);
}

lbMatrix4x4f& lbMatrix4x4f::scale(float _scale)
{
	return scale(_scale,_scale,_scale);
}

lbMatrix4x4f lbMatrix4x4f::scaled(float _scale) const
{
	return scaled(_scale,_scale,_scale);
}

lbMatrix4x4f& lbMatrix4x4f::scale(float xscale,float yscale, float zscale)
{
	lbMatrix4x4f mat;
	mat.makeScale(xscale, yscale, zscale);
	return transform(mat);
}

lbMatrix4x4f lbMatrix4x4f::scaled(float xscale,float yscale, float zscale) const
{
	lbMatrix4x4f mat;
	mat.makeScale(xscale, yscale, zscale);
	return transformed(mat);
}

lbMatrix4x4f& lbMatrix4x4f::translate(float x,float y, float z)
{
	lbMatrix4x4f mat;
	mat.makeTranslation(x,y,z);
	return transform(mat);
}

lbMatrix4x4f lbMatrix4x4f::translated(float x,float y, float z) const
{
	lbMatrix4x4f mat;
	mat.makeTranslation(x,y,z);
	return transformed(mat);
}

lbMatrix4x4f& lbMatrix4x4f::postMultiply(const lbMatrix4x4f& mb)
{

}

lbMatrix4x4f lbMatrix4x4f::postMultiplied(const lbMatrix4x4f& mb) const
{
}

lbMatrix4x4f& lbMatrix4x4f::preMultiply(const lbMatrix4x4f& mb)
{
	set(	a*mb.a+e*mb.b+i*mb.c+m*mb.d,
						b*mb.a+f*mb.b+j*mb.c+n*mb.d,
						c*mb.a+g*mb.b+k*mb.c+o*mb.d,
						d*mb.a+h*mb.b+l*mb.c+p*mb.d,

						a*mb.e+e*mb.f+i*mb.g+m*mb.h,
						b*mb.e+f*mb.f+j*mb.g+n*mb.h,
						c*mb.e+g*mb.f+k*mb.g+o*mb.h,
						d*mb.e+h*mb.f+l*mb.g+p*mb.h,

						a*mb.i+e*mb.j+i*mb.k+m*mb.l,
						b*mb.i+f*mb.j+j*mb.k+n*mb.l,
						c*mb.i+g*mb.j+k*mb.k+o*mb.l,
						d*mb.i+h*mb.j+l*mb.k+p*mb.l,

						a*mb.m+e*mb.n+i*mb.o+m*mb.p,
						b*mb.m+f*mb.n+j*mb.o+n*mb.p,
						c*mb.m+g*mb.n+k*mb.o+o*mb.p,
						d*mb.m+h*mb.n+l*mb.o+p*mb.p);
//	set(tmpm);
	return *this;
}

lbMatrix4x4f lbMatrix4x4f::preMultiplied(const lbMatrix4x4f& mb) const
{
	return lbMatrix4x4f(a*mb.a+e*mb.b+i*mb.c+m*mb.d,
						b*mb.a+f*mb.b+j*mb.c+n*mb.d,
						c*mb.a+g*mb.b+k*mb.c+o*mb.d,
						d*mb.a+h*mb.b+l*mb.c+p*mb.d,

						a*mb.e+e*mb.f+i*mb.g+m*mb.h,
						b*mb.e+f*mb.f+j*mb.g+n*mb.h,
						c*mb.e+g*mb.f+k*mb.g+o*mb.h,
						d*mb.e+h*mb.f+l*mb.g+p*mb.h,

						a*mb.i+e*mb.j+i*mb.k+m*mb.l,
						b*mb.i+f*mb.j+j*mb.k+n*mb.l,
						c*mb.i+g*mb.j+k*mb.k+o*mb.l,
						d*mb.i+h*mb.j+l*mb.k+p*mb.l,

						a*mb.m+e*mb.n+i*mb.o+m*mb.p,
						b*mb.m+f*mb.n+j*mb.o+n*mb.p,
						c*mb.m+g*mb.n+k*mb.o+o*mb.p,
						d*mb.m+h*mb.n+l*mb.o+p*mb.p);
}

lbMatrix4x4f& lbMatrix4x4f::transform(const lbMatrix4x4f& mb)
{
	return preMultiply(mb);
}

lbMatrix4x4f lbMatrix4x4f::transformed(const lbMatrix4x4f& mb) const
{
	return preMultiplied(mb);
}

lbMatrix4x4f& lbMatrix4x4f::operator=(const lbMatrix4x4f& mb)
{
	a=mb.a;e=mb.e;i=mb.i;m=mb.m;
	b=mb.b;f=mb.f;j=mb.j;n=mb.n;
	c=mb.c;g=mb.g;k=mb.k;o=mb.o;
	d=mb.d;h=mb.h;l=mb.l;p=mb.p;
	return *this;
}

lbMatrix4x4f& lbMatrix4x4f::operator*=(const lbMatrix4x4f& mb)
{
//	a*=mb.a;e*=mb.e;i*=mb.i;m*=mb.m;
//	b*=mb.b;f*=mb.f;j*=mb.j;n*=mb.n;
//	c*=mb.c;g*=mb.g;k*=mb.k;o*=mb.o;
//	d*=mb.d;h*=mb.h;l*=mb.l;p*=mb.p;

	preMultiply(mb);
	return *this;
}

lbMatrix4x4f lbMatrix4x4f::operator*(const lbMatrix4x4f& mb) const
{
	return preMultiplied(mb);
}

lbMatrix4x4f& lbMatrix4x4f::operator=(float val)
{
	set(val);
	return *this;
}

lbMatrix4x4f& lbMatrix4x4f::operator*=(float val)
{
	a*=val;e*=val;i*=val;m*=val;
	b*=val;f*=val;j*=val;n*=val;
	c*=val;g*=val;k*=val;o*=val;
	d*=val;h*=val;l*=val;p*=val;
	return *this;
}

lbMatrix4x4f& lbMatrix4x4f::operator/=(float val)
{
	a/=val;e/=val;i/=val;m/=val;
	b/=val;f/=val;j/=val;n/=val;
	c/=val;g/=val;k/=val;o/=val;
	d/=val;h/=val;l/=val;p/=val;
	return *this;
}

lbMatrix4x4f lbMatrix4x4f::operator*(float val) const
{
	lbMatrix4x4f mat=*this;
	mat*=val;
	return mat;
}

lbMatrix4x4f lbMatrix4x4f::operator/(float val) const
{
	lbMatrix4x4f mat=*this;
	mat/=val;
	return mat;
}

lbVec3f lbMatrix4x4f::operator*(const lbVec3f& v) const
{
	return lbVec3f(		a*v.x+e*v.y+i*v.z+m*1,
						b*v.x+f*v.y+j*v.z+n*1,
						c*v.x+g*v.y+k*v.z+o*1);
}
