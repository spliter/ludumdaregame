/*
 * lbMatrix4x4f.h
 *
 *  Created on: 23-04-2011
 *      Author: Spliter
 */

#ifndef LBMATRIX4X4F_H_
#define LBMATRIX4X4F_H_
#include "lbBaseMath.h"
#include "lbVec3f.h"
class lbMatrix4x4f
{
public:
	union
	{
		float me[16];
		struct
		{
			float a;
			float b;
			float c;
			float d;

			float e;
			float f;
			float g;
			float h;

			float i;
			float j;
			float k;
			float l;

			float m;
			float n;
			float o;
			float p;
		};
	};


	lbMatrix4x4f();
	lbMatrix4x4f(const lbMatrix4x4f& mb);
	lbMatrix4x4f(	float _a, float _b, float _c, float _d,
					float _e, float _f, float _g, float _h,
					float _i, float _j, float _k, float _l,
					float _m, float _n, float _o, float _p);

	void set(float f);
	void set(	float _a, float _b, float _c, float _d,
				float _e, float _f, float _g, float _h,
				float _i, float _j, float _k, float _l,
				float _m, float _n, float _o, float _p);
	void set(const lbMatrix4x4f& mb);

	lbMatrix4x4f& makeIdentity();
	lbMatrix4x4f& makeRotation(float rad,lbVec3f axis);
	lbMatrix4x4f& makeRotation(float rad,float ax,float ay,float az);
	lbMatrix4x4f& makeRotationDeg(float deg, lbVec3f axis);
	lbMatrix4x4f& makeRotationDeg(float deg,float ax,float ay,float az);
	lbMatrix4x4f& makeScale(float xscale, float yscale, float zscale);
	lbMatrix4x4f& makeTranslation(float xtranslate,float ytranslate, float ztranslate);

	lbMatrix4x4f& transpose();
	lbMatrix4x4f transposed() const;

	lbMatrix4x4f& inverse();
	lbMatrix4x4f inversed() const;

	lbMatrix4x4f& rotate(float rad,lbVec3f axis);
	lbMatrix4x4f& rotate(float rad, float ax, float ay, float az);
	lbMatrix4x4f rotated(float rad,lbVec3f axis) const;
	lbMatrix4x4f rotated(float rad, float ax, float ay, float az) const;
	lbMatrix4x4f& rotateDeg(float deg,lbVec3f axis);
	lbMatrix4x4f& rotateDeg(float deg, float ax, float ay, float az);
	lbMatrix4x4f rotatedDeg(float deg,lbVec3f axis) const;
	lbMatrix4x4f rotatedDeg(float deg, float ax, float ay, float az) const;

	lbMatrix4x4f& scale(float _scale);
	lbMatrix4x4f scaled(float _scale) const;
	lbMatrix4x4f& scale(float xscale,float yscale, float zscale);
	lbMatrix4x4f scaled(float xscale,float yscale, float zscale) const;

	lbMatrix4x4f& translate(float x,float y, float z);
	lbMatrix4x4f translated(float x,float y, float z) const;


	lbMatrix4x4f& postMultiply(const lbMatrix4x4f& b);
	lbMatrix4x4f postMultiplied(const lbMatrix4x4f& b) const;

	lbMatrix4x4f& preMultiply(const lbMatrix4x4f& b);
	lbMatrix4x4f preMultiplied(const lbMatrix4x4f& b) const;

	lbMatrix4x4f& transform(const lbMatrix4x4f& b);
	lbMatrix4x4f transformed(const lbMatrix4x4f& b) const;

	lbMatrix4x4f& operator=(const lbMatrix4x4f& b);
	lbMatrix4x4f& operator*=(const lbMatrix4x4f& b);
	lbMatrix4x4f& operator/=(const lbMatrix4x4f& b);
	lbMatrix4x4f& operator+=(const lbMatrix4x4f& b);
	lbMatrix4x4f& operator-=(const lbMatrix4x4f& b);

	lbMatrix4x4f operator*(const lbMatrix4x4f& b) const;
	lbMatrix4x4f operator/(const lbMatrix4x4f& b) const;
	lbMatrix4x4f operator+(const lbMatrix4x4f& b) const;
	lbMatrix4x4f operator-(const lbMatrix4x4f& b) const;

	lbMatrix4x4f& operator=(float f);
	lbMatrix4x4f& operator*=(float f);
	lbMatrix4x4f& operator/=(float f);

	lbMatrix4x4f operator*(float f) const;
	lbMatrix4x4f operator/(float f) const;

	lbVec3f operator*(const lbVec3f& v) const;

};
#endif /* LBMATRIX4X4F_H_ */
