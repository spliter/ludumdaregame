/*
 * lbQuaternion.cpp
 *
 *  Created on: 20-05-2011
 *      Author: Spliter
 */
#include "iostream"
using namespace std;
#include "lbQuaternion.h"
lbQuaternion::lbQuaternion()
{
	x = y = z = 0;
	w = 1;
}

lbQuaternion::lbQuaternion(const lbQuaternion& quat)
{
	x = quat.x;
	y = quat.y;
	z = quat.z;
	w = quat.w;
}

lbQuaternion::lbQuaternion(float _x, float _y, float _z, float _w)
{
	x = _x;
	y = _y;
	z = _z;
	w = _w;
}

lbQuaternion::lbQuaternion(float deg, lbVec3f axis)
{
	setRotDeg(deg, axis);
	normalize();
}

lbQuaternion& lbQuaternion::set(float _x, float _y, float _z, float _w)
{
	x = _x;
	y = _y;
	z = _z;
	w = _w;
	return *this;
}

lbQuaternion& lbQuaternion::setRot(float pitch, float yaw, float roll)
{
	double c1 = cos(pitch/2);
	double c2 = cos(yaw/2);
	double c3 = cos(roll/2);

	double s1 = sin(pitch/2);
	double s2 = sin(yaw/2);
	double s3 = sin(roll/2);

	double c1c2 = c1*c2;
	double s1s2 = s1*s2;

	w = c1c2*c3 -  s1s2*s3;
	x = s1*c2*c3 + c1*s2*s3;
	y = c1*s2*c3 - s1*c2*s3;
	z = c1c2*s3 +  s1s2*c3;
	return *this;
}

lbQuaternion& lbQuaternion::setRotDeg(float pitch, float yaw, float roll)
{
	setRot(degToRad(pitch), degToRad(yaw), degToRad(roll));
	return *this;
}

lbQuaternion& lbQuaternion::setRot(float rad, const lbVec3f& vec)
{
	setRot(rad, vec.x, vec.y, vec.z);
	return *this;
}

lbQuaternion& lbQuaternion::setRot(float rad, float nx, float ny, float nz)
{
	float ls = nx*nx+ny*ny+nz*nz;
	float epsilon = 1e-7;
	if(ls<epsilon)
	{
		set(0.0, 0.0, 0.0, 1.0);
		return *this;
	}
	float invNorm = sqrt(ls);
	float cosHalf = cos(0.5*rad);
	float sinHalf = sin(0.5*rad);

	x = nx*sinHalf*invNorm;
	y = ny*sinHalf*invNorm;
	z = nz*sinHalf*invNorm;
	w = cosHalf;
	return normalize();
}

lbQuaternion& lbQuaternion::setRotDeg(float deg, const lbVec3f& vec)
{
	return setRot(deg*DEG_TO_RAD_CONST, vec.x, vec.y, vec.z);
}

lbQuaternion& lbQuaternion::setRotDeg(float deg, float nx, float ny, float nz)
{
	return setRot(deg*DEG_TO_RAD_CONST, nx, ny, nz);
}

lbQuaternion& lbQuaternion::normalize(float precision)
{
	float dsqr = x*x+y*y+z*z+w*w;

	if(fabs(dsqr-1.0)>precision)
	{
		float d1 = 1.0/sqrt(dsqr);
		x *= d1;
		y *= d1;
		z *= d1;
		w *= d1;
	}
	return *this;
}

lbQuaternion lbQuaternion::normalized(float precision)
{
	lbQuaternion ret(*this);
	ret.normalize(precision);
	return ret;
}

lbQuaternion& lbQuaternion::makeIdentity()
{
	return set(0.0, 0.0, 0.0, 1.0);
}

lbQuaternion& lbQuaternion::rotateByDeg(float deg, const lbVec3f& vec)
{
	lbQuaternion aux;
	aux.setRotDeg(deg, vec);
	return rotateBy(aux);
}

lbQuaternion& lbQuaternion::rotateBy(float rad, const lbVec3f& vec)
{
	lbQuaternion aux;
	aux.setRot(rad, vec);
	return rotateBy(aux);
}

lbQuaternion& lbQuaternion::rotateBy(const lbQuaternion& rhs)
{
	set(rhs.w*x+rhs.x*w+rhs.y*z-rhs.z*y, rhs.w*y-rhs.x*z+rhs.y*w+rhs.z*x, rhs.w*z+rhs.x*y-rhs.y*x+rhs.z*w, rhs.w*w-rhs.x*x-rhs.y*y-rhs.z*z);
	return *this;
}

lbQuaternion lbQuaternion::rotatedBy(const lbQuaternion& rhs) const
{
	return lbQuaternion(rhs.w*x+rhs.x*w+rhs.y*z-rhs.z*y, rhs.w*y-rhs.x*z+rhs.y*w+rhs.z*x, rhs.w*z+rhs.x*y-rhs.y*x+rhs.z*w, rhs.w*w-rhs.x*x-rhs.y*y-rhs.z*z);
}

lbQuaternion lbQuaternion::conjugate()
{
	return lbQuaternion(-x, -y, -z, w);

}

bool lbQuaternion::operator==(const lbQuaternion& quat)
{
	return x==quat.x&&y==quat.y&&z==quat.z&&w==quat.w;
}

bool lbQuaternion::operator!=(const lbQuaternion& quat)
{
	return x!=quat.x||y!=quat.y||z!=quat.z||w!=quat.w;
}

lbQuaternion& lbQuaternion::operator=(const lbQuaternion& quat)
{
	x = quat.x;
	y = quat.y;
	z = quat.z;
	w = quat.w;
	return *this;
}

lbQuaternion& lbQuaternion::operator*=(const lbQuaternion& quat)
{
	return rotateBy(quat);
}

lbQuaternion lbQuaternion::operator*(const lbQuaternion& quat) const
{
	return rotatedBy(quat);
}

lbQuaternion& lbQuaternion::operator-=(const lbQuaternion& quat)
{
	x -= quat.x;
	y -= quat.y;
	z -= quat.z;
	w -= quat.w;
	return *this;
}

lbQuaternion lbQuaternion::operator-() const
{
	return lbQuaternion(-x, -y, -z, -w);
}

lbQuaternion lbQuaternion::operator-(const lbQuaternion& quat) const
{
	return lbQuaternion(x-quat.x, y-quat.y, z-quat.z, w-quat.w);
}

lbQuaternion& lbQuaternion::operator+=(const lbQuaternion& quat)
{
	x += quat.x;
	y += quat.y;
	z += quat.z;
	w += quat.w;
	return *this;
}

lbQuaternion lbQuaternion::operator+(const lbQuaternion& quat) const
{
	return lbQuaternion(x+quat.x, y+quat.y, z+quat.z, w+quat.w);
}

lbQuaternion lbQuaternion::operator*(const float& f) const
{
	return lbQuaternion(x*f, y*f, z*f, w*f);
}

lbQuaternion lbQuaternion::operator/(const float& f) const
{
	return lbQuaternion(x/f, y/f, z/f, w/f);
}

lbVec3f lbQuaternion::operator*(const lbVec3f& v) const
{
	//Algorithm courtesy of Fabian Giensen, found on http://molecularmusings.wordpress.com/2013/05/24/a-faster-quaternion-vector-multiplication/
	lbVec3f t = 2*lbVec3f(x,y,z).cross(v);
	lbVec3f nt = v+t*w+lbVec3f(x,y,z).cross(t);
	return nt;
}

lbMatrix4x4f lbQuaternion::getRotationMatrix()
{
	//Note: Code copied from  gpWiki
	//http://www.gpwiki.org/index.php/OpenGL:Tutorials:Using_Quaternions_to_represent_rotation

	float x2 = x*x;
	float y2 = y*y;
	float z2 = z*z;
	float xy = x*y;
	float xz = x*z;
	float yz = y*z;
	float wx = w*-x;
	float wy = w*-y;
	float wz = w*-z;

	// This calculation would be a lot more complicated for non-unit length quaternions
	// Note: The constructor of Matrix4 expects the Matrix in column-major format like expected by
	//   OpenGL
	return lbMatrix4x4f(1.0f-2.0f*(y2+z2), 2.0f*(xy-wz), 2.0f*(xz+wy), 0.0f, 2.0f*(xy+wz), 1.0f-2.0f*(x2+z2), 2.0f*(yz-wx), 0.0f, 2.0f*(xz-wy), 2.0f*(yz+wx), 1.0f-2.0f*(x2+y2), 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f);
}

lbAxisAngle lbQuaternion::toAxisAngle()
{
	lbAxisAngle aangle;
	aangle.angle = 2*acosf(w);
	float s = sqrt(1.0-w*w);
	if(s<0.00001)
	{
		aangle.axis.set(x/s, y/s, z/s);
	}
	else
	{
		aangle.axis.set(x, y, z);
	}
	return aangle;
}

lbQuaternion slerp(const lbQuaternion& from, const lbQuaternion& to, float ratio)
{
	float dot = from.x*to.x+from.y*to.y+from.z*to.z+from.w*to.w;

	float precision = 0.9995;

	lbQuaternion auxTo(to);

	if(dot<0)
	{
		dot = -dot;
		auxTo = -to;
	}

	if(dot>precision)
	{
		//if angles are too close then just linearly interpolate
		lbQuaternion result = from+(auxTo-from)*ratio;
		result.normalize();
		return result;
	}

	float omega = acos(dot);
	float somega = sin(omega);

	if(somega==0.0)
		somega = 0.0001;
	float fromRatio = sin((1.0-ratio)*omega)/somega;
	float toRatio = sin(ratio*omega)/somega;

	return ((from*fromRatio)+(auxTo*toRatio));
}

lbQuaternion trislerp(const lbQuaternion& a, const lbQuaternion& b, const lbQuaternion& c, float u, float v)
{
	float abRatio = 0.0f;
	float cRatio = (1-(v+u));
	if(u+v>0.000001f)
	{
		abRatio = u/(u+v);
	}
	return slerp(slerp(b,a,abRatio),c,cRatio);
}

lbQuaternion slerpZero(const lbQuaternion from, float ratio)
{
	float dot = from.w;

	float precision = 0.9995;

	lbQuaternion auxTo(0.0, 0.0, 0.0, 1.0);

	if(dot<0)
	{
		dot = -dot;
		auxTo.set(0.0, 0.0, 0.0, -1.0);
	}

	if(dot>precision)
	{
		//if angles are too close then just linearly interpolate
		lbQuaternion result = from+(auxTo-from)*ratio;
		result.normalize();
		return result;
	}

	float omega = acos(dot);
	float somega = sin(omega);

	if(somega==0.0)
		somega = 0.0001;
	float fromRatio = sin((1.0-ratio)*omega)/somega;
	float toRatio = sin(ratio*omega)/somega;

	return ((from*fromRatio)+(auxTo*toRatio));
}

lbQuaternion operator*(const float& f, const lbQuaternion& quat)
{
	return lbQuaternion(quat.x*f, quat.y*f, quat.z*f, quat.w*f);
}

lbQuaternion operator-(const float& f, const lbQuaternion& quat)
{
	return lbQuaternion(f-quat.x, f-quat.y, f-quat.z, f-quat.w);
}

lbQuaternion operator+(const float& f, const lbQuaternion& quat)
{
	return lbQuaternion(f+quat.x, f+quat.y, f+quat.z, f+quat.w);
}

lbQuaternion operator/(const float& f, const lbQuaternion& quat)
{
	return lbQuaternion(f/quat.x, f/quat.y, f/quat.z, f/quat.w);
}

lbQuaternion lbQuaternion::getRotation(lbVec3f vecFrom, lbVec3f vecTo)
{
	vecFrom.normalize();
	vecTo.normalize();

	float cs = vecFrom.dot(vecTo);
	lbVec3f v = vecFrom.crossed(vecTo);

	float qw = 1+cs;
	if(cs<-1+0.00001)
		return lbQuaternion(0.0, 1.0, 0.0, 0.0);

	//lbVec3f c=vecFrom.crossed(vecTo);
	lbQuaternion rot(v.x, v.y, v.z, qw);
	rot.normalize();
	return rot;

	//	vecFrom.normalize();
	//	vecTo.normalize();
	//
	//
	//	float angle=vecFrom.dot(vecTo);
	//	lbVec3f axis=vecFrom.crossed(vecTo);
	//	axis.normalize();
	//
	//	float dot=vecFrom.dot(vecTo);
	//
	//	if(dot<-1.0f+0.0000001)
	//		return lbQuaternion(0,1,0,0);
	////	if(dot>1.0f-0.001)
	////		return lbQuaternion(0,0,0,1);
	//	lbQuaternion rot(axis.x*0.5,axis.y*0.5,axis.z*0.5,angle*0.5);
	//
	////	float w=sqrt(vecFrom.squareLength()+vecTo.squareLength())+dot;
	////	lbQuaternion rot(axis.x,axis.y,axis.z,w);
	//	rot.normalize();
	//	return rot;


	//	//	vecFrom.normalize();
	////	vecTo.normalize();
	//	lbVec3f axis=vecFrom.crossed(vecTo);
	//	float dot=vecFrom.dot(vecTo);
	//	if(dot<-1.0f+0.000001)
	//		return lbQuaternion(0,1,0,0);
	////	if(dot>1.0f-0.001)
	////		return lbQuaternion(0,0,0,1);
	////	lbQuaternion rot(axis.x*0.5f,axis.y*0.5f,axis.z*0.5f,(dot+1.0f)*0.5f);
	//	float w=sqrt(vecFrom.squareLength()+vecTo.squareLength())+dot;
	//	lbQuaternion rot(axis.x,axis.y,axis.z,w);
	//	rot.normalize();
	//	return rot;
}

std::ostream &operator<<(std::ostream &stream, lbQuaternion &quat)
{
	stream<<"quat("<<quat.x<<" , "<<quat.y<<" , "<<quat.z<<" , "<<quat.w<<")";
	return stream;
}
