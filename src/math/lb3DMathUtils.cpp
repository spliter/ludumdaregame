/*
 * lb3DMath.cpp
 *
 *  Created on: 04-01-2013
 *      Author: Spliter
 */

#include "lb3DMathUtils.h"

lbVec3f castRayFromCamera(lbVec3f camPos, lbVec3f camNormal, lbVec3f camUp, float camFov, float viewportWidth,float viewportHeight, float x, float y)
{
	lbVec3f camRight = camUp.crossed(camNormal).normalize();
	float unitRatio = 2*tanf(degToRad(camFov)/2)/viewportHeight;
	lbVec3f uIncr = unitRatio*camRight;
	lbVec3f vIncr = unitRatio*camUp;

	lbVec3f rayDir = camNormal+0.5f*(2*x+1-viewportWidth)*uIncr+0.5f*(2*y+1-viewportHeight)*vIncr;
	rayDir.normalize();

	return rayDir;
}

float intersectRayWithPlane(lbVec3f rayStart, lbVec3f rayNormal, lbVec3f planeNormal, float planeDistance)
{
	float ndt = rayNormal.dot(planeNormal);
	if(ndt>=0.0000000000000001f)
		return -1;

	float iTime = -(rayStart.dot(planeNormal)-planeDistance)/ndt;
	return iTime;
}
