/*
 * lbAnimatedTextBox.h
 *
 *  Created on: 19-11-2012
 *      Author: Spliter
 */

#pragma once
#include <string>
#include <vector>
#include "UiElement.h"

class FTFont;

class lbAnimatedTextBox: public UiElement
{
public:
	enum AnimationState
	{
		asPaused,asPlaying,asFinished
	};

	lbAnimatedTextBox();
	virtual ~lbAnimatedTextBox();

	void setFont(FTFont* font);
	void setText(const char text[],bool showAll);//if showAll is false then it will reset the animation and hide all characters

	void resetAnimation();
	void startAnimation();
	void pauseAnimation();
	void showAll();

	virtual void update(unsigned long frameDif);
	virtual void draw();

	AnimationState getState(){return _animationState;}
private:
	AnimationState _animationState;
	int _visibleCharacters;
	int _totalCharacters;
	long _millisPerCharacter;
	long _ellapsedCharMillis;//millis ellapsed since last character was shown
	int _maxCharsPerLine;
	std::vector<std::string> _textLines;
	FTFont* _font;
};
