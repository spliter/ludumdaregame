/*
 * UiElementGroup.cpp
 *
 *  Created on: 24-09-2012
 *      Author: Spliter
 */

#include "UiElementGroup.h"

UiElementGroup::UiElementGroup()
{
}

UiElementGroup::~UiElementGroup()
{
}

void UiElementGroup::update(unsigned long frameDiffMillis)
{
	updateChildren(frameDiffMillis);
}

void UiElementGroup::draw()
{
	drawChildren();
}

void UiElementGroup::addElement(UiElement* element)
{
	element->setParent(this);
	_children.remove(element);
	_children.push_back(element);
}

void UiElementGroup::removeElement(UiElement* element)
{
	element->setParent(NULL);
	_children.remove(element);
}

bool UiElementGroup::handleMouseButtonEvent(lbMouseButtonEvent* event)
{
	bool childHandled = false;
	if(event->MousePressed)
	{
		std::list<UiElement*> buffer = _children;
		std::list<UiElement*>::reverse_iterator it = buffer.rbegin();

		while(it != buffer.rend())
		{
			if((*it)->handleMouseButtonEvent(event))
			{
				childHandled = true;
				break;
			}
			it++;
			;
		}
		if(childHandled)
		{
			UiElement::handleMouseButtonEvent(event);
		}
	}
	else
	{
		std::list<UiElement*> buffer = _children;
		std::list<UiElement*>::reverse_iterator it = buffer.rbegin();

		while(it != buffer.rend())
		{
			(*it)->handleMouseButtonEvent(event);
			it++;
			;
		}
		UiElement::handleMouseButtonEvent(event);
	}
	return childHandled;
}

bool UiElementGroup::handleMouseMotionEvent(lbMouseMotionEvent* event)
{
	std::list<UiElement*> buffer = _children;
	std::list<UiElement*>::reverse_iterator it = buffer.rbegin();

	while(it != buffer.rend())
	{
		(*it)->handleMouseMotionEvent(event);
		it++;
		;
	}
	UiElement::handleMouseMotionEvent(event);
	return false;
}

void UiElementGroup::markTransformDirty()
{
	if(!_transformDirty)
	{
		_transformDirty = true;
		std::list<UiElement*>::reverse_iterator it = _children.rbegin();
		while(it != _children.rend())
		{
			(*it)->markTransformDirty();
			it++;
			;
		}
	}
}

void UiElementGroup::updateChildren(unsigned long frameDiffMillis)
{
	std::list<UiElement*> buffer = _children;
	std::list<UiElement*>::reverse_iterator it = buffer.rbegin();

	while(it != buffer.rend())
	{
		(*it)->update(frameDiffMillis);
		it++;
		;
	}
}

void UiElementGroup::drawChildren()
{
	std::list<UiElement*> buffer = _children;
	std::list<UiElement*>::reverse_iterator it = buffer.rbegin();

	while(it != buffer.rend())
	{
		(*it)->draw();
		it++;
		;
	}
}

