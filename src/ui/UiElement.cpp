/*
 * UiElement.cpp
 *
 *  Created on: 24-09-2012
 *      Author: Spliter
 */

#include "UiElement.h"
#include "UiElementGroup.h"
#include <assert.h>

UiElement::UiElement():
	_pressed(false),
	_pressButton(lbMouseButtonEvent::ButtonUnknown),
	_enabled(true),
	_listener(NULL),
	_parent(NULL),
	_isClickable(false),
	_transformDirty(true)
{

}

UiElement::~UiElement()
{
}

void UiElement::setPosition(float x, float y)
{
	_pos.set(x,y);
	_boundingRect.set(_pos.x,_pos.y,_pos.x+_size.x,_pos.y+_size.y);
	markTransformDirty();
}

void UiElement::setGlobalPosition(float x, float y)
{
	if(_parent)
	{
		_parent->updateTransform();
		lbVec2f pPos = _parent->getGlobalPosition();
		x-=pPos.x;
		y-=pPos.y;
	}
	_pos.set(x,y);
	_boundingRect.set(_pos.x,_pos.y,_pos.x+_size.x,_pos.y+_size.y);
	markTransformDirty();
}

lbVec2f UiElement::getGlobalPosition()
{
	updateTransform();
	return _globalPos;
}

void UiElement::setSize(float width, float height)
{
	_size.set(width,height);
	_boundingRect.set(_pos.x,_pos.y,_pos.x+_size.x,_pos.y+_size.y);
	markTransformDirty();
}

lbRectF UiElement::getLocalBoundingRect()
{
	return _boundingRect;
}

lbRectF UiElement::getGlobalBoundingRect()
{
	updateTransform();
	return _globalBoundingRect;
}

bool UiElement::isInside(float x, float y)
{
	updateTransform();
	return _globalBoundingRect.isInside(x,y);
}

bool UiElement::handleMouseButtonEvent(lbMouseButtonEvent* event)
{
	bool handled = false;
	if(_isClickable)
	{
		switch(event->type)
		{
			case lbMouseButtonEvent::MousePressed:
			{
				if(isInside(event->x,event->y))
				{
					_pressed = true;
					_pressButton = event->button;
					if(_listener && _enabled)
					{
						_listener->onPressed(this,event);
					}
					handled = true;
				}
				else
				{
					if(_pressed)
					{
						_pressed = false;
						if(_listener && _enabled)
						{
							_listener->onReleased(this,event);
						}
					}
				}
			}
			break;
			case lbMouseButtonEvent::MouseReleased:
			{
				if(_pressed && _pressButton==event->button)
				{
					_pressed = false;
					if(_listener && _enabled)
					{
						_listener->onReleased(this,event);
					}
					if(isInside(event->x,event->y))
					{
						if(_listener && _enabled)
						{
							_listener->onClicked(this,event);
						}
					}
				}
			}
			break;
		}
	}
	return handled;
}

bool UiElement::handleMouseMotionEvent(lbMouseMotionEvent* event)
{
	if(_isClickable)
	{
		if(_pressed)
		{
			if(_listener && _enabled)
			{
				_listener->onDragged(this,_pressButton, event);
			}
		}
		return true;
	}
	return false;
}

void UiElement::setListener(UiListener* listener)
{
	_listener = listener;
	_isClickable = true;
}

void UiElement::updateTransform()
{
	//note: we assume our local bounding box is updated
	if(_transformDirty)
	{
		_transformDirty = false;
		if(_parent)
		{
			_globalPos = _pos+_parent->getGlobalPosition();
			_globalBoundingRect.set(_globalPos.x,_globalPos.y,_globalPos.x+_size.x,_globalPos.y+_size.y);
		}
		else
		{
			_globalPos = _pos;
			_globalBoundingRect = _boundingRect;
		}
	}
}


void UiElement::setClickable(bool clickable)
{
	_isClickable=clickable;
	if(!_isClickable && _pressed)
	{
		_pressed=false;
		if(_listener)
		{
			_listener->onCancelled(this);
		}
	}
}

void UiElement::setEnabled(bool enabled)
{
	_enabled = enabled;
	if(!_enabled && _pressed)
	{
		_pressed=false;
		if(_listener)
		{
			_listener->onCancelled(this);
		}
	}
}

void UiElement::setParent(UiElementGroup* parent)
{
	assert(!_parent || !parent);

	_parent = parent;
}

