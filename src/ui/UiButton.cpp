/*
 * UiButton.cpp
 *
 *  Created on: 24-09-2012
 *      Author: Spliter
 */

#include "UiButton.h"
#include <GLee.h>
#include "../utils/graphics/drawUtils.h"
#include "../utils/graphics/lbTexture.h"

UiButton::UiButton()
{

}

UiButton::~UiButton()
{
}

void UiButton::draw()
{
	if(isPressed() && _pressedTexture)
	{
		_pressedTexture->bind();
		glColor3f(1,1,1);
		lbRectF rect = getGlobalBoundingRect();
		drawRect(rect.x1, rect.y1, rect.x2, rect.y2);
		_pressedTexture->unbind();
	}
	else if(_releasedTexture)
	{
		_releasedTexture->bind();
		glColor3f(1,1,1);
		lbRectF rect = getGlobalBoundingRect();
		drawRect(rect.x1, rect.y1, rect.x2, rect.y2);
		_releasedTexture->unbind();
	}
}

void UiButton::setGraphics(lbTexture* pressedTexture,lbTexture* releasedTexture)
{
	_pressedTexture = pressedTexture;
	_releasedTexture = releasedTexture;
}
